/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */

//#include "app_simple_timer.h"
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <math.h>

//#include "nrf_drv_twi.h"
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "app_button.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "nrf_drv_twi.h"
#include "app_pwm.h"

#include "mx25_cmd.h"
#include "mx25_def.h"

#include "nrf_drv_spi.h"
#include "nrf_drv_gpiote.h"
//#include "nrf_drv_timer.h"

#include "device_manager.h"
#include "pstorage.h" // pairing

#include "ble_dfu.h"
#include "dfu_app_handler.h"


#define FIRM_REV_MAJOR 0x00       
#define FIRM_REV_MINOR 0x01 
#define FIRM_REVISION  ((FIRM_REV_MAJOR << 8) | FIRM_REV_MINOR)  

// for dfu
/*
#include "ble_dfu.h"
#include "dfu_app_handler.h"

#include "dfu_transport.h"
#include "bootloader.h"
#include "bootloader_util.h"
*/

#define SSD1306_LCDWIDTH                  128
#define SSD1306_LCDHEIGHT                 64

static uint8_t buffer[SSD1306_LCDHEIGHT * SSD1306_LCDWIDTH / 8] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
    0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x80, 0x80, 0xC0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xF8, 0xE0, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x00, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00, 0xFF,
#if (SSD1306_LCDHEIGHT * SSD1306_LCDWIDTH > 96*16)
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x80, 0x80, 0x00, 0x00, 0x80, 0x80, 0x00, 0x00,
    0x80, 0xFF, 0xFF, 0x80, 0x80, 0x00, 0x80, 0x80, 0x00, 0x80, 0x80, 0x80, 0x80, 0x00, 0x80, 0x80,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x00, 0x00, 0x8C, 0x8E, 0x84, 0x00, 0x00, 0x80, 0xF8,
    0xF8, 0xF8, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xE0, 0xE0, 0xC0, 0x80,
    0x00, 0xE0, 0xFC, 0xFE, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFE, 0xFF, 0xC7, 0x01, 0x01,
    0x01, 0x01, 0x83, 0xFF, 0xFF, 0x00, 0x00, 0x7C, 0xFE, 0xC7, 0x01, 0x01, 0x01, 0x01, 0x83, 0xFF,
    0xFF, 0xFF, 0x00, 0x38, 0xFE, 0xC7, 0x83, 0x01, 0x01, 0x01, 0x83, 0xC7, 0xFF, 0xFF, 0x00, 0x00,
    0x01, 0xFF, 0xFF, 0x01, 0x01, 0x00, 0xFF, 0xFF, 0x07, 0x01, 0x01, 0x01, 0x00, 0x00, 0x7F, 0xFF,
    0x80, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x7F, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x01, 0xFF,
    0xFF, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x03, 0x0F, 0x3F, 0x7F, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE7, 0xC7, 0xC7, 0x8F,
    0x8F, 0x9F, 0xBF, 0xFF, 0xFF, 0xC3, 0xC0, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0xFC, 0xFC,
    0xFC, 0xFC, 0xFC, 0xFC, 0xFC, 0xF8, 0xF8, 0xF0, 0xF0, 0xE0, 0xC0, 0x00, 0x01, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x01, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x03, 0x03, 0x03, 0x01, 0x01,
    0x03, 0x01, 0x00, 0x00, 0x00, 0x01, 0x03, 0x03, 0x03, 0x03, 0x01, 0x01, 0x03, 0x03, 0x00, 0x00,
    0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x01, 0x00, 0x00, 0x00, 0x01, 0x03, 0x01, 0x00, 0x00, 0x00, 0x03,
    0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
#if (SSD1306_LCDHEIGHT == 64)
    0x00, 0x00, 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF9, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0x1F, 0x0F,
    0x87, 0xC7, 0xF7, 0xFF, 0xFF, 0x1F, 0x1F, 0x3D, 0xFC, 0xF8, 0xF8, 0xF8, 0xF8, 0x7C, 0x7D, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0x3F, 0x0F, 0x07, 0x00, 0x30, 0x30, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xFE, 0xFE, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0xC0, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0xC0, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0x7F, 0x3F, 0x1F,
    0x0F, 0x07, 0x1F, 0x7F, 0xFF, 0xFF, 0xF8, 0xF8, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xF8, 0xE0,
    0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFE, 0xFE, 0x00, 0x00,
    0x00, 0xFC, 0xFE, 0xFC, 0x0C, 0x06, 0x06, 0x0E, 0xFC, 0xF8, 0x00, 0x00, 0xF0, 0xF8, 0x1C, 0x0E,
    0x06, 0x06, 0x06, 0x0C, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFE, 0xFE, 0x00, 0x00, 0x00, 0x00, 0xFC,
    0xFE, 0xFC, 0x00, 0x18, 0x3C, 0x7E, 0x66, 0xE6, 0xCE, 0x84, 0x00, 0x00, 0x06, 0xFF, 0xFF, 0x06,
    0x06, 0xFC, 0xFE, 0xFC, 0x0C, 0x06, 0x06, 0x06, 0x00, 0x00, 0xFE, 0xFE, 0x00, 0x00, 0xC0, 0xF8,
    0xFC, 0x4E, 0x46, 0x46, 0x46, 0x4E, 0x7C, 0x78, 0x40, 0x18, 0x3C, 0x76, 0xE6, 0xCE, 0xCC, 0x80,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x01, 0x07, 0x0F, 0x1F, 0x1F, 0x3F, 0x3F, 0x3F, 0x3F, 0x1F, 0x0F, 0x03,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x0F, 0x00, 0x00,
    0x00, 0x0F, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x0F, 0x00, 0x00, 0x03, 0x07, 0x0E, 0x0C,
    0x18, 0x18, 0x0C, 0x06, 0x0F, 0x0F, 0x0F, 0x00, 0x00, 0x01, 0x0F, 0x0E, 0x0C, 0x18, 0x0C, 0x0F,
    0x07, 0x01, 0x00, 0x04, 0x0E, 0x0C, 0x18, 0x0C, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x0F, 0x0F, 0x00,
    0x00, 0x0F, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x07,
    0x07, 0x0C, 0x0C, 0x18, 0x1C, 0x0C, 0x06, 0x06, 0x00, 0x04, 0x0E, 0x0C, 0x18, 0x0C, 0x0F, 0x07,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
#endif
#endif
};

bool isSaveData;

int updateCycle = 15;
int precision = 10;

int stepTimeMin = 300;   // 0.3 sec
int stepTimeMax = 2000;  // 2.0 sec


#define INT_MAX 2147483647


#ifndef MIN
# define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
# define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

///////////////////////

int outputStep = 0;


#define MASTER_TWI_INST          0    //!< TWI interface used as a master accessing EEPROM memory

static app_timer_id_t main_timer_id;
static app_timer_id_t save_timer_id;
static app_timer_id_t noti_timer_id;

static app_timer_id_t button_timer_id;

#define MS_BETWEEN_SAMPLING_ADC		10000		      /* e.g.: fire timeout every 10 seconds */
#define MAIN_TIMER_INTERVAL         APP_TIMER_TICKS(200, APP_TIMER_PRESCALER)
#define TIMER_TICKS		            APP_TIMER_TICKS(MS_BETWEEN_SAMPLING_ADC, APP_TIMER_PRESCALER)
#define IN_LINE_PRINT_CNT        6  //<! Number of data bytes printed in single line

#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50, APP_TIMER_PRESCALER) 

    #define TWI_SCL_M                3   //!< Master SCL pin
    #define TWI_SDA_M                4   //!< Master SDA pin
    #define EEPROM_SIM_SCL_S         31   //!< Slave SCL pin
    #define EEPROM_SIM_SDA_S         30   //!< Slave SDA pin

#define IS_SRVC_CHANGED_CHARACT_PRESENT 1                                           /**< Include the service_changed characteristic. If not enabled, the server's database cannot be changed for the lifetime of the device. */

#define DEVICE_NAME                     "NaverBAND"                               /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

//#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
//#define APP_ADV_TIMEOUT_IN_SECONDS      180                                         /**< The advertising timeout (in units of seconds). */

#define APP_ADV_FAST_INTERVAL           40                                                  /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_SLOW_INTERVAL           3200                                                /**< Slow advertising interval (in units of 0.625 ms. This value corresponds to 2 seconds). */
#define APP_ADV_FAST_TIMEOUT            30                                                  /**< The duration of the fast advertising period (in seconds). */
#define APP_ADV_SLOW_TIMEOUT            180                                                 /**< The advertising timeout in units of seconds. */

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS            8 // 7 //5 //(2 + BSP_APP_TIMERS_NUMBER)                 /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                           /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

//#define START_STRING                    "Start...\n"                                /**< The string that will be sent over the UART when the application starts. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

// testtest
#define UART_TX_BUF_SIZE                1024 //1024//256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

static ble_nus_t                        m_nus;                                      /**< Structure to identify the Nordic UART Service. */
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */

static ble_uuid_t                       m_adv_uuids[] = {{BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}};  /**< Universally unique service identifier. */

#define SENSOR_BUF_SIZE	36 //240
#define SENSOR_BUF_HEAD 4
#define SENSOR_BUF_TOTAL_SIZE (SENSOR_BUF_SIZE + SENSOR_BUF_HEAD)

void bsp_configuration();

#define MAX_QUEUE_SIZE 1000 

typedef struct {
    uint8 time_year;
	uint8 time_month;
	uint8 time_day;
	uint8 time_hh;
	uint8 time_mm;
	uint8 time_ss;
	uint32 data_count;
	uint16 timeTotalBackup;
	uint16 timeTotal;
} DATA_TIMESTAMP_T;

DATA_TIMESTAMP_T dataTimeStamp;

typedef struct {
    //char sBuffer[256];
    char queue[MAX_QUEUE_SIZE+1];
    char sBufferTemp[40];
	char sBufferForHeader[4];
    uint8 readLength;
    int remainLength;
    uint8 index;
    uint16 ucid;
    bool more;
    uint8 iLoop;
    int front, rear;
    bool sending;
} DATA_SEND_T;

DATA_SEND_T dataSendBuffer;

typedef struct {
	uint8 flashPageChecker;
	uint8 flashPageToRead;

	uint16 pageNum;
	uint16 sectorNum;
} FLASH_MAP_T;

FLASH_MAP_T flashMap;

#define BMI055_ADDR                    0x18

#if( TWI0_ENABLED + TWI1_ENABLED ) > 0
static const nrf_drv_twi_t m_twi_master = NRF_DRV_TWI_INSTANCE(MASTER_TWI_INST);
#endif

static uint32_t eeprom_read(size_t addr, uint8_t * pdata, size_t size);

static void enqueue(char* data, uint16 len);
static void dequeue();
static int is_full();
static int is_empty();

static __INLINE bool buf_check(uint8_t * p_buf, uint16_t len);
	

#define RW_MODE_R 0
#define RW_MODE_W 1
#define SENSOR_DATA_SIZE 6

#define FLASH_TARGET_ADDR 0x020000   // 2번 블럭부터는 raw 데이터를 저장하자
//#define FLASH_TARGET_ADDR 0x7FF000 // memory full test 용
#define FLASH_TARGET_SIZE 0x7FFFFF

static uint32 flash_addr_for_erase; 
static uint32 flash_addr_r;  // raw data read start address
static uint32 flash_addr;    // raw data write start address
#define ACTIVITY_BASE       0x000000 
#define ACTIVITY_LIMIT_SIZE 0xFFFF
#define ACTIVITY_COUNT      (ACTIVITY_LIMIT_SIZE + 1) // 2byte
static uint32  flash_addr_activity;
static uint32  flash_addr_activity_r;

static bool readRun;
//static uint32 writeCount;

#define TX_BUF_SIZE 40
static uint8_t m_tx_data[TX_BUF_SIZE] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer with data to transfer. */
static uint8_t m_rx_data[TX_BUF_SIZE] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer for incoming data. */
static uint8_t row_data[20] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer for incoming data. */

#define DELAY_MS_100             100
#define DELAY_MS                 10//1000                /**< Timer Delay in milli-seconds. */

static uint8_t rcvBuffer[SENSOR_DATA_SIZE];

static void spi_send_recv(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode);
						  
static void spi_send_recv2(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode);		

static void spi_send_recv3(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode);							  
						  
static void showReadData(uint16_t len);						  
static void spi_master_event_handler(nrf_drv_spi_event_t event);	
static void spi_master_event_handler2(nrf_drv_spi_event_t event);							  
static volatile bool m_transfer_completed = true; /**< A flag to inform about completed transfer. */
static volatile bool m_transfer_completed2 = true; /**< A flag to inform about completed transfer. */

//static uint8_t sendDataBuffer[BLE_NUS_MAX_DATA_LEN];			

uint8 sensorMode;						  

#define TX_RX_BUF_LENGTH         16                 /**< SPI transaction buffer leng`th. */						  
						  
#if (SPI0_ENABLED == 1)
    static const nrf_drv_spi_t m_spi_master = NRF_DRV_SPI_INSTANCE(0);
#elif (SPI1_ENABLED == 1)
    static const nrf_drv_spi_t m_spi_master = NRF_DRV_SPI_INSTANCE(1);
#elif (SPI2_ENABLED == 1)
    static const nrf_drv_spi_t m_spi_master = NRF_DRV_SPI_INSTANCE(2);
#else
    #error "No SPI enabled."
#endif		

static const nrf_drv_spi_t m_spi_master2 = NRF_DRV_SPI_INSTANCE(2);

// Command
bool CMD_MEMORY_CLEAR; // flash memory clear command

// Status
bool passHandler; // 모션센서에서 값을 읽을것인가?
bool SAVE_DATA;   // flash memory에 모션센서 값을 기록
bool stepCheck;   // step count 모드
bool SEND_BATTERY; // 배터리값을 앱으로 보낸다.

// Step
int step = 0;
int ax, ay, az;

long long timestamp;
int stepOutput;
int detectStep;
long long lastStepTime = 0;
int initStep = 0;

int resultStep;

int count_step(long long times, uint16 axData, uint16 ayData, uint16 azData);
static void sensorModeChange(uint8 mode);

time_t now;

signed short acc_x, acc_y, acc_z = 0;

static void oledInit(uint32_t dc, uint32_t rs, uint32_t cs, uint32_t clk, uint32_t mosi);
void ssd1306_clear_screen(unsigned char chFill);
void ssd1306_clear_buffer_128(unsigned char chFill);
void ssd1306_refresh_gram(int mode);
void ssd1306_refresh_gram_h_scroll(int mode);
void ssd1306_refresh_gram_h_scroll_bottom(int mode);

void ssd1306_refresh_gram_4_128_test(int mode); // 64 by 32 영역을 확인하기 위해서 일단 다 그려보는게 필요하다.

#define SSD1306_CMD    0
#define SSD1306_DAT    1

//////////////////////////
// 네이버 새로운 알고리즘
// 2015.12.14
//////////////////////////
#define get_index(_index, _term, _size) \
    ((_index + _term) % (_size))
	
	
	
#define AS 2048.0
#define GRAVITY 9.80655

#define DURATION 2
#define STDEV_THRES 1.3
#define MAX_SCALAR 20
#define STATE_KEEP_DURATION 200
#define STEP_MIN_INTERVAL 300
#define STEP_WINDOW_DURATION 3000
#define STEP_WINDOW_THRES 3

#define MAX_HZ 50
#define MAX_SIZE (MAX_HZ * DURATION)
#define SIZE MAX_HZ

struct acc {
    // statistic info
    float val_sum;
    float sq_sum;
    float stdev;

    // scalar window
    float window[MAX_SIZE];
    int index;

    // state window
    int state_window[MAX_SIZE];
    long state_timestamp[MAX_SIZE];
    int state_index;

    // step window
    long step_window[MAX_SIZE];
    int step_index;

    // last peak info
    time_t last_peak_time;
};	

struct acc stat;

//int update(struct acc *stat, signed short _ax, signed short _ay, signed short _az);
int update(struct acc *stat, int _ax, int _ay, int _az);
int footprint(struct acc *stat, long timestamp);

/*
struct timeval {
  long tv_sec;
  long tv_usec;
};
*/

int retStepCount;
int totalStepCount = 0;
int totalStepCountBackup = -1;
	
#define LE8_L(x)                       ((x) & 0xff)
#define LE8_XOR(x)                     ((x) ^ 0xff)
	

static void notiFlashEraseComplete();
static void notiFlashWriteFull();
static void notiFlashReadFull();
static void notiFlashReadLast();
static void notiToApp(uint8 code);
static void notiToAppFirmwareVersion();
static void notiToAppRealtimeStep(uint8 count);

uint8 notiStateCode = 0;
bool SEND_NOTI_TO_APP = false;

//////////
// PWM
//////////
#define MOTOR_A_PWM_PIN     22
#define MOTOR_A_EN_PIN      23

APP_PWM_INSTANCE(PWM1,1);                   // Create the instance "PWM1" using TIMER1.

void pwm_ready_callback(uint32_t pwm_id);
static volatile bool ready_flag;            // A flag indicating PWM status.

bool motorStatusOld;
bool motorStatusNew;
uint8 motorStatusChangeCount;
bool IS_PWM_ENABLE;
bool ING_PWM;
bool PWM_ON;

void pwm_stop_A(void);
void pwm_start_A(void);

bool STOP_VIBE;
uint8 stopVibeCount;
	

#define DIS_ONE_BYTE   1
#define DIS_MULTI_BYTE 0

unsigned char strCountHex[5];
unsigned char strDistanceHex[5];
unsigned char strCaloryHex[6];

uint8 callResetCount;

// ddd
bool REAL_STEP = false;
bool EVT_STEP = false;

// memory
char activityTemp[5];
uint16 activity_save_timer = 0;

static void writeActivity(int count);
static void readActivity();

uint8 countRealAct = 0;
uint8 countRealActBackup = 0;

bool save_activity_count;
uint16 activity_count;
uint16 activity_count_readed;

uint8 currentActivity;

bool SEND_ACT_DATA = false;

bool CLEAR_ACT_DATA = false;

bool GET_FIRM_VER = false; // 펌웨어 버전 가져오기 커맨드

void ssd1306_command(uint8_t c);
uint8_t* spi_transfer(uint8_t * message, const uint16_t len);
void spi_send_recv_new(uint8_t * const p_tx_data,
                   uint8_t * const p_rx_data,
                   const uint16_t len);
					   
static uint32_t _dc, _rs, _cs;

#define _HI(p)      nrf_gpio_pin_set(p)
#define _LO(p)      nrf_gpio_pin_clear(p)

#define _HI_RS()    _HI(_rs)
#define _LO_RS()    _LO(_rs)
#define _HI_DC()    _HI(_dc)
#define _LO_DC()    _LO(_dc)
#define _HI_CS()    _HI(_cs)
#define _LO_CS()    _LO(_cs)

#define SSD1306_CONFIG_VDD_PIN      2
#define SSD1306_CONFIG_CLK_PIN      2
#define SSD1306_CONFIG_MOSI_PIN     27
#define SSD1306_CONFIG_CS_PIN       25
#define SSD1306_CONFIG_DC_PIN       26 // miso
#define SSD1306_CONFIG_RST_PIN      24

ble_dfu_init_t   dfus_init;
static void advertising_stop(void);
//static void app_context_load(dm_handle_t const * p_handle);
	
#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x04                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START         0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                   0xFFFF                                     /**< Max handle value in BLE. */

static ble_dfu_t                         m_dfus;     

static void reset_prepare(void);
	
static ble_gap_sec_params_t      m_sec_param;                              /**< Security parameter for use in security requests. */
static dm_application_instance_t        m_app_handle;                               /**< Application identifier allocated by device manager. */
static dm_handle_t                      m_bonded_peer_handle;                       /**< Device reference handle to the current connected peer. */

static void device_manager_init(bool erase_bonds);

#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE//BLE_GAP_IO_CAPS_DISPLAY_ONLY //BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */
	
#define STATIC_PASSKEY              "123456"                         /**< Static pin. */
uint8_t                             passkey[] = STATIC_PASSKEY;

static void goalInit();
static void sendBattery();
	
/////////////
// step

typedef struct {
	unsigned int goal_step;      // 목표 걸음수
	unsigned int goal_calory;    // 목표 칼로리
	unsigned int goal_distance;  // 목표 거리
	uint8 goal_default;          // 기본 목표
} GOAL_INFO;

GOAL_INFO goalInfo;

typedef struct {
	double heightInInches;
	double weight;
	uint8 sex;
} DATA_MAN_INFO;

DATA_MAN_INFO dataManInfo;

typedef struct {
	uint16 disFirst;
	double disSecond;
	uint8 disThird;
} DATA_DIS_T;

DATA_DIS_T dataDis;

typedef struct {
	uint8 calFirst;
	uint16 calSecond;	
	uint32 calData;
} DATA_CALORY_T;

DATA_CALORY_T dataCal;

#define MILE_BY_INCHE 63360

typedef enum {
	CONV_INCH = 0,
	CONV_CM,
	CONV_POUND,
	CONV_KG,
	CONV_MAX
} conv_type_t;

conv_type_t converType;

void showOledDistance();
void showOledCalory();
static double stepToMile(double step);
static double stepPerMile(double step);
static void setInfoWeight(double w, int mode);
static void setInfoHeight(double h, int mode);
static void setInfoSex(uint8 s);
static double getInfoHeight();
static double getInfoWeight();
static uint32 stepToCalory(uint32 step);
static void showManInfo();
static void manInfoInit();


static double stepToKm(double step);
static double stepPerKm(double step);

//void ssd1306_draw_bitmap(unsigned char chXpos, unsigned char chYpos, const unsigned char *pchBmp, unsigned char chWidth, unsigned char chHeight);

/////////////
// band mode
typedef enum {
	BAND_MODE_CLOCK = 0,
	BAND_MODE_WALK,
	BAND_MODE_DISTANCE,
	BAND_MODE_CAL,
	BAND_MODE_MAX
} band_mode_t;

band_mode_t band_mode;

static void bandModeChange(band_mode_t mode); // band의 모드를 변경합니다.
static void bandModeLoop(); // band의 모드를 변경합니다.
	
void ssd1306_display(void);
void ssd1306_display_char_app(unsigned char chXpos, unsigned char chYpos, uint8 index, unsigned char chSize, unsigned char chMode);
void ssd1306_display_string_app(unsigned char chXpos, unsigned char  chYpos, uint8 index, unsigned char  chSize, unsigned char  chMode);
void ssd1306_display_string_h_scroll(unsigned char chXpos, unsigned char  chYpos, const unsigned char  *pchString, unsigned char  chSize, unsigned char  chMode);

bool vibeNew = false;
unsigned char vibePattern[15];
uint8 vibePatternIndex = 0;
bool isVibeOn = false;
uint8 vibePatternCount = 0;
unsigned char vibeData;
////////////
// 시계
////////////
typedef enum
{
    APP_STATE_SINGLE_SHOT,                                              /**< Application state where single shot timer mode is tested. */
    APP_STATE_REPEATED                                                  /**< Application state where repeated timer mode is tested. */
} state_t;

static __INLINE void state_entry_action_execute(void);
static void state_machine_state_change(state_t new_state);
//static void led_and_timer_control(uint32_t led_id, app_simple_timer_mode_t timer_mode);
void timeout_handler(void * p_context);

static void clockInit(); // 밴드의 시간정보를 초기화한다.	
unsigned char strCurruntTime[5];

bool SHOW_CURRENT_TIME; // 현재 시간을 보여주는 준다.

#define TIMEOUT_VALUE                    50000                          /**< 50 mseconds timer time-out value. */
#define TOGGLE_LED_COUNTER               (500 / (TIMEOUT_VALUE / 1000)) /**< Interval for toggling a LED. Yields to 500 mseconds. */
#define STATE_TRANSIT_COUNTER_INIT_VALUE (4 * TOGGLE_LED_COUNTER)       /**< Initial value for the state transition counter.  */
#define GENERIC_DELAY_TIME               1000                           /**< Generic delay time used by application. */

static volatile uint32_t m_state_transit_counter = 0;                            /**< State transition counter variable. */
static volatile uint32_t m_toggle_led_counter    = 0;                            /**< Led toggling counter variable. */
static volatile state_t  m_state;      
/*
const nrf_drv_timer_t TIMER_CLOCK = NRF_DRV_TIMER_INSTANCE(2); //NRF_DRV_TIMER_INSTANCE(0);
uint32_t time_ms = 60000;
uint32_t time_ticks;
void timer_clcok_event_handler(nrf_timer_event_t event_type, void* p_context);
*/

#define DIS_SIZE 0 // 64 by 32
				   // 1 : 128 by 64

/*
#define SSD1306_WIDTH    128
#define SSD1306_HEIGHT   64
*/
#define SSD1306_WIDTH    64
#define SSD1306_HEIGHT   32

#define OLED_W 64
#define OLED_H 4
#define REAL_W 128
#define REAL_H 4
//static unsigned char s_chDispalyBuffer[OLED_W][OLED_H]; // [128][8]
static unsigned char s_chDispalyBuffer[REAL_W][REAL_H]; // [128][8]
//static unsigned char s_chDispalyBuffer[128][8];

const unsigned char c_chFont1206[95][12];
//const unsigned char c_chFont1206[10][12];
const unsigned char c_chFont1608[95][16];
//const unsigned char c_chFont1608[26][16];

void ssd1306_draw_bitmap(unsigned char chXpos, unsigned char chYpos, const unsigned char *pchBmp, unsigned char chWidth, unsigned char chHeight);
void ssd1306_draw_point(unsigned char chXpos, unsigned char chYpos, unsigned char chPoint);

void ssd1306_display_char(unsigned char chXpos, unsigned char chYpos, unsigned char chChr, unsigned char chSize, unsigned char chMode);
void ssd1306_display_string(unsigned char chXpos, unsigned char  chYpos, const unsigned char  *pchString, unsigned char  chSize, unsigned char  chMode);
void ssd1306_draw_0608char(unsigned char chXpos, unsigned char chYpos, unsigned char chChar);

void showOledCount();

bool SHOW_OLED_STEP_FLAG;
bool IS_STEP_CHANGE;
bool IS_SHOW_CALL_NUM;
bool SHOW_CALL_NUM;
bool SHOW_DISTANCE;
bool SHOW_CALORY;
bool SHOW_16_8;

bool IS_CALL_NUM_SCROLLING;  // 전화번호 스크롤중인가?


unsigned char phone_num[17];

static uint8_t oled_data[64] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, };

/*
const unsigned char IMG_inputvolt[] = {
0x00,0x00,0x00,0x00,0x00,0x00,0x1f,
0x38,0x8f,0x91,0xfa,0x1c,0x40,0x1f,
0x44,0x80,0x70,0x42,0x22,0x40,0x1f,
0x44,0x87,0x90,0x4e,0x22,0x60,0x1f,
0x44,0x88,0x70,0x62,0x22,0x40,0x5f,
0x38,0x88,0x10,0x92,0x1c,0x40,0x1f,
0x00,0x07,0xd1,0x0a,0x00,0x40,0x1f,
0x20,0x80,0x00,0x02,0x10,0x40,0x1f,
0x3f,0x87,0xe0,0x80,0x1f,0xc0,0x5f,
0x20,0x80,0x10,0x80,0x10,0x40,0x1f,
0x1f,0x00,0x10,0x7e,0x0f,0x80,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00,0x1f
}; 
*/

/*
const unsigned char IMG_inputvolt[] = {
0x3F,0x00,0x00,0x00,0x00,0x00,0x1f,
0x3F,0x00,0x00,0x00,0x00,0x00,0x1f,
0x0C,0x00,0x00,0x00,0x00,0x00,0x1f,
0x0C,0x00,0x00,0x00,0x00,0x00,0x1f,
0x0C,0x00,0x00,0x00,0x00,0x00,0x1f,
0x0C,0x00,0x00,0x00,0x00,0x00,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00,0x1f,
0x00,0x00,0x00,0x00,0x00,0x00,0x1f,
}; 
*/

// 마지막 바이트의 하위 4비트는 출력이 안됨
// 마지막 바이트의 1f는 아예 출력이 안됨
// 각각 한줄이 OLED에서 한줄이다. 
// 각바이트의 비트 순서는 8, 4, 2, 1이다. 
///*
const unsigned char IMG_inputvolt[] = {
0x3F,
0x3F,
0x0C,
0x0C,
0x0C,
0x0C,
}; 
//*/

const unsigned char c_chFont0608[11][8] = 
{
	{0x73,0x8b,0x8b,0x8b,0x8b,0x8b,0x8b,0x73},/*"0",0*/
	{0x23,0xe3,0x23,0x23,0x23,0x23,0x23,0x23},/*"1",1*/
	{0x73,0x8b,0x8b,0x13,0x23,0x43,0x83,0xfb},/*"2",2*/
	{0x73,0x8b,0x0b,0x73,0x0b,0x0b,0x8b,0x73},/*"3",3*/
	{0x33,0x33,0x53,0x53,0x93,0xfb,0x13,0x13},/*"4",4*/
	{0xfb,0x83,0x83,0xf3,0x0b,0x0b,0x8b,0x73},/*"5",5*/
	{0x73,0x8b,0x83,0xf3,0x8b,0x8b,0x8b,0x73},/*"6",6*/
	{0xfb,0x0b,0x13,0x13,0x13,0x23,0x23,0x23},/*"7",7*/
	{0x73,0x8b,0x8b,0x73,0x8b,0x8b,0x8b,0x73},/*"8",8*/
	{0x73,0x8b,0x8b,0x8b,0x7b,0x0b,0x8b,0x73},/*"9",9*/
	{0x03,0x03,0x03,0x03,0x03,0x03,0x33,0x33},/*":",10*/
};


const unsigned char c_chFont1206[95][12] = 
{
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},// " ",0
	{0x00,0x00,0x00,0x00,0x3F,0x40,0x00,0x00,0x00,0x00,0x00,0x00},//"!",1
	{0x00,0x00,0x30,0x00,0x40,0x00,0x30,0x00,0x40,0x00,0x00,0x00},//""",2
	{0x09,0x00,0x0B,0xC0,0x3D,0x00,0x0B,0xC0,0x3D,0x00,0x09,0x00},//"#",3
	{0x18,0xC0,0x24,0x40,0x7F,0xE0,0x22,0x40,0x31,0x80,0x00,0x00},//"$",4
	{0x18,0x00,0x24,0xC0,0x1B,0x00,0x0D,0x80,0x32,0x40,0x01,0x80},//"%",5
	{0x03,0x80,0x1C,0x40,0x27,0x40,0x1C,0x80,0x07,0x40,0x00,0x40},//"&",6
	{0x10,0x00,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"'",7
	{0x00,0x00,0x00,0x00,0x00,0x00,0x1F,0x80,0x20,0x40,0x40,0x20},//"(",8
	{0x00,0x00,0x40,0x20,0x20,0x40,0x1F,0x80,0x00,0x00,0x00,0x00},//")",9
	{0x09,0x00,0x06,0x00,0x1F,0x80,0x06,0x00,0x09,0x00,0x00,0x00},//"*",10
	{0x04,0x00,0x04,0x00,0x3F,0x80,0x04,0x00,0x04,0x00,0x00,0x00},//"+",11
	{0x00,0x10,0x00,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//",",12
	{0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x00,0x00},//"-",13
	{0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//".",14
	{0x00,0x20,0x01,0xC0,0x06,0x00,0x38,0x00,0x40,0x00,0x00,0x00},//"/",15
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},//"0",16
	{0x00,0x00,0x10,0x40,0x3F,0xC0,0x00,0x40,0x00,0x00,0x00,0x00},//"1",17
	{0x18,0xC0,0x21,0x40,0x22,0x40,0x24,0x40,0x18,0x40,0x00,0x00},//"2",18
	{0x10,0x80,0x20,0x40,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},//"3",19
	{0x02,0x00,0x0D,0x00,0x11,0x00,0x3F,0xC0,0x01,0x40,0x00,0x00},//"4",20
	{0x3C,0x80,0x24,0x40,0x24,0x40,0x24,0x40,0x23,0x80,0x00,0x00},//"5",21	
	{0x1F,0x80,0x24,0x40,0x24,0x40,0x34,0x40,0x03,0x80,0x00,0x00},//"6",22
	{0x30,0x00,0x20,0x00,0x27,0xC0,0x38,0x00,0x20,0x00,0x00,0x00},//"7",23
	{0x1B,0x80,0x24,0x40,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},//"8",24
	{0x1C,0x00,0x22,0xC0,0x22,0x40,0x22,0x40,0x1F,0x80,0x00,0x00},//"9",25
	{0x00,0x00,0x00,0x00,0x08,0x40,0x00,0x00,0x00,0x00,0x00,0x00},//":",26
	{0x00,0x00,0x00,0x00,0x04,0x60,0x00,0x00,0x00,0x00,0x00,0x00},//";",27
	{0x00,0x00,0x04,0x00,0x0A,0x00,0x11,0x00,0x20,0x80,0x40,0x40},//"<",28
	{0x09,0x00,0x09,0x00,0x09,0x00,0x09,0x00,0x09,0x00,0x00,0x00},//"=",29
	{0x00,0x00,0x40,0x40,0x20,0x80,0x11,0x00,0x0A,0x00,0x04,0x00},//">",30
	{0x18,0x00,0x20,0x00,0x23,0x40,0x24,0x00,0x18,0x00,0x00,0x00},//"?",31
	{0x1F,0x80,0x20,0x40,0x27,0x40,0x29,0x40,0x1F,0x40,0x00,0x00},//"@",32
	{0x00,0x40,0x07,0xC0,0x39,0x00,0x0F,0x00,0x01,0xC0,0x00,0x40},//"A",33
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},//"B",34
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x30,0x80,0x00,0x00},//"C",35
	{0x20,0x40,0x3F,0xC0,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},//"D",36
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x2E,0x40,0x30,0xC0,0x00,0x00},//"E",37
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x2E,0x00,0x30,0x00,0x00,0x00},//"F",38
	{0x0F,0x00,0x10,0x80,0x20,0x40,0x22,0x40,0x33,0x80,0x02,0x00},//"G",39
	{0x20,0x40,0x3F,0xC0,0x04,0x00,0x04,0x00,0x3F,0xC0,0x20,0x40},//"H",40
	{0x20,0x40,0x20,0x40,0x3F,0xC0,0x20,0x40,0x20,0x40,0x00,0x00},//"I",41
	{0x00,0x60,0x20,0x20,0x20,0x20,0x3F,0xC0,0x20,0x00,0x20,0x00},//"J",42
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x0B,0x00,0x30,0xC0,0x20,0x40},//"K",43
	{0x20,0x40,0x3F,0xC0,0x20,0x40,0x00,0x40,0x00,0x40,0x00,0xC0},//"L",44
	{0x3F,0xC0,0x3C,0x00,0x03,0xC0,0x3C,0x00,0x3F,0xC0,0x00,0x00},//"M",45
	{0x20,0x40,0x3F,0xC0,0x0C,0x40,0x23,0x00,0x3F,0xC0,0x20,0x00},//"N",46
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},//"O",47
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x24,0x00,0x18,0x00,0x00,0x00},//"P",48
	{0x1F,0x80,0x21,0x40,0x21,0x40,0x20,0xE0,0x1F,0xA0,0x00,0x00},//"Q",49
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x26,0x00,0x19,0xC0,0x00,0x40},//"R",50
	{0x18,0xC0,0x24,0x40,0x24,0x40,0x22,0x40,0x31,0x80,0x00,0x00},//"S",51
	{0x30,0x00,0x20,0x40,0x3F,0xC0,0x20,0x40,0x30,0x00,0x00,0x00},//"T",52
	{0x20,0x00,0x3F,0x80,0x00,0x40,0x00,0x40,0x3F,0x80,0x20,0x00},//"U",53
	{0x20,0x00,0x3E,0x00,0x01,0xC0,0x07,0x00,0x38,0x00,0x20,0x00},//"V",54
	{0x38,0x00,0x07,0xC0,0x3C,0x00,0x07,0xC0,0x38,0x00,0x00,0x00},//"W",55
	{0x20,0x40,0x39,0xC0,0x06,0x00,0x39,0xC0,0x20,0x40,0x00,0x00},//"X",56
	{0x20,0x00,0x38,0x40,0x07,0xC0,0x38,0x40,0x20,0x00,0x00,0x00},//"Y",57
	{0x30,0x40,0x21,0xC0,0x26,0x40,0x38,0x40,0x20,0xC0,0x00,0x00},//"Z",58
	{0x00,0x00,0x00,0x00,0x7F,0xE0,0x40,0x20,0x40,0x20,0x00,0x00},//"[",59
	{0x00,0x00,0x70,0x00,0x0C,0x00,0x03,0x80,0x00,0x40,0x00,0x00},//"\",60
	{0x00,0x00,0x40,0x20,0x40,0x20,0x7F,0xE0,0x00,0x00,0x00,0x00},//"]",61
	{0x00,0x00,0x20,0x00,0x40,0x00,0x20,0x00,0x00,0x00,0x00,0x00},//"^",62
	{0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10},//"_",63
	{0x00,0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"`",64
	{0x00,0x00,0x02,0x80,0x05,0x40,0x05,0x40,0x03,0xC0,0x00,0x40},//"a",65
	{0x20,0x00,0x3F,0xC0,0x04,0x40,0x04,0x40,0x03,0x80,0x00,0x00},//"b",66
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x40,0x06,0x40,0x00,0x00},//"c",67
	{0x00,0x00,0x03,0x80,0x04,0x40,0x24,0x40,0x3F,0xC0,0x00,0x40},//"d",68
	{0x00,0x00,0x03,0x80,0x05,0x40,0x05,0x40,0x03,0x40,0x00,0x00},//"e",69
	{0x00,0x00,0x04,0x40,0x1F,0xC0,0x24,0x40,0x24,0x40,0x20,0x00},//"f",70
	{0x00,0x00,0x02,0xE0,0x05,0x50,0x05,0x50,0x06,0x50,0x04,0x20},//"g",71
	{0x20,0x40,0x3F,0xC0,0x04,0x40,0x04,0x00,0x03,0xC0,0x00,0x40},//"h",72
	{0x00,0x00,0x04,0x40,0x27,0xC0,0x00,0x40,0x00,0x00,0x00,0x00},//"i",73
	{0x00,0x10,0x00,0x10,0x04,0x10,0x27,0xE0,0x00,0x00,0x00,0x00},//"j",74
	{0x20,0x40,0x3F,0xC0,0x01,0x40,0x07,0x00,0x04,0xC0,0x04,0x40},//"k",75
	{0x20,0x40,0x20,0x40,0x3F,0xC0,0x00,0x40,0x00,0x40,0x00,0x00},//"l",76
	{0x07,0xC0,0x04,0x00,0x07,0xC0,0x04,0x00,0x03,0xC0,0x00,0x00},//"m",77
	{0x04,0x40,0x07,0xC0,0x04,0x40,0x04,0x00,0x03,0xC0,0x00,0x40},//"n",78
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x40,0x03,0x80,0x00,0x00},//"o",79
	{0x04,0x10,0x07,0xF0,0x04,0x50,0x04,0x40,0x03,0x80,0x00,0x00},//"p",80
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x50,0x07,0xF0,0x00,0x10},//"q",81
	{0x04,0x40,0x07,0xC0,0x02,0x40,0x04,0x00,0x04,0x00,0x00,0x00},//"r",82
	{0x00,0x00,0x06,0x40,0x05,0x40,0x05,0x40,0x04,0xC0,0x00,0x00},//"s",83
	{0x00,0x00,0x04,0x00,0x1F,0x80,0x04,0x40,0x00,0x40,0x00,0x00},//"t",84
	{0x04,0x00,0x07,0x80,0x00,0x40,0x04,0x40,0x07,0xC0,0x00,0x40},//"u",85
	{0x04,0x00,0x07,0x00,0x04,0xC0,0x01,0x80,0x06,0x00,0x04,0x00},//"v",86
	{0x06,0x00,0x01,0xC0,0x07,0x00,0x01,0xC0,0x06,0x00,0x00,0x00},//"w",87
	//{0x04,0x40,0x06,0xC0,0x01,0x00,0x06,0xC0,0x04,0x40,0x00,0x00},//"x",88
	//{0x04,0x10,0x07,0x10,0x04,0xE0,0x01,0x80,0x06,0x00,0x04,0x00},//"y",89
	//{0x00,0x00,0x04,0x40,0x05,0xC0,0x06,0x40,0x04,0x40,0x00,0x00},//"z",90
	{0xFF,0x00,0x01,0x00,0x20,0x00,0xFF,0x80,0x00,0x00,0xFF,0xC0},//"x",88   네
	{0x7F,0x80,0x80,0x40,0x80,0x40,0x7F,0x80,0x00,0x00,0xFF,0xC0},//"y",89   이
	{0xFF,0x80,0x08,0x80,0x08,0x80,0xFF,0x80,0x08,0x00,0xFF,0xC0},//"z",90   버
	{0x00,0x00,0x00,0x00,0x04,0x00,0x7B,0xE0,0x40,0x20,0x00,0x00},//"{",91
	{0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xF0,0x00,0x00,0x00,0x00},//"|",92
	{0x00,0x00,0x40,0x20,0x7B,0xE0,0x04,0x00,0x00,0x00,0x00,0x00},//"}",93
	{0x40,0x00,0x80,0x00,0x40,0x00,0x20,0x00,0x20,0x00,0x40,0x00},//"~",94
}; 


/*
const unsigned char c_chFont1206[10][12] = 
{
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},// " ",0
	{0x00,0x00,0x00,0x00,0x3F,0x40,0x00,0x00,0x00,0x00,0x00,0x00},//"!",1
	{0x00,0x00,0x30,0x00,0x40,0x00,0x30,0x00,0x40,0x00,0x00,0x00},//""",2
	{0x09,0x00,0x0B,0xC0,0x3D,0x00,0x0B,0xC0,0x3D,0x00,0x09,0x00},//"#",3
	{0x18,0xC0,0x24,0x40,0x7F,0xE0,0x22,0x40,0x31,0x80,0x00,0x00},//"$",4
	{0x18,0x00,0x24,0xC0,0x1B,0x00,0x0D,0x80,0x32,0x40,0x01,0x80},//"%",5
	{0x03,0x80,0x1C,0x40,0x27,0x40,0x1C,0x80,0x07,0x40,0x00,0x40},//"&",6
	{0x10,0x00,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"'",7
	{0x00,0x00,0x00,0x00,0x00,0x00,0x1F,0x80,0x20,0x40,0x40,0x20},//"(",8
	{0x00,0x00,0x40,0x20,0x20,0x40,0x1F,0x80,0x00,0x00,0x00,0x00},//")",9
*/
	/*
	{0x09,0x00,0x06,0x00,0x1F,0x80,0x06,0x00,0x09,0x00,0x00,0x00},//"*",10
	{0x04,0x00,0x04,0x00,0x3F,0x80,0x04,0x00,0x04,0x00,0x00,0x00},//"+",11
	{0x00,0x10,0x00,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//",",12
	{0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x00,0x00},//"-",13
	{0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//".",14
	{0x00,0x20,0x01,0xC0,0x06,0x00,0x38,0x00,0x40,0x00,0x00,0x00},//"/",15
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},//"0",16
	{0x00,0x00,0x10,0x40,0x3F,0xC0,0x00,0x40,0x00,0x00,0x00,0x00},//"1",17
	{0x18,0xC0,0x21,0x40,0x22,0x40,0x24,0x40,0x18,0x40,0x00,0x00},//"2",18
	{0x10,0x80,0x20,0x40,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},//"3",19
	{0x02,0x00,0x0D,0x00,0x11,0x00,0x3F,0xC0,0x01,0x40,0x00,0x00},//"4",20
	{0x3C,0x80,0x24,0x40,0x24,0x40,0x24,0x40,0x23,0x80,0x00,0x00},//"5",21	
	{0x1F,0x80,0x24,0x40,0x24,0x40,0x34,0x40,0x03,0x80,0x00,0x00},//"6",22
	{0x30,0x00,0x20,0x00,0x27,0xC0,0x38,0x00,0x20,0x00,0x00,0x00},//"7",23
	{0x1B,0x80,0x24,0x40,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},//"8",24
	{0x1C,0x00,0x22,0xC0,0x22,0x40,0x22,0x40,0x1F,0x80,0x00,0x00},//"9",25
	{0x00,0x00,0x00,0x00,0x08,0x40,0x00,0x00,0x00,0x00,0x00,0x00},//":",26
	{0x00,0x00,0x00,0x00,0x04,0x60,0x00,0x00,0x00,0x00,0x00,0x00},//";",27
	{0x00,0x00,0x04,0x00,0x0A,0x00,0x11,0x00,0x20,0x80,0x40,0x40},//"<",28
	{0x09,0x00,0x09,0x00,0x09,0x00,0x09,0x00,0x09,0x00,0x00,0x00},//"=",29
	{0x00,0x00,0x40,0x40,0x20,0x80,0x11,0x00,0x0A,0x00,0x04,0x00},//">",30
	{0x18,0x00,0x20,0x00,0x23,0x40,0x24,0x00,0x18,0x00,0x00,0x00},//"?",31
	{0x1F,0x80,0x20,0x40,0x27,0x40,0x29,0x40,0x1F,0x40,0x00,0x00},//"@",32
	{0x00,0x40,0x07,0xC0,0x39,0x00,0x0F,0x00,0x01,0xC0,0x00,0x40},//"A",33
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},//"B",34
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x30,0x80,0x00,0x00},//"C",35
	{0x20,0x40,0x3F,0xC0,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},//"D",36
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x2E,0x40,0x30,0xC0,0x00,0x00},//"E",37
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x2E,0x00,0x30,0x00,0x00,0x00},//"F",38
	{0x0F,0x00,0x10,0x80,0x20,0x40,0x22,0x40,0x33,0x80,0x02,0x00},//"G",39
	{0x20,0x40,0x3F,0xC0,0x04,0x00,0x04,0x00,0x3F,0xC0,0x20,0x40},//"H",40
	{0x20,0x40,0x20,0x40,0x3F,0xC0,0x20,0x40,0x20,0x40,0x00,0x00},//"I",41
	{0x00,0x60,0x20,0x20,0x20,0x20,0x3F,0xC0,0x20,0x00,0x20,0x00},//"J",42
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x0B,0x00,0x30,0xC0,0x20,0x40},//"K",43
	{0x20,0x40,0x3F,0xC0,0x20,0x40,0x00,0x40,0x00,0x40,0x00,0xC0},//"L",44
	{0x3F,0xC0,0x3C,0x00,0x03,0xC0,0x3C,0x00,0x3F,0xC0,0x00,0x00},//"M",45
	{0x20,0x40,0x3F,0xC0,0x0C,0x40,0x23,0x00,0x3F,0xC0,0x20,0x00},//"N",46
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},//"O",47
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x24,0x00,0x18,0x00,0x00,0x00},//"P",48
	{0x1F,0x80,0x21,0x40,0x21,0x40,0x20,0xE0,0x1F,0xA0,0x00,0x00},//"Q",49
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x26,0x00,0x19,0xC0,0x00,0x40},//"R",50
	{0x18,0xC0,0x24,0x40,0x24,0x40,0x22,0x40,0x31,0x80,0x00,0x00},//"S",51
	{0x30,0x00,0x20,0x40,0x3F,0xC0,0x20,0x40,0x30,0x00,0x00,0x00},//"T",52
	{0x20,0x00,0x3F,0x80,0x00,0x40,0x00,0x40,0x3F,0x80,0x20,0x00},//"U",53
	{0x20,0x00,0x3E,0x00,0x01,0xC0,0x07,0x00,0x38,0x00,0x20,0x00},//"V",54
	{0x38,0x00,0x07,0xC0,0x3C,0x00,0x07,0xC0,0x38,0x00,0x00,0x00},//"W",55
	{0x20,0x40,0x39,0xC0,0x06,0x00,0x39,0xC0,0x20,0x40,0x00,0x00},//"X",56
	{0x20,0x00,0x38,0x40,0x07,0xC0,0x38,0x40,0x20,0x00,0x00,0x00},//"Y",57
	{0x30,0x40,0x21,0xC0,0x26,0x40,0x38,0x40,0x20,0xC0,0x00,0x00},//"Z",58
	{0x00,0x00,0x00,0x00,0x7F,0xE0,0x40,0x20,0x40,0x20,0x00,0x00},//"[",59
	{0x00,0x00,0x70,0x00,0x0C,0x00,0x03,0x80,0x00,0x40,0x00,0x00},//"\",60
	{0x00,0x00,0x40,0x20,0x40,0x20,0x7F,0xE0,0x00,0x00,0x00,0x00},//"]",61
	{0x00,0x00,0x20,0x00,0x40,0x00,0x20,0x00,0x00,0x00,0x00,0x00},//"^",62
	{0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10},//"_",63
	{0x00,0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"`",64
	{0x00,0x00,0x02,0x80,0x05,0x40,0x05,0x40,0x03,0xC0,0x00,0x40},//"a",65
	{0x20,0x00,0x3F,0xC0,0x04,0x40,0x04,0x40,0x03,0x80,0x00,0x00},//"b",66
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x40,0x06,0x40,0x00,0x00},//"c",67
	{0x00,0x00,0x03,0x80,0x04,0x40,0x24,0x40,0x3F,0xC0,0x00,0x40},//"d",68
	{0x00,0x00,0x03,0x80,0x05,0x40,0x05,0x40,0x03,0x40,0x00,0x00},//"e",69
	{0x00,0x00,0x04,0x40,0x1F,0xC0,0x24,0x40,0x24,0x40,0x20,0x00},//"f",70
	{0x00,0x00,0x02,0xE0,0x05,0x50,0x05,0x50,0x06,0x50,0x04,0x20},//"g",71
	{0x20,0x40,0x3F,0xC0,0x04,0x40,0x04,0x00,0x03,0xC0,0x00,0x40},//"h",72
	{0x00,0x00,0x04,0x40,0x27,0xC0,0x00,0x40,0x00,0x00,0x00,0x00},//"i",73
	{0x00,0x10,0x00,0x10,0x04,0x10,0x27,0xE0,0x00,0x00,0x00,0x00},//"j",74
	{0x20,0x40,0x3F,0xC0,0x01,0x40,0x07,0x00,0x04,0xC0,0x04,0x40},//"k",75
	{0x20,0x40,0x20,0x40,0x3F,0xC0,0x00,0x40,0x00,0x40,0x00,0x00},//"l",76
	{0x07,0xC0,0x04,0x00,0x07,0xC0,0x04,0x00,0x03,0xC0,0x00,0x00},//"m",77
	{0x04,0x40,0x07,0xC0,0x04,0x40,0x04,0x00,0x03,0xC0,0x00,0x40},//"n",78
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x40,0x03,0x80,0x00,0x00},//"o",79
	{0x04,0x10,0x07,0xF0,0x04,0x50,0x04,0x40,0x03,0x80,0x00,0x00},//"p",80
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x50,0x07,0xF0,0x00,0x10},//"q",81
	{0x04,0x40,0x07,0xC0,0x02,0x40,0x04,0x00,0x04,0x00,0x00,0x00},//"r",82
	{0x00,0x00,0x06,0x40,0x05,0x40,0x05,0x40,0x04,0xC0,0x00,0x00},//"s",83
	{0x00,0x00,0x04,0x00,0x1F,0x80,0x04,0x40,0x00,0x40,0x00,0x00},//"t",84
	{0x04,0x00,0x07,0x80,0x00,0x40,0x04,0x40,0x07,0xC0,0x00,0x40},//"u",85
	{0x04,0x00,0x07,0x00,0x04,0xC0,0x01,0x80,0x06,0x00,0x04,0x00},//"v",86
	{0x06,0x00,0x01,0xC0,0x07,0x00,0x01,0xC0,0x06,0x00,0x00,0x00},//"w",87
	{0x04,0x40,0x06,0xC0,0x01,0x00,0x06,0xC0,0x04,0x40,0x00,0x00},//"x",88
	{0x04,0x10,0x07,0x10,0x04,0xE0,0x01,0x80,0x06,0x00,0x04,0x00},//"y",89
	{0x00,0x00,0x04,0x40,0x05,0xC0,0x06,0x40,0x04,0x40,0x00,0x00},//"z",90
	{0x00,0x00,0x00,0x00,0x04,0x00,0x7B,0xE0,0x40,0x20,0x00,0x00},//"{",91
	{0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xF0,0x00,0x00,0x00,0x00},//"|",92
	{0x00,0x00,0x40,0x20,0x7B,0xE0,0x04,0x00,0x00,0x00,0x00,0x00},//"}",93
	{0x40,0x00,0x80,0x00,0x40,0x00,0x20,0x00,0x20,0x00,0x40,0x00},//"~",94
	*/
//}; 


const unsigned char c_chFont1608[95][16] = 
{	  
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//" ",0
	{0x00,0x00,0x00,0x00,0x00,0x00,0x1F,0xCC,0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00},//"!",1
	{0x00,0x00,0x08,0x00,0x30,0x00,0x60,0x00,0x08,0x00,0x30,0x00,0x60,0x00,0x00,0x00},//""",2
	{0x02,0x20,0x03,0xFC,0x1E,0x20,0x02,0x20,0x03,0xFC,0x1E,0x20,0x02,0x20,0x00,0x00},//"#",3
	{0x00,0x00,0x0E,0x18,0x11,0x04,0x3F,0xFF,0x10,0x84,0x0C,0x78,0x00,0x00,0x00,0x00},//"$",4
	{0x0F,0x00,0x10,0x84,0x0F,0x38,0x00,0xC0,0x07,0x78,0x18,0x84,0x00,0x78,0x00,0x00},//"%",5
	{0x00,0x78,0x0F,0x84,0x10,0xC4,0x11,0x24,0x0E,0x98,0x00,0xE4,0x00,0x84,0x00,0x08},//"&",6
	{0x08,0x00,0x68,0x00,0x70,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"'",7
	{0x00,0x00,0x00,0x00,0x00,0x00,0x07,0xE0,0x18,0x18,0x20,0x04,0x40,0x02,0x00,0x00},//"(",8
	{0x00,0x00,0x40,0x02,0x20,0x04,0x18,0x18,0x07,0xE0,0x00,0x00,0x00,0x00,0x00,0x00},//")",9
	{0x02,0x40,0x02,0x40,0x01,0x80,0x0F,0xF0,0x01,0x80,0x02,0x40,0x02,0x40,0x00,0x00},//"*",10
	{0x00,0x80,0x00,0x80,0x00,0x80,0x0F,0xF8,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x00},//"+",11
	{0x00,0x01,0x00,0x0D,0x00,0x0E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//",",12
	{0x00,0x00,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80},//"-",13
	{0x00,0x00,0x00,0x0C,0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//".",14
	{0x00,0x00,0x00,0x06,0x00,0x18,0x00,0x60,0x01,0x80,0x06,0x00,0x18,0x00,0x20,0x00},//"/",15
	{0x00,0x00,0x07,0xF0,0x08,0x08,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"0",16
	{0x00,0x00,0x08,0x04,0x08,0x04,0x1F,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"1",17
	{0x00,0x00,0x0E,0x0C,0x10,0x14,0x10,0x24,0x10,0x44,0x11,0x84,0x0E,0x0C,0x00,0x00},//"2",18
	{0x00,0x00,0x0C,0x18,0x10,0x04,0x11,0x04,0x11,0x04,0x12,0x88,0x0C,0x70,0x00,0x00},//"3",19
	{0x00,0x00,0x00,0xE0,0x03,0x20,0x04,0x24,0x08,0x24,0x1F,0xFC,0x00,0x24,0x00,0x00},//"4",20
	{0x00,0x00,0x1F,0x98,0x10,0x84,0x11,0x04,0x11,0x04,0x10,0x88,0x10,0x70,0x00,0x00},//"5",21
	{0x00,0x00,0x07,0xF0,0x08,0x88,0x11,0x04,0x11,0x04,0x18,0x88,0x00,0x70,0x00,0x00},//"6",22
	{0x00,0x00,0x1C,0x00,0x10,0x00,0x10,0xFC,0x13,0x00,0x1C,0x00,0x10,0x00,0x00,0x00},//"7",23
	{0x00,0x00,0x0E,0x38,0x11,0x44,0x10,0x84,0x10,0x84,0x11,0x44,0x0E,0x38,0x00,0x00},//"8",24
	{0x00,0x00,0x07,0x00,0x08,0x8C,0x10,0x44,0x10,0x44,0x08,0x88,0x07,0xF0,0x00,0x00},//"9",25
	{0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x0C,0x03,0x0C,0x00,0x00,0x00,0x00,0x00,0x00},//":",26
	{0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x06,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//";",27
	{0x00,0x00,0x00,0x80,0x01,0x40,0x02,0x20,0x04,0x10,0x08,0x08,0x10,0x04,0x00,0x00},//"<",28
	{0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x00,0x00},//"=",29
	{0x00,0x00,0x10,0x04,0x08,0x08,0x04,0x10,0x02,0x20,0x01,0x40,0x00,0x80,0x00,0x00},//">",30
	{0x00,0x00,0x0E,0x00,0x12,0x00,0x10,0x0C,0x10,0x6C,0x10,0x80,0x0F,0x00,0x00,0x00},//"?",31
	{0x03,0xE0,0x0C,0x18,0x13,0xE4,0x14,0x24,0x17,0xC4,0x08,0x28,0x07,0xD0,0x00,0x00},//"@",32
	{0x00,0x04,0x00,0x3C,0x03,0xC4,0x1C,0x40,0x07,0x40,0x00,0xE4,0x00,0x1C,0x00,0x04},//"A",33
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x04,0x11,0x04,0x0E,0x88,0x00,0x70,0x00,0x00},//"B",34
	{0x03,0xE0,0x0C,0x18,0x10,0x04,0x10,0x04,0x10,0x04,0x10,0x08,0x1C,0x10,0x00,0x00},//"C",35
	{0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"D",36
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x04,0x17,0xC4,0x10,0x04,0x08,0x18,0x00,0x00},//"E",37
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x00,0x17,0xC0,0x10,0x00,0x08,0x00,0x00,0x00},//"F",38
	{0x03,0xE0,0x0C,0x18,0x10,0x04,0x10,0x04,0x10,0x44,0x1C,0x78,0x00,0x40,0x00,0x00},//"G",39
	{0x10,0x04,0x1F,0xFC,0x10,0x84,0x00,0x80,0x00,0x80,0x10,0x84,0x1F,0xFC,0x10,0x04},//"H",40
	{0x00,0x00,0x10,0x04,0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x04,0x00,0x00,0x00,0x00},//"I",41
	{0x00,0x03,0x00,0x01,0x10,0x01,0x10,0x01,0x1F,0xFE,0x10,0x00,0x10,0x00,0x00,0x00},//"J",42
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x03,0x80,0x14,0x64,0x18,0x1C,0x10,0x04,0x00,0x00},//"K",43
	{0x10,0x04,0x1F,0xFC,0x10,0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x0C,0x00,0x00},//"L",44
	{0x10,0x04,0x1F,0xFC,0x1F,0x00,0x00,0xFC,0x1F,0x00,0x1F,0xFC,0x10,0x04,0x00,0x00},//"M",45
	{0x10,0x04,0x1F,0xFC,0x0C,0x04,0x03,0x00,0x00,0xE0,0x10,0x18,0x1F,0xFC,0x10,0x00},//"N",46
	{0x07,0xF0,0x08,0x08,0x10,0x04,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"O",47
	{0x10,0x04,0x1F,0xFC,0x10,0x84,0x10,0x80,0x10,0x80,0x10,0x80,0x0F,0x00,0x00,0x00},//"P",48
	{0x07,0xF0,0x08,0x18,0x10,0x24,0x10,0x24,0x10,0x1C,0x08,0x0A,0x07,0xF2,0x00,0x00},//"Q",49
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x00,0x11,0xC0,0x11,0x30,0x0E,0x0C,0x00,0x04},//"R",50
	{0x00,0x00,0x0E,0x1C,0x11,0x04,0x10,0x84,0x10,0x84,0x10,0x44,0x1C,0x38,0x00,0x00},//"S",51
	{0x18,0x00,0x10,0x00,0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x00,0x18,0x00,0x00,0x00},//"T",52
	{0x10,0x00,0x1F,0xF8,0x10,0x04,0x00,0x04,0x00,0x04,0x10,0x04,0x1F,0xF8,0x10,0x00},//"U",53
	{0x10,0x00,0x1E,0x00,0x11,0xE0,0x00,0x1C,0x00,0x70,0x13,0x80,0x1C,0x00,0x10,0x00},//"V",54
	{0x1F,0xC0,0x10,0x3C,0x00,0xE0,0x1F,0x00,0x00,0xE0,0x10,0x3C,0x1F,0xC0,0x00,0x00},//"W",55
	{0x10,0x04,0x18,0x0C,0x16,0x34,0x01,0xC0,0x01,0xC0,0x16,0x34,0x18,0x0C,0x10,0x04},//"X",56
	{0x10,0x00,0x1C,0x00,0x13,0x04,0x00,0xFC,0x13,0x04,0x1C,0x00,0x10,0x00,0x00,0x00},//"Y",57
	{0x08,0x04,0x10,0x1C,0x10,0x64,0x10,0x84,0x13,0x04,0x1C,0x04,0x10,0x18,0x00,0x00},//"Z",58
	{0x00,0x00,0x00,0x00,0x00,0x00,0x7F,0xFE,0x40,0x02,0x40,0x02,0x40,0x02,0x00,0x00},//"[",59
	{0x00,0x00,0x30,0x00,0x0C,0x00,0x03,0x80,0x00,0x60,0x00,0x1C,0x00,0x03,0x00,0x00},//"\",60
	{0x00,0x00,0x40,0x02,0x40,0x02,0x40,0x02,0x7F,0xFE,0x00,0x00,0x00,0x00,0x00,0x00},//"]",61
	{0x00,0x00,0x00,0x00,0x20,0x00,0x40,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x00,0x00},//"^",62
	{0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01},//"_",63
	{0x00,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"`",64
	{0x00,0x00,0x00,0x98,0x01,0x24,0x01,0x44,0x01,0x44,0x01,0x44,0x00,0xFC,0x00,0x04},//"a",65
	{0x10,0x00,0x1F,0xFC,0x00,0x88,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x70,0x00,0x00},//"b",66
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x00},//"c",67
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x11,0x08,0x1F,0xFC,0x00,0x04},//"d",68
	{0x00,0x00,0x00,0xF8,0x01,0x44,0x01,0x44,0x01,0x44,0x01,0x44,0x00,0xC8,0x00,0x00},//"e",69
	{0x00,0x00,0x01,0x04,0x01,0x04,0x0F,0xFC,0x11,0x04,0x11,0x04,0x11,0x00,0x18,0x00},//"f",70
	{0x00,0x00,0x00,0xD6,0x01,0x29,0x01,0x29,0x01,0x29,0x01,0xC9,0x01,0x06,0x00,0x00},//"g",71
	{0x10,0x04,0x1F,0xFC,0x00,0x84,0x01,0x00,0x01,0x00,0x01,0x04,0x00,0xFC,0x00,0x04},//"h",72
	{0x00,0x00,0x01,0x04,0x19,0x04,0x19,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"i",73
	{0x00,0x00,0x00,0x03,0x00,0x01,0x01,0x01,0x19,0x01,0x19,0xFE,0x00,0x00,0x00,0x00},//"j",74
	{0x10,0x04,0x1F,0xFC,0x00,0x24,0x00,0x40,0x01,0xB4,0x01,0x0C,0x01,0x04,0x00,0x00},//"k",75
	{0x00,0x00,0x10,0x04,0x10,0x04,0x1F,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"l",76
	{0x01,0x04,0x01,0xFC,0x01,0x04,0x01,0x00,0x01,0xFC,0x01,0x04,0x01,0x00,0x00,0xFC},//"m",77
	{0x01,0x04,0x01,0xFC,0x00,0x84,0x01,0x00,0x01,0x00,0x01,0x04,0x00,0xFC,0x00,0x04},//"n",78
	{0x00,0x00,0x00,0xF8,0x01,0x04,0x01,0x04,0x01,0x04,0x01,0x04,0x00,0xF8,0x00,0x00},//"o",79
	{0x01,0x01,0x01,0xFF,0x00,0x85,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x70,0x00,0x00},//"p",80
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x01,0x05,0x01,0xFF,0x00,0x01},//"q",81
	{0x01,0x04,0x01,0x04,0x01,0xFC,0x00,0x84,0x01,0x04,0x01,0x00,0x01,0x80,0x00,0x00},//"r",82
	{0x00,0x00,0x00,0xCC,0x01,0x24,0x01,0x24,0x01,0x24,0x01,0x24,0x01,0x98,0x00,0x00},//"s",83
	{0x00,0x00,0x01,0x00,0x01,0x00,0x07,0xF8,0x01,0x04,0x01,0x04,0x00,0x00,0x00,0x00},//"t",84
	{0x01,0x00,0x01,0xF8,0x00,0x04,0x00,0x04,0x00,0x04,0x01,0x08,0x01,0xFC,0x00,0x04},//"u",85
	{0x01,0x00,0x01,0x80,0x01,0x70,0x00,0x0C,0x00,0x10,0x01,0x60,0x01,0x80,0x01,0x00},//"v",86
	{0x01,0xF0,0x01,0x0C,0x00,0x30,0x01,0xC0,0x00,0x30,0x01,0x0C,0x01,0xF0,0x01,0x00},//"w",87
	{0x00,0x00,0x01,0x04,0x01,0x8C,0x00,0x74,0x01,0x70,0x01,0x8C,0x01,0x04,0x00,0x00},//"x",88
	{0x01,0x01,0x01,0x81,0x01,0x71,0x00,0x0E,0x00,0x18,0x01,0x60,0x01,0x80,0x01,0x00},//"y",89
	{0x00,0x00,0x01,0x84,0x01,0x0C,0x01,0x34,0x01,0x44,0x01,0x84,0x01,0x0C,0x00,0x00},//"z",90
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x3E,0xFC,0x40,0x02,0x40,0x02},//"{",91
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00},//"|",92
	{0x00,0x00,0x40,0x02,0x40,0x02,0x3E,0xFC,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"}",93
	{0x00,0x00,0x60,0x00,0x80,0x00,0x80,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x20,0x00},//"~",94
};

unsigned char c_chFont1608_app[10][16] = 
{	  
//	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//" ",0
	{0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA},// 화면에 픽셀이 찍히는지 테스트하기 위한 용도
};

uint8 appFontData = 0; // c_chFont1608_app에 표현할 글자를 몇자 가지고 있는지 저장
uint8 appFontDataIndex = 0;

/*
const unsigned char c_chFont1608[26][16] = 
{	  
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//" ",0
	{0x00,0x00,0x00,0x00,0x00,0x00,0x1F,0xCC,0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00},//"!",1
	{0x00,0x00,0x08,0x00,0x30,0x00,0x60,0x00,0x08,0x00,0x30,0x00,0x60,0x00,0x00,0x00},//""",2
	{0x02,0x20,0x03,0xFC,0x1E,0x20,0x02,0x20,0x03,0xFC,0x1E,0x20,0x02,0x20,0x00,0x00},//"#",3
	{0x00,0x00,0x0E,0x18,0x11,0x04,0x3F,0xFF,0x10,0x84,0x0C,0x78,0x00,0x00,0x00,0x00},//"$",4
	{0x0F,0x00,0x10,0x84,0x0F,0x38,0x00,0xC0,0x07,0x78,0x18,0x84,0x00,0x78,0x00,0x00},//"%",5
	{0x00,0x78,0x0F,0x84,0x10,0xC4,0x11,0x24,0x0E,0x98,0x00,0xE4,0x00,0x84,0x00,0x08},//"&",6
	{0x08,0x00,0x68,0x00,0x70,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"'",7
	{0x00,0x00,0x00,0x00,0x00,0x00,0x07,0xE0,0x18,0x18,0x20,0x04,0x40,0x02,0x00,0x00},//"(",8
	{0x00,0x00,0x40,0x02,0x20,0x04,0x18,0x18,0x07,0xE0,0x00,0x00,0x00,0x00,0x00,0x00},//")",9
	{0x02,0x40,0x02,0x40,0x01,0x80,0x0F,0xF0,0x01,0x80,0x02,0x40,0x02,0x40,0x00,0x00},//"*",10
	{0x00,0x80,0x00,0x80,0x00,0x80,0x0F,0xF8,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x00},//"+",11
	{0x00,0x01,0x00,0x0D,0x00,0x0E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//",",12
	{0x00,0x00,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80},//"-",13
	{0x00,0x00,0x00,0x0C,0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//".",14
	{0x00,0x00,0x00,0x06,0x00,0x18,0x00,0x60,0x01,0x80,0x06,0x00,0x18,0x00,0x20,0x00},//"/",15
	{0x00,0x00,0x07,0xF0,0x08,0x08,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"0",16
	{0x00,0x00,0x08,0x04,0x08,0x04,0x1F,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"1",17
	{0x00,0x00,0x0E,0x0C,0x10,0x14,0x10,0x24,0x10,0x44,0x11,0x84,0x0E,0x0C,0x00,0x00},//"2",18
	{0x00,0x00,0x0C,0x18,0x10,0x04,0x11,0x04,0x11,0x04,0x12,0x88,0x0C,0x70,0x00,0x00},//"3",19
	{0x00,0x00,0x00,0xE0,0x03,0x20,0x04,0x24,0x08,0x24,0x1F,0xFC,0x00,0x24,0x00,0x00},//"4",20
	{0x00,0x00,0x1F,0x98,0x10,0x84,0x11,0x04,0x11,0x04,0x10,0x88,0x10,0x70,0x00,0x00},//"5",21
	{0x00,0x00,0x07,0xF0,0x08,0x88,0x11,0x04,0x11,0x04,0x18,0x88,0x00,0x70,0x00,0x00},//"6",22
	{0x00,0x00,0x1C,0x00,0x10,0x00,0x10,0xFC,0x13,0x00,0x1C,0x00,0x10,0x00,0x00,0x00},//"7",23
	{0x00,0x00,0x0E,0x38,0x11,0x44,0x10,0x84,0x10,0x84,0x11,0x44,0x0E,0x38,0x00,0x00},//"8",24
	{0x00,0x00,0x07,0x00,0x08,0x8C,0x10,0x44,0x10,0x44,0x08,0x88,0x07,0xF0,0x00,0x00},//"9",25
	*/
	/*
	{0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x0C,0x03,0x0C,0x00,0x00,0x00,0x00,0x00,0x00},//":",26
	{0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x06,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//";",27
	{0x00,0x00,0x00,0x80,0x01,0x40,0x02,0x20,0x04,0x10,0x08,0x08,0x10,0x04,0x00,0x00},//"<",28
	{0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x00,0x00},//"=",29
	{0x00,0x00,0x10,0x04,0x08,0x08,0x04,0x10,0x02,0x20,0x01,0x40,0x00,0x80,0x00,0x00},//">",30
	{0x00,0x00,0x0E,0x00,0x12,0x00,0x10,0x0C,0x10,0x6C,0x10,0x80,0x0F,0x00,0x00,0x00},//"?",31
	{0x03,0xE0,0x0C,0x18,0x13,0xE4,0x14,0x24,0x17,0xC4,0x08,0x28,0x07,0xD0,0x00,0x00},//"@",32
	{0x00,0x04,0x00,0x3C,0x03,0xC4,0x1C,0x40,0x07,0x40,0x00,0xE4,0x00,0x1C,0x00,0x04},//"A",33
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x04,0x11,0x04,0x0E,0x88,0x00,0x70,0x00,0x00},//"B",34
	{0x03,0xE0,0x0C,0x18,0x10,0x04,0x10,0x04,0x10,0x04,0x10,0x08,0x1C,0x10,0x00,0x00},//"C",35
	{0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"D",36
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x04,0x17,0xC4,0x10,0x04,0x08,0x18,0x00,0x00},//"E",37
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x00,0x17,0xC0,0x10,0x00,0x08,0x00,0x00,0x00},//"F",38
	{0x03,0xE0,0x0C,0x18,0x10,0x04,0x10,0x04,0x10,0x44,0x1C,0x78,0x00,0x40,0x00,0x00},//"G",39
	{0x10,0x04,0x1F,0xFC,0x10,0x84,0x00,0x80,0x00,0x80,0x10,0x84,0x1F,0xFC,0x10,0x04},//"H",40
	{0x00,0x00,0x10,0x04,0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x04,0x00,0x00,0x00,0x00},//"I",41
	{0x00,0x03,0x00,0x01,0x10,0x01,0x10,0x01,0x1F,0xFE,0x10,0x00,0x10,0x00,0x00,0x00},//"J",42
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x03,0x80,0x14,0x64,0x18,0x1C,0x10,0x04,0x00,0x00},//"K",43
	{0x10,0x04,0x1F,0xFC,0x10,0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x0C,0x00,0x00},//"L",44
	{0x10,0x04,0x1F,0xFC,0x1F,0x00,0x00,0xFC,0x1F,0x00,0x1F,0xFC,0x10,0x04,0x00,0x00},//"M",45
	{0x10,0x04,0x1F,0xFC,0x0C,0x04,0x03,0x00,0x00,0xE0,0x10,0x18,0x1F,0xFC,0x10,0x00},//"N",46
	{0x07,0xF0,0x08,0x08,0x10,0x04,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"O",47
	{0x10,0x04,0x1F,0xFC,0x10,0x84,0x10,0x80,0x10,0x80,0x10,0x80,0x0F,0x00,0x00,0x00},//"P",48
	{0x07,0xF0,0x08,0x18,0x10,0x24,0x10,0x24,0x10,0x1C,0x08,0x0A,0x07,0xF2,0x00,0x00},//"Q",49
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x00,0x11,0xC0,0x11,0x30,0x0E,0x0C,0x00,0x04},//"R",50
	{0x00,0x00,0x0E,0x1C,0x11,0x04,0x10,0x84,0x10,0x84,0x10,0x44,0x1C,0x38,0x00,0x00},//"S",51
	{0x18,0x00,0x10,0x00,0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x00,0x18,0x00,0x00,0x00},//"T",52
	{0x10,0x00,0x1F,0xF8,0x10,0x04,0x00,0x04,0x00,0x04,0x10,0x04,0x1F,0xF8,0x10,0x00},//"U",53
	{0x10,0x00,0x1E,0x00,0x11,0xE0,0x00,0x1C,0x00,0x70,0x13,0x80,0x1C,0x00,0x10,0x00},//"V",54
	{0x1F,0xC0,0x10,0x3C,0x00,0xE0,0x1F,0x00,0x00,0xE0,0x10,0x3C,0x1F,0xC0,0x00,0x00},//"W",55
	{0x10,0x04,0x18,0x0C,0x16,0x34,0x01,0xC0,0x01,0xC0,0x16,0x34,0x18,0x0C,0x10,0x04},//"X",56
	{0x10,0x00,0x1C,0x00,0x13,0x04,0x00,0xFC,0x13,0x04,0x1C,0x00,0x10,0x00,0x00,0x00},//"Y",57
	{0x08,0x04,0x10,0x1C,0x10,0x64,0x10,0x84,0x13,0x04,0x1C,0x04,0x10,0x18,0x00,0x00},//"Z",58
	{0x00,0x00,0x00,0x00,0x00,0x00,0x7F,0xFE,0x40,0x02,0x40,0x02,0x40,0x02,0x00,0x00},//"[",59
	{0x00,0x00,0x30,0x00,0x0C,0x00,0x03,0x80,0x00,0x60,0x00,0x1C,0x00,0x03,0x00,0x00},//"\",60
	{0x00,0x00,0x40,0x02,0x40,0x02,0x40,0x02,0x7F,0xFE,0x00,0x00,0x00,0x00,0x00,0x00},//"]",61
	{0x00,0x00,0x00,0x00,0x20,0x00,0x40,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x00,0x00},//"^",62
	{0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01},//"_",63
	{0x00,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"`",64
	{0x00,0x00,0x00,0x98,0x01,0x24,0x01,0x44,0x01,0x44,0x01,0x44,0x00,0xFC,0x00,0x04},//"a",65
	{0x10,0x00,0x1F,0xFC,0x00,0x88,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x70,0x00,0x00},//"b",66
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x00},//"c",67
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x11,0x08,0x1F,0xFC,0x00,0x04},//"d",68
	{0x00,0x00,0x00,0xF8,0x01,0x44,0x01,0x44,0x01,0x44,0x01,0x44,0x00,0xC8,0x00,0x00},//"e",69
	{0x00,0x00,0x01,0x04,0x01,0x04,0x0F,0xFC,0x11,0x04,0x11,0x04,0x11,0x00,0x18,0x00},//"f",70
	{0x00,0x00,0x00,0xD6,0x01,0x29,0x01,0x29,0x01,0x29,0x01,0xC9,0x01,0x06,0x00,0x00},//"g",71
	{0x10,0x04,0x1F,0xFC,0x00,0x84,0x01,0x00,0x01,0x00,0x01,0x04,0x00,0xFC,0x00,0x04},//"h",72
	{0x00,0x00,0x01,0x04,0x19,0x04,0x19,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"i",73
	{0x00,0x00,0x00,0x03,0x00,0x01,0x01,0x01,0x19,0x01,0x19,0xFE,0x00,0x00,0x00,0x00},//"j",74
	{0x10,0x04,0x1F,0xFC,0x00,0x24,0x00,0x40,0x01,0xB4,0x01,0x0C,0x01,0x04,0x00,0x00},//"k",75
	{0x00,0x00,0x10,0x04,0x10,0x04,0x1F,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"l",76
	{0x01,0x04,0x01,0xFC,0x01,0x04,0x01,0x00,0x01,0xFC,0x01,0x04,0x01,0x00,0x00,0xFC},//"m",77
	{0x01,0x04,0x01,0xFC,0x00,0x84,0x01,0x00,0x01,0x00,0x01,0x04,0x00,0xFC,0x00,0x04},//"n",78
	{0x00,0x00,0x00,0xF8,0x01,0x04,0x01,0x04,0x01,0x04,0x01,0x04,0x00,0xF8,0x00,0x00},//"o",79
	{0x01,0x01,0x01,0xFF,0x00,0x85,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x70,0x00,0x00},//"p",80
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x01,0x05,0x01,0xFF,0x00,0x01},//"q",81
	{0x01,0x04,0x01,0x04,0x01,0xFC,0x00,0x84,0x01,0x04,0x01,0x00,0x01,0x80,0x00,0x00},//"r",82
	{0x00,0x00,0x00,0xCC,0x01,0x24,0x01,0x24,0x01,0x24,0x01,0x24,0x01,0x98,0x00,0x00},//"s",83
	{0x00,0x00,0x01,0x00,0x01,0x00,0x07,0xF8,0x01,0x04,0x01,0x04,0x00,0x00,0x00,0x00},//"t",84
	{0x01,0x00,0x01,0xF8,0x00,0x04,0x00,0x04,0x00,0x04,0x01,0x08,0x01,0xFC,0x00,0x04},//"u",85
	{0x01,0x00,0x01,0x80,0x01,0x70,0x00,0x0C,0x00,0x10,0x01,0x60,0x01,0x80,0x01,0x00},//"v",86
	{0x01,0xF0,0x01,0x0C,0x00,0x30,0x01,0xC0,0x00,0x30,0x01,0x0C,0x01,0xF0,0x01,0x00},//"w",87
	{0x00,0x00,0x01,0x04,0x01,0x8C,0x00,0x74,0x01,0x70,0x01,0x8C,0x01,0x04,0x00,0x00},//"x",88
	{0x01,0x01,0x01,0x81,0x01,0x71,0x00,0x0E,0x00,0x18,0x01,0x60,0x01,0x80,0x01,0x00},//"y",89
	{0x00,0x00,0x01,0x84,0x01,0x0C,0x01,0x34,0x01,0x44,0x01,0x84,0x01,0x0C,0x00,0x00},//"z",90
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x3E,0xFC,0x40,0x02,0x40,0x02},//"{",91
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00},//"|",92
	{0x00,0x00,0x40,0x02,0x40,0x02,0x3E,0xFC,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"}",93
	{0x00,0x00,0x60,0x00,0x80,0x00,0x80,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x20,0x00},//"~",94
	*/
//};
	
/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse 
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

#if( TWI0_ENABLED + TWI1_ENABLED ) > 0
static ret_code_t twi_master_init(void)
{
	
    ret_code_t ret;
	
	/*
    const nrf_drv_twi_config_t config =
    {
       .scl                = TWI_SCL_M,
       .sda                = TWI_SDA_M,
       .frequency          = NRF_TWI_FREQ_400K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };
	*/
	
	///*
    const nrf_drv_twi_config_t config =
    {
       .scl                = EEPROM_SIM_SCL_S,
       .sda                = EEPROM_SIM_SDA_S,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };	
	//*/

    do
    {
        ret = nrf_drv_twi_init(&m_twi_master, &config, NULL);
        if(NRF_SUCCESS != ret)
        {
            break;
        }
        nrf_drv_twi_enable(&m_twi_master);
    }while(0);
    return ret;
}
#endif

/*
static void print_addr(size_t addr)
{
    printf("%.2x: \n", (unsigned int)addr);
}

static void print_hex(uint8_t data)
{
    printf("%.2x ", (unsigned int)data);
}
*/


static void main_timeout_handler(void * p_context) {
 		size_t addr = 2;
		uint8_t buff[IN_LINE_PRINT_CNT]; 
	
	activity_save_timer += 1;

	// todo 69
    // 활동정보를 보내지 않을때만, 정보를 수집하자.	
	if( SEND_ACT_DATA == false ) {
		if( activity_save_timer >= 69 ) {// 1s // if( activity_save_timer >= 75 ) { // 200ms마다 들어오니, 75면 15s다.			
			countRealActBackup = countRealAct;
			countRealAct = 0;
			save_activity_count = true;		
			activity_save_timer = 0;
		}
	}
		
		uint32_t err_code;
		err_code = eeprom_read(addr, buff, IN_LINE_PRINT_CNT);
		//*/ fff
		
		// acc x, y, z의 값을 2의 보수로 보여줌 
		/*
		printf("sen : %x, %x, %x, %x, %x, %x\n", buff[0], buff[1], buff[2],buff[3], buff[4], buff[5]);		

		acc_x = ((((signed short)buff[1]) << 8) | (buff[0]) & 0xF0);
		acc_y = ((((signed short)buff[3]) << 8) | (buff[2]) & 0xF0);
		acc_z = ((((signed short)buff[5]) << 8) | (buff[4]) & 0xF0);
		
		acc_x = acc_x >> 4;
		acc_y = acc_y >> 4;
		acc_z = acc_z >> 4;
		
		printf("acc_x, y, z = %d, %d, %d\n", acc_x, acc_y, acc_z);
		*/
		
		acc_x = ((((signed short)buff[1]) << 8) | (buff[0]) & 0xF0);
		acc_y = ((((signed short)buff[3]) << 8) | (buff[2]) & 0xF0);
		acc_z = ((((signed short)buff[5]) << 8) | (buff[4]) & 0xF0);
		
		acc_x = acc_x >> 4;
		acc_y = acc_y >> 4;
		acc_z = acc_z >> 4;
		
		///*
		if( STOP_VIBE == true ) {
			stopVibeCount++;
			
			if( stopVibeCount >= 3 ) {
				stopVibeCount = 0;
				STOP_VIBE = false;
				
				printf("pwm_stop_A \n");
				
				pwm_stop_A();
			}
		}
		//*/
		
		if( vibeNew == true ) {
			vibeNew = false;
			printf("in vibeNew \n");
			
			vibeData = vibePattern[ vibePatternIndex ];
			
			printf("in vibeNew, %x\n", vibeData);
			
			if( vibePattern[vibePatternIndex] != 0 ) {
				if( vibeData & 0x80 ) {
					isVibeOn = true;
					printf("in vibeNew, vibe ON \n");
				}
				else {
					isVibeOn = false;
					printf("in vibeNew, vibe OFF \n");
				}
				
				vibePatternCount = vibePattern[ vibePatternIndex ] & 0x7F;
				
				printf("in vibeNew, count : %d\n", vibePatternCount);
			}
			else {
				printf("in vibeNew 0 \n");
			}
		}
		
		
		if( vibePatternCount > 0 ) {
			if( isVibeOn == true ) {  // 진동을 해야한다.
				//IS_PWM_ENABLE = true;
				//PWM_ON = true;
				// TODO
				// 이 함수가 main()에 있는 상태에서 OLED에 전화번호가 scrolling되는 상황이면
				// 이 함수를 태울수없는 문제가 발생한다. 왜 그러지?			
				pwm_start_A();
			}
			else {                    // 진동을 멈춘다.
				//IS_PWM_ENABLE = true;
				//PWM_ON = false;
				pwm_stop_A();
			}
			
			vibePatternCount--;
			
			if( vibePatternCount <= 0 ) {
				
				isVibeOn = false;
				vibePatternIndex++;
				
				printf("get new vibe pattern %d\n", vibePatternIndex);
				
				if( vibePatternIndex < 15 ) {
					printf("get new vibe pattern < 15\n");
					vibeNew = true;				
				}
				else {
					printf("get new vibe pattern equal 15 or over\n");
					vibeNew = false;
					vibePatternIndex = 0;
					memset(&vibePattern, 0, sizeof(vibePattern));	
				}
			}
		}
		
		///*
		// 진동 preset 들어왔을때
		if( ING_PWM == true ) {
			if( motorStatusChangeCount == 0 ) {
				IS_PWM_ENABLE = true;
				PWM_ON = true;
			}
			motorStatusChangeCount++;
			
			if( motorStatusChangeCount >= 10 ) {
				printf("motor end\n");
				
				if( motorStatusChangeCount == 10 ) {
				IS_PWM_ENABLE = true;
				PWM_ON = false;
			}
				
				motorStatusChangeCount = 0;
				
				ING_PWM = false;							
			}
		}
		//*/
		
		//////////////////
		// Flash			
		
		if( IS_SHOW_CALL_NUM == true ) {
			callResetCount++;
			
			if( callResetCount >= 25) {
				printf("XX\n");
				callResetCount = 0;
				IS_SHOW_CALL_NUM = false;
				//SHOW_OLED_STEP_FLAG = true;
			}
		}	
		
		uint8 iLoop = 0;
		uint16 remainCount = 0;
		
		if( passHandler == true) {
			//printf("pass timer. nothing to do\n");
			//readRun = true;
			
			if( stepCheck == true ) {
				// 여기에서 Acceleration 값을 넣자.
				//now = time(NULL);
				
				// 기존 알고리즘 
				//resultStep = count_step(dataTimeStamp.data_count, acc_x, acc_y, acc_z);
				
				retStepCount = update(&stat, acc_x, acc_y, acc_z);
				
				// 걸음수가 발생하면 실시간 걸음 상태일때, 걸음수를 앱으로 보내자.
				if( retStepCount != 0 ) {
					EVT_STEP = true;
				}
				
				countRealAct += retStepCount;
				
				totalStepCountBackup = totalStepCount;
				totalStepCount += retStepCount;
				
				if( totalStepCountBackup != totalStepCount ) {							
					SHOW_OLED_STEP_FLAG = true;
				}
				
				
				
				dataTimeStamp.data_count += 20;
				
				//printf("acc_x, y, z = %d, %d, %d, %u\n", acc_x, acc_y, acc_z, dataTimeStamp.data_count);
				
				//printf("step : %u\n", resultStep);
//				printf("step : %u, %u\n", retStepCount, totalStepCount);	 // step을 보여준다.

				//SHOW_OLED_STEP_FLAG = true;
			}
		}
		else {
			/*
			for(iLoop = 0; iLoop < 6; iLoop++) {
				rcvBuffer[iLoop] = (uint8_t)('a' + iLoop);
			}
			*/
			
			memcpy(&rcvBuffer, &buff, 6);
			
//			printf("\n\nen : %x, %x, %x, %x, %x, %x\n", rcvBuffer[0], rcvBuffer[1], rcvBuffer[2], rcvBuffer[3], rcvBuffer[4], rcvBuffer[5]);
			
			enqueue( rcvBuffer, 6 );
			
			if(dataSendBuffer.rear >= dataSendBuffer.front) {
				remainCount = dataSendBuffer.rear - dataSendBuffer.front;   
				///*
			//				printf("CH : dequeue 11 read count : %u\n", remainCount);
				//*/
			}
			// front가 큰 경우는 rear데이터가 버퍼의 끝을 넘어서 다시 처음으로 간 경우다. 
			else {
				remainCount = MAX_QUEUE_SIZE - dataSendBuffer.front + dataSendBuffer.rear;
				/*
				printf("CH : dequeue 22 read count : %u\n", remainCount);
				*/
			}
			
//			printf("save count : %u\n", remainCount);

			if((remainCount >= 100) && (dataSendBuffer.sending == false)) {
				dataSendBuffer.sending = true;
			}			
		}
	//}				
}

static void updataTime(uint8 gap) {
	printf("updataTime 100 \n");
	
    dataTimeStamp.time_ss += gap;
    
    /*
      날짜(년, 월, 일)는 변경하지 않도록 하자. 
      월마다 날수(28일, 29일, 30일, 31일)가 다르다. 
      윤년, 윤월 등등 그런것까지 모두 판별하기가 힘들다. 
      
      일단, 년, 월, 일은 사용하지 않는걸로 하고 시간만 계산하도록 하자. 
      */
    if( dataTimeStamp.time_ss >= 60 ) {
        dataTimeStamp.time_ss -= 60;
        
        dataTimeStamp.time_mm += 1;
        
        if( dataTimeStamp.time_mm >= 60 ) {
            dataTimeStamp.time_mm -= 60;
            
            dataTimeStamp.time_hh += 1;
            
            if( dataTimeStamp.time_hh >= 24 ) {
                dataTimeStamp.time_hh -= 24;                               
            }
        }
    }
	
	printf("current time : %d : %d : %d\n", dataTimeStamp.time_hh, dataTimeStamp.time_mm, dataTimeStamp.time_ss);
	
	sprintf(strCurruntTime, "%02d:%02d", dataTimeStamp.time_hh, dataTimeStamp.time_mm);
	
	printf("time : %s\n", strCurruntTime);
	
	// 초는 OLED에 표시가 안되니 계산하지 말자. 분단위까지만 나오는데, 보이지 않는 초가 변경되었다고 갱신하면 
	// 필요없는 갱신이 생긴다.
	dataTimeStamp.timeTotal = dataTimeStamp.time_hh + dataTimeStamp.time_mm; // + dataTimeStamp.time_ss; 
	
	printf("time total : %d, %d\n", dataTimeStamp.timeTotal, dataTimeStamp.timeTotalBackup );
	
	if( dataTimeStamp.timeTotalBackup != dataTimeStamp.timeTotal ) {
		printf("time modi\n");
		SHOW_CURRENT_TIME = true;		
		dataTimeStamp.timeTotalBackup = dataTimeStamp.timeTotal;		
	}		
	else {
		printf("time same\n");
	}
}

static void noti_timeout_handler(void * p_context) {
	uint16 err_code;
	
	printf("noti_timeout_handler \n");
	
	if( (dataTimeStamp.time_hh == 0xff ) && (dataTimeStamp.time_mm == 0xff) ) {
		printf("clock is not set \n");
	}
	else {
		updataTime(10);
	}
	
	//LEDS_OFF(BSP_LED_3_MASK);
	
	//err_code = app_timer_stop(noti_timer_id);
//	printf("noti_timeout_handler, %d\n", err_code);
}


static void save_timeout_handler(void * p_context) {
	uint16 remainCount = 0;
	uint16 err_code;
	
//	printf("******* save timeout *******\n");
	
	if( dataSendBuffer.sending == true ) {
		if( isSaveData == false) {
			if(is_empty()) { 
				printf("Queue is empty\n");
			}
			else {
				if(dataSendBuffer.rear >= dataSendBuffer.front) {
					remainCount = dataSendBuffer.rear - dataSendBuffer.front;   
					///*
				//				printf("CH : dequeue 11 read count : %u\n", remainCount);
					//*/
				}
				// front가 큰 경우는 rear데이터가 버퍼의 끝을 넘어서 다시 처음으로 간 경우다. 
				else {
					remainCount = MAX_QUEUE_SIZE - dataSendBuffer.front + dataSendBuffer.rear;
					/*
					printf("CH : dequeue 22 read count : %u\n", remainCount);
					*/
				}
				
				printf("save count : %u\n", remainCount);

				if(remainCount >= 36) {
					//dataSendBuffer.sending = TRUE;  
					printf("save - run dequeue \n");		
					dequeue();
					
					///*
					err_code = app_timer_stop(save_timer_id);
					//APP_ERROR_CHECK(err_code);
					
					err_code = app_timer_start(save_timer_id, MAIN_TIMER_INTERVAL, NULL);
					printf("start : %d\n", err_code);	
					//*/
					
				}	
				else {
					dataSendBuffer.sending = FALSE;  
					//printf("* save timeout, noting < 20 *\n");
				}
			} 
		}
		else {
		//	printf("save handler : sending is true, saving is processing\n");
		}			
	}
	else {
//		printf("save handler : sending is FALSE\n");
	}
	
}

static void savedDataSend(void) {
    printf("savedDataSend \r\n");
	
	readRun = true;
    
    /*
    DEBUG_MSG_STR("accExtTimer : ");
    DEBUG_MSG_UINT16(g_acc_data.dataSize);
    DEBUG_MSG_STR(" , ");
    DEBUG_MSG_UINT16(g_acc_data.dataSizeRead );
    DEBUG_MSG_STR(" .. \r\n");
    */    
    
	/*
    if( g_acc_data.dataSize > g_acc_data.dataSizeRead ) {        
        TimerDelete(accExtTimerID);
        accExtTimerID = TimerCreate(ACC_TIMER, TRUE, accExtTimerHandler);
    }
    else {
        DEBUG_MSG_STR("no data for send \r\n");
        statusNoti(0x3);
    }
	*/
	
	passHandler = true;
}

static void savedDataClear(void) {
	uint8 iLoop;
	
	printf("Clear \r\n");
	
	ssd1306_clear_screen(0x00);
	ssd1306_display_string(10, 10, "Clear...", 12, 1);
	ssd1306_refresh_gram( DIS_MULTI_BYTE );	
	
	nrf_delay_ms(DELAY_MS + 30);
	
	
    flash_addr = FLASH_TARGET_ADDR;
	flash_addr_r = FLASH_TARGET_ADDR;
	
	
	///*
	// Block Erase(64K)
	// 64M 전체 블럭을 삭제한다.
	for( iLoop = 0; iLoop < 128; iLoop++) {	 //
		m_tx_data[0] = 0x06;
		spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
		nrf_delay_ms(DELAY_MS);
		
		m_tx_data[0] = 0xD8;		
		m_tx_data[1] = flash_addr >> 16;
		m_tx_data[2] = flash_addr >> 8;
		m_tx_data[3] = flash_addr;	
		
		printf("block add : %d, %x, %x, %x\n", iLoop, m_tx_data[1], m_tx_data[2], m_tx_data[3]);
		spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_W ); // TX_RX_BUF_LENGTH		
		
		flash_addr += 0x10000;
		nrf_delay_ms(DELAY_MS);
	}			  

	flash_addr = FLASH_TARGET_ADDR;
	
	notiFlashEraseComplete();
	sensorModeChange(0);
	SHOW_OLED_STEP_FLAG = true;  // 전체 메모리 초기화후 스텝카운트를 보여주도록 하자.
	//*/
}

static void sensorModeChange(uint8 mode) {
    //printf("mode change \r\n");
    
	// 공통 설정
	REAL_STEP = false;	
	
	// 4는 실시간 걸음수다. 일단 기본적으로는 스텝카운트 체크가 기본이고,
	// 걸음수가 올라가면 BT로 올라간 수를 전송하는것만 추가하면 된다.
    if( mode == 0 || mode == 4 ) { // 스텝카운트
        printf("stepcount \r\n");
		
		passHandler = true;
		readRun = false;
		stepCheck = true;
        //sensorMode = 0;
        //TimerDelete(accTimerID);
        
        //readTest();					
		
		if( mode == 4 ) {
			printf("mode : realtime step count\n");		
			REAL_STEP = true;			
		}
    }
    else if( mode == 1 ) { // 실시간 raw data 측정
        printf("realtime \r\n");
        //sensorMode = 1;
        //TimerDelete(accTimerID);
        //accTimerID = TimerCreate(ACC_TIMER, TRUE, accTimerHandler);
        
       //   writeTest();
				
		
		passHandler = true;
		readRun = false;
		stepCheck = false;
    }
    else if( mode == 2 ) { // 수면모드
        //sensorMode = 2;
        //TimerDelete(accTimerID);
        printf("sleep \r\n");
		
		passHandler = true;
		readRun = false;
		stepCheck = false;
        
        //writeTest(); // 빌드가 되지 않아서 여기에서 한 번 호출함. 의미없는 코드임
    }
    else if( mode == 3 ) { // 실시간 raw data 메모리에 저장
		printf("sleep save \r\n");
		
		readRun = false;
		passHandler = false;
		//stepCheck = false;
		
		// 
		
		
      //  if( sensorMode != 3 ) {
        //    sensorMode = 3;
        
           // DEBUG_MSG_STR("mode change to raw to ext : ");
          ////  DEBUG_MSG_UINT16( g_acc_data.dataSize );
          //  DEBUG_MSG_STR(" .\r\n ");
            
            
          //  TimerDelete(accTimerID);
           // accTimerID = TimerCreate(ACC_TIMER, TRUE, accTimerHandler);
       // }
        //else {
          //  printf("already mode 3 \r\n");
            //statusNoti(0x4);
        //}
    }
}


/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of 
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);
	
	// pairing										  
	err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_KEYRING);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_nus    Nordic UART Service structure.
 * @param[in] p_data   Data to be send to UART module.
 * @param[in] length   Length of the data.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t length)
{
	uint16 err_code;
	//printf("nus_data_handler\n");
	
	/*
    for (uint32_t i = 0; i < length; i++)
    {
		printf("data is %x\n", p_data[i]);
        while(app_uart_put(p_data[i]) != NRF_SUCCESS);
    }
    while(app_uart_put('\n') != NRF_SUCCESS);
	*/
	LEDS_ON(BSP_LED_3_MASK);
	err_code = app_timer_start(noti_timer_id, MAIN_TIMER_INTERVAL, NULL);
	printf("start : %d\n", err_code);
	
	printf("packet, %x, %x, %x\n", p_data[0], p_data[1], p_data[2]);
	
	nrf_delay_ms(DELAY_MS);
	
	if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0x12 ) && ( p_data[2] == 0x01 ) ) { // 가속센서 모드 변경
        if( p_data[3] == 0x00 ) {  // 스텝카운트 측정
            sensorModeChange(0);
        }
        else if( p_data[3] == 0x01 ) { // 실시간 raw data 측정
            sensorModeChange(1);			
        }
        else if( p_data[3] == 0x02 ) { // 수면 측정
            sensorModeChange(2);
        }
        else if( p_data[3] == 0x03 ) { // 실시간 raw data 메모리에 저장
            sensorModeChange(3);
        }
		else if( p_data[3] == 0x4 ) { // 실시간 걸음수
            sensorModeChange(4);
        }
    }    
    else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xC9 ) && ( p_data[2] == 0x01 ) ) { // 모션센서 주기 변경
        if( p_data[3] == 0x01 ) {  // 7.81Hz
//            Bml055SetBW( BMI055_ACC_BW_1 );
        }
        else if( p_data[3] == 0x02 ) { // 15.63Hz
        //    Bml055SetBW( BMI055_ACC_BW_2 );
        }
        else if( p_data[3] == 0x03 ) { // 31.25Hz
        //    Bml055SetBW( BMI055_ACC_BW_3 );
        }
        else if( p_data[3] == 0x04 ) { // 62.5Hz
         //   Bml055SetBW( BMI055_ACC_BW_4 );
        }
        else if( p_data[3] == 0x05 ) { // 125Hz
         //   Bml055SetBW( BMI055_ACC_BW_5 );
        }
        else if( p_data[3] == 0x06 ) { // 250Hz
         //   Bml055SetBW( BMI055_ACC_BW_6 );
        }
        else if( p_data[3] == 0x07 ) { // 500Hz
         //   Bml055SetBW( BMI055_ACC_BW_7 );
        }
        else if( p_data[3] == 0x08 ) { // 1000Hz
         //   Bml055SetBW( BMI055_ACC_BW_8 );
        }
    } 
    
    else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xC8 ) && ( p_data[2] == 0x0 ) ) { // Data Symc 테스트용
        //printf("cmd : call \r\n ");
        //DataSync();
        savedDataSend();
    }
    else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xCB ) && ( p_data[2] == 0x0 ) ) { // 저장된 데이터수 요청
        //printf("cmd : saved data count \r\n ");
        //DataSync();
     //   savedDataCountSend();
    }
    else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0x17 ) && ( p_data[2] == 0x0 ) ) { // 데이터 초기화
        //printf("cmd : data clear \r\n ");       
//        savedDataClear();
		CMD_MEMORY_CLEAR = true;
    }
    else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xC ) && ( p_data[2] == 0x6 ) ) { // 시간설정
		printf("cmd : set time \r\n ");
		
		dataTimeStamp.time_hh = p_data[6]; // 시
		dataTimeStamp.time_mm = p_data[7]; // 분
		dataTimeStamp.time_ss = p_data[8]; // 초
    } 
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xCE ) && ( p_data[2] == 0xE ) ) { // 전화알림
		printf("incoming call\n");			
		
		IS_SHOW_CALL_NUM = true;
		
		if( p_data[16] == 0 ) {
			printf("incoming call, end\n");
		}
		else {
			printf("incoming call, start\n");
		}
		
//		printf("call : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x", p_data[3], p_data[4], p_data[5], p_data[6], 
//				p_data[7], p_data[8], p_data[9], p_data[10], p_data[11], p_data[12], p_data[13], p_data[14], p_data[15]);
				
		memset(phone_num, 0, sizeof(phone_num));
		memcpy(phone_num, &p_data[3], 13);
		
		IS_CALL_NUM_SCROLLING = true; 
		
		SHOW_CALL_NUM = true;
		
		//pwm_start_A();
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xCF ) && ( p_data[2] == 0x1 ) ) { // 진동(프리셋)
		printf("vibe(preset)\n");	
		
		ING_PWM = true;
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD0 ) && ( p_data[2] == 0x1 ) ) { // 비트맵 폰트전송(16 by 8, 테스트용) 전송시작알림
		
		appFontData = p_data[3];
		appFontDataIndex = 0;
		
		printf("16 by 8 font start %d\n", appFontData);	
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD2 ) && ( p_data[2] == 0x0 ) ) { // 비트맵 폰트전송(16 by 8, 테스트용) 전송완료알림
		printf("16 by 8 font end\n");	
		SHOW_16_8 = true;
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD1 ) && ( p_data[2] == 0x10 ) ) { // 비트맵 폰트전송(16 by 8, 테스트용)
		printf("16 by 8 font data\n");	
		
		c_chFont1608_app[appFontDataIndex][0] = p_data[3];
		c_chFont1608_app[appFontDataIndex][1] = p_data[4];
		c_chFont1608_app[appFontDataIndex][2] = p_data[5];
		c_chFont1608_app[appFontDataIndex][3] = p_data[6];
		c_chFont1608_app[appFontDataIndex][4] = p_data[7];
		c_chFont1608_app[appFontDataIndex][5] = p_data[8];
		c_chFont1608_app[appFontDataIndex][6] = p_data[9];
		c_chFont1608_app[appFontDataIndex][7] = p_data[10];
		c_chFont1608_app[appFontDataIndex][8] = p_data[11];
		c_chFont1608_app[appFontDataIndex][9] = p_data[12];
		c_chFont1608_app[appFontDataIndex][10] = p_data[13];
		c_chFont1608_app[appFontDataIndex][11] = p_data[14];
		c_chFont1608_app[appFontDataIndex][12] = p_data[15];
		c_chFont1608_app[appFontDataIndex][13] = p_data[16];
		c_chFont1608_app[appFontDataIndex][14] = p_data[17];
		c_chFont1608_app[appFontDataIndex][15] = p_data[18];
		
		appFontDataIndex++;
		
		//appFontData = 1;
		
		//ssd1306_display_char(5, 8, &c_chFont1608_app[0][0], 16, 1);
		
		//SHOW_16_8 = true;
		//ssd1306_display_char_app(5, 8, 0, 16, 1);
	}	
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD3 ) && ( p_data[2] == 0x10 ) ) { // vibe custom
		printf("vibe custom\n");	
		// xxx = p_data[3]; // 전체 repeat 수
		vibePattern[0]  = p_data[4];
		vibePattern[1]  = p_data[5];
		vibePattern[2]  = p_data[6];
		vibePattern[3]  = p_data[7];
		vibePattern[4]  = p_data[8];
		vibePattern[5]  = p_data[9];
		vibePattern[6]  = p_data[10];
		vibePattern[7]  = p_data[11];
		vibePattern[8]  = p_data[12];
		vibePattern[9]  = p_data[13];
		vibePattern[10] = p_data[14];
		vibePattern[11] = p_data[15];
		vibePattern[12] = p_data[16];
		vibePattern[13] = p_data[17];
		vibePattern[14] = p_data[18];
		
		vibeNew = true;
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD4 ) && ( p_data[2] == 0x0 ) ) { // 배터리 잔량
		printf("battery check\n");	
		
		SEND_BATTERY = true;	
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD5 ) && ( p_data[2] == 0x5 ) ) { // 사용자 정보입력
		unsigned int user_w_input = 0;
		unsigned int user_h_input = 0;
		double user_w = 0;
		double user_h = 0;
		// [4][5] 키
		// [6][7] 몸무게
		
		// 몸무게
		printf("xx %d, %d\n", p_data[6], p_data[7]);		
		user_w_input = p_data[6] << 8;
		printf("weight 1, %u\n", user_w_input);
		user_w_input = user_w_input | p_data[7];
		printf("weight 2, %u\n", user_w_input);
		
		// 키
		printf("xx %d, %d\n", p_data[4], p_data[5]);		
		user_h_input = p_data[4] << 8;
		printf("height 1, %u\n", user_h_input);
		user_h_input = user_h_input | p_data[5];
		printf("height 2, %u\n", user_h_input);
		
		user_w = (double)user_w_input;
		user_h = (double)user_h_input;
		
		// 사용자 정보 설정
		setInfoWeight(user_w, CONV_KG);
		setInfoHeight(user_h, CONV_CM);
		setInfoSex(p_data[3]);				
		
		showManInfo();
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD6 ) && ( p_data[2] == 0x9 ) ) { // 기본 목표 설정		
		printf("goal, %d, %d, %d\n", p_data[4], p_data[5], p_data[6]);
		goalInfo.goal_step = p_data[4] << 16;                         // 목표 걸음수
		printf("goal 1, %u\n", goalInfo.goal_step);
		goalInfo.goal_step =  goalInfo.goal_step | (p_data[5] << 8);  // 목표 걸음수
		printf("goal 2, %u\n", goalInfo.goal_step);
		goalInfo.goal_step =  goalInfo.goal_step | p_data[6];         // 목표 걸음수
		printf("goal 3, %u\n", goalInfo.goal_step);
		
		printf("goal cal, %d, %d, %d\n", p_data[9], p_data[10], p_data[11]);
		goalInfo.goal_calory = p_data[9] << 16;    // 목표 칼로리
		printf("cal 1, %u\n", goalInfo.goal_calory);
		goalInfo.goal_calory = goalInfo.goal_calory | (p_data[10] << 8);    // 목표 칼로리
		printf("cal 2, %u\n", goalInfo.goal_calory);
		goalInfo.goal_calory = goalInfo.goal_calory | p_data[11];    // 목표 칼로리
		printf("cal 3, %u\n", goalInfo.goal_calory);
		
		printf("goal dis, %d, %d\n", p_data[7], p_data[8]);
		goalInfo.goal_distance = p_data[7] << 8;  // 목표 거리
		printf("dis 1, %u\n", goalInfo.goal_distance);
		goalInfo.goal_distance = goalInfo.goal_distance | p_data[8];  // 목표 거리
		printf("dis 2, %u\n", goalInfo.goal_distance);
		
		goalInfo.goal_default = p_data[3]; // 기본목표
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD7 ) && ( p_data[2] == 0x2 ) ) { // 활동 데이터 가져가기
		printf("get act data\n");
		
		SEND_ACT_DATA = true;
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD8 ) && ( p_data[2] == 0x0 ) ) { // 데이터 초기화(제품용)
		printf("clear data\n");
		
		CLEAR_ACT_DATA = true;
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xDA ) && ( p_data[2] == 0x0 ) ) { // 펌웨어 버전 가져오기
		printf("get firmware version\n");
		
		GET_FIRM_VER = true;
	}
	else {
		// 지원하지 않는 커맨드다. 앱에 해당 상태를 전송하자.
		notiStateCode = 0x9;
		SEND_NOTI_TO_APP = true;
	}
}
/**@snippet [Handling the data received over BLE] */


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t       err_code;
    ble_nus_init_t nus_init;
    
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;
    
    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
	
	// for dfu
	///*
	
	
    // Initialize the Device Firmware Update Service.
    memset(&dfus_init, 0, sizeof(dfus_init));

    dfus_init.evt_handler   = dfu_app_on_dfu_evt;
    dfus_init.error_handler = NULL;
    dfus_init.evt_handler   = dfu_app_on_dfu_evt;
    dfus_init.revision      = DFU_REVISION;

    err_code = ble_dfu_init(&m_dfus, &dfus_init);
    APP_ERROR_CHECK(err_code);

    dfu_app_reset_prepare_set(reset_prepare);
    dfu_app_dm_appl_instance_set(m_app_handle);
	
}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;
    
    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;
    
    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
	printf("sleep_mode_enter \n");
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;
	
	printf("on_adv_evt \n");

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
			printf("on_adv_evt, fast adv\n");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;
		case BLE_ADV_EVT_SLOW:
			printf("on_adv_evt, slow adv\n");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_ADV_EVT_IDLE:
			printf("on_adv_evt, idle adv\n");
		
			ble_advertising_start(BLE_ADV_MODE_SLOW);
		
            break;
        default:
            break;
    }
}


/**@brief Function for the Application's S110 SoftDevice event handler.
 *
 * @param[in] p_ble_evt S110 SoftDevice event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t                         err_code;
	
	printf("on_ble_evt\n");
    
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
			printf("on_ble_evt, connected\n");
		
			LEDS_ON(BSP_LED_2_MASK);
		
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
			printf("on_ble_evt, disconnected\n");
		
			LEDS_OFF(BSP_LED_2_MASK);
			
            err_code = bsp_indication_set(BSP_INDICATE_IDLE);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

		
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
			printf("on_ble_evt, PARAMS_REQUEST\n");
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
			printf("on_ble_evt, ATTR_MISSING\n");
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;
		
		case BLE_GAP_EVT_AUTH_KEY_REQUEST:
			printf("on_ble_evt, BLE_GAP_EVT_AUTH_KEY_REQUEST\n");
			break;

        default:
			printf("on_ble_evt, default\n");
            // No implementation needed.
            break;
    }
}


/**@brief Function for dispatching a S110 SoftDevice event to all modules with a S110 SoftDevice 
 *        event handler.
 *
 * @details This function is called from the S110 SoftDevice event interrupt handler after a S110 
 *          SoftDevice event has been received.
 *
 * @param[in] p_ble_evt  S110 SoftDevice event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
	printf("ble_evt_dispatch\n");
	dm_ble_evt_handler(p_ble_evt); // pairing
	//ble_db_discovery_on_ble_evt(&m_ble_db_discovery, p_ble_evt); // pairing
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_nus_on_ble_evt(&m_nus, p_ble_evt);
	
	
	ble_dfu_on_ble_evt(&m_dfus, p_ble_evt);
	
    on_ble_evt(p_ble_evt);
	/*
	if (p_ble_evt->header.evt_id == BLE_GAP_EVT_TIMEOUT) {
		printf("ble_evt_dispatch, gap timeout\n");
	}
	
	if (p_ble_evt->header.evt_id == BLE_GAP_EVT_TIMEOUT)
        ble_advertising_start(BLE_ADV_MODE_FAST);
    else
        ble_advertising_on_ble_evt(p_ble_evt);
	*/
	
    ble_advertising_on_ble_evt(p_ble_evt);
    bsp_btn_ble_on_ble_evt(p_ble_evt);
    
}

// pairing

static void sys_evt_dispatch(uint32_t sys_evt)
{
    pstorage_sys_event_handler(sys_evt);
    ble_advertising_on_sys_evt(sys_evt);
}


/**@brief Function for the S110 SoftDevice initialization.
 *
 * @details This function initializes the S110 SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;
    
    // Initialize SoftDevice.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, NULL);

    // Enable BLE stack.
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
#if (defined(S130) || defined(S132))
    ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
#endif
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
    err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    // Subscribe for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
	
	// pairing
	err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    uint32_t err_code;
    switch (event)
    {
        case BSP_EVENT_SLEEP:
			printf("evt hander sleep\n");
//            sleep_mode_enter();
            break;

        case BSP_EVENT_DISCONNECT:
			printf("evt hander disconn\n");
            err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_WHITELIST_OFF:
			printf("evt hander white off\n");
            err_code = ble_advertising_restart_without_whitelist();
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;
			
		case BSP_EVENT_KEY_3 :
			printf("key 0\n");
		
			bandModeLoop();
			break;
		
		case BSP_EVENT_KEY_1 : 
			printf("key 1\n");
			break;

        default:
            break;
    }
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to 
 *          a string. The string will be be sent over BLE when the last character received was a 
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of 
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;

            if ((data_array[index - 1] == '\n') || (index >= (BLE_NUS_MAX_DATA_LEN)))
            {
                err_code = ble_nus_string_send(&m_nus, data_array, index);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
                
                index = 0;
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}
/**@snippet [Handling the data received over UART] */


/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
static void uart_init(void)
{
    uint32_t                     err_code;
    const app_uart_comm_params_t comm_params =
    {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_ENABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud38400
    };

    APP_UART_FIFO_INIT( &comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOW,
                       err_code);
    APP_ERROR_CHECK(err_code);
}
/**@snippet [UART Initialization] */


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;

    // Build advertising data struct to pass into @ref ble_advertising_init.
    memset(&advdata, 0, sizeof(advdata));
    advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance = false;
    advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    scanrsp.uuids_complete.p_uuids  = m_adv_uuids;

    ble_adv_modes_config_t options = {0};
	options.ble_adv_whitelist_enabled = BLE_ADV_WHITELIST_ENABLED; // pairing
    options.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
    options.ble_adv_fast_interval = APP_ADV_FAST_INTERVAL;
    options.ble_adv_fast_timeout  = APP_ADV_FAST_TIMEOUT;
    options.ble_adv_slow_enabled  = BLE_ADV_SLOW_ENABLED;
    options.ble_adv_slow_interval = APP_ADV_SLOW_INTERVAL;
    options.ble_adv_slow_timeout  = APP_ADV_SLOW_TIMEOUT;

    err_code = ble_advertising_init(&advdata, &scanrsp, &options, on_adv_evt, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    bsp_event_t startup_event;

    uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
                                 APP_TIMER_TICKS(100, APP_TIMER_PRESCALER), 
                                 bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


/**@brief Function for placing the application in low power state while waiting for events.
 */
static void power_manage(void)
{
	printf("power_manage\n");
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

#if( TWI0_ENABLED + TWI1_ENABLED ) > 0
static uint32_t eeprom_read(size_t addr, uint8_t * pdata, size_t size)
{
    uint32_t ret;

	//printf("eeprom read \n");
    do
    {
       uint8_t addr8 = (uint8_t)addr;
       ret = nrf_drv_twi_tx(&m_twi_master, BMI055_ADDR, &addr8, 1, true);
       if(NRF_SUCCESS != ret)
       {
           break;
       }
       ret = nrf_drv_twi_rx(&m_twi_master, BMI055_ADDR, pdata, size, false);
    }while(0);
	
    return ret;
}
#endif

/*
void timer_clcok_event_handler(nrf_timer_event_t event_type, void* p_context)
{
    switch(event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:
			printf("clock accur \n");
            break;
        
        default:
            //Do nothing.
            break;
    }    
}
*/

// 밴드의 시간정보를 초기화한다.	
static void clockInit() {
	dataTimeStamp.time_year = 0x0;
	dataTimeStamp.time_month = 0x0;
	dataTimeStamp.time_day = 0x0;
	dataTimeStamp.time_hh = 0x0;
	dataTimeStamp.time_mm = 0x0;
	dataTimeStamp.time_ss = 0x0;
	dataTimeStamp.data_count = 0;	
	
	dataTimeStamp.timeTotalBackup = 0;
	dataTimeStamp.timeTotal = 0;
}

static void bsp_button_event_handler(uint8_t pin_no, uint8_t button_action)
{
    bsp_event_t        event  = BSP_EVENT_NOTHING;
    uint32_t           button = 0;
    uint32_t           err_code;
    static uint8_t     current_long_push_pin_no;              /**< Pin number of a currently pushed button, that could become a long push if held long enough. */
    static bsp_event_t release_event_at_push[BUTTONS_NUMBER]; /**< Array of what the release event of each button was last time it was pushed, so that no release event is sent if the event was bound after the push of the button. */

    if (button < BUTTONS_NUMBER)
    {
        switch(button_action)
        {
            case APP_BUTTON_PUSH:
				printf("APP_BUTTON_PUSH \n");
                break;
            case APP_BUTTON_RELEASE:
				printf("APP_BUTTON_RELEASE \n");

                break;
            case BSP_BUTTON_ACTION_LONG_PUSH:
				printf("BSP_BUTTON_ACTION_LONG_PUSH \n");
                break;
        }
    }
}

static void button_timer_handler(void * p_context)
{
    bsp_button_event_handler(*(uint8_t *)p_context, BSP_BUTTON_ACTION_LONG_PUSH);
}

void button_event_handler(uint8_t pin_no, uint8_t button_action)
{
    if (button_action == APP_BUTTON_PUSH)
    {
        switch (pin_no)
        {
            case BSP_BUTTON_0:
                printf("BSP_BUTTON_0 \n");
                break;

            case BSP_BUTTON_1:
                printf("BSP_BUTTON_1 \n");
                break;

            default:
                printf("BSP_BUTTON_ no no \n");
                break;
        }
    }
}

static void buttons_init(void)
{
    // Note: Array must be static because a pointer to it will be saved in the Button handler
    //       module.
    static app_button_cfg_t buttons[] =
    {
        {BSP_BUTTON_3, false, BUTTON_PULL, button_event_handler},
		{BSP_BUTTON_1, false, BUTTON_PULL, button_event_handler}
        // YOUR_JOB: Add other buttons to be used:
        // {MY_BUTTON_PIN,     false, NRF_GPIO_PIN_NOPULL, button_event_handler}
    };
	
	//bsp_event_to_button_action_assign(0, BSP_BUTTON_ACTION_PUSH, BSP_EVENT_DEFAULT);
    
    app_button_init(buttons, sizeof(buttons) / sizeof(buttons[0]), BUTTON_DETECTION_DELAY);
                    
    // Note: If the only use of buttons is to wake up, the app_button module can be omitted, and
    //       the wakeup button can be configured by
    // GPIO_WAKEUP_BUTTON_CONFIG(WAKEUP_BUTTON_PIN);
	
	app_button_enable();
	
	/*
	app_timer_create(&button_timer_id,
						APP_TIMER_MODE_SINGLE_SHOT,
						button_timer_handler);
	*/
}

static void bandModeLoop() {
	band_mode = band_mode + 1;
	
	printf("band mode : %d\n", band_mode);
	
	if( band_mode == BAND_MODE_MAX ) {
		band_mode = BAND_MODE_CLOCK;
		printf("band mode is clock\n");
	}	
	
	if( band_mode == BAND_MODE_CLOCK ) {
		SHOW_OLED_STEP_FLAG = false;   // 걸음수 숨김
		SHOW_CURRENT_TIME = true;      // 현재시간 보임
		SHOW_DISTANCE = false;         // 거리 숨김
		SHOW_CALORY = false;           // 칼로리 숨김
	}
	else if( band_mode == BAND_MODE_WALK ) {
		SHOW_OLED_STEP_FLAG = true;    // 걸음수 보임
		SHOW_DISTANCE = false;         // 거리 숨김
		SHOW_CALORY = false;           // 칼로리 숨김
	}
	else if( band_mode == BAND_MODE_DISTANCE ) {
		SHOW_OLED_STEP_FLAG = false;   // 걸음수 숨김
		SHOW_DISTANCE = true;          // 거리 보임
		SHOW_CALORY = false;           // 칼로리 숨김
	}
	else if( band_mode == BAND_MODE_CAL ) {
		SHOW_OLED_STEP_FLAG = false;   // 걸음수 숨김
		SHOW_DISTANCE = false;         // 거리 숨김
		SHOW_CALORY = true;            // 칼로리 보임
	}
}

static void bandModeChange(band_mode_t mode) {
	band_mode = mode;
}


/**@brief Application main function.
 */
int main(void)
{
    uint32_t err_code;
    bool erase_bonds;
    //uint8_t  start_string[] = START_STRING;
	uint8_t iLoop = 0;
	countRealActBackup = 0;
	countRealAct = 0;
	
	REAL_STEP = false;
	EVT_STEP = false;
	
	GET_FIRM_VER = false;
	
	notiStateCode = 0;
	SEND_NOTI_TO_APP = false;
	
	CLEAR_ACT_DATA = false; // 밴드에 저장된 각종 활동정보를 지울지 관리하는 플래그
	
	activity_save_timer = 0;
	save_activity_count = false;
	activity_count = 0;
	activity_count_readed = 0;
	
	currentActivity = 0;
	
	SHOW_DISTANCE = false;
	SHOW_CALORY = false;
	SEND_ACT_DATA = false;
	
	manInfoInit(); // 사용자 정보 초기화 
	goalInit();    // 목표값 초기화
	
	bandModeChange( BAND_MODE_CLOCK );
	
	SEND_BATTERY = false;
	
	SHOW_CURRENT_TIME = false; // 현재시간을 보여준다.
	
	uint8 appFontData = 0;
	uint8 appFontDataIndex = 0;
	
	vibePatternIndex = 0;
	isVibeOn = false;
	vibePatternCount = 0;
	vibeNew = false;
	
	memset(&vibePattern, 0, sizeof(vibePattern));		
	
	IS_CALL_NUM_SCROLLING = false;
	
	////////
	// PWM
	////////
	nrf_gpio_pin_dir_set(MOTOR_A_EN_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_clear(MOTOR_A_EN_PIN);
	
	motorStatusOld = false;
    motorStatusNew = false;
    motorStatusChangeCount = 0;
	IS_PWM_ENABLE = false;
	ING_PWM = false;
	PWM_ON = false;
	STOP_VIBE = false;
	stopVibeCount = 0;
	
	//uint16 remainCount = 0;
	resultStep = 0;
	
	IS_SHOW_CALL_NUM = false;
	
	callResetCount = 0;
	
	stepCheck = false;
	IS_STEP_CHANGE = false;
	SHOW_CALL_NUM = false;
	
	SHOW_16_8 = false;
	
	SHOW_OLED_STEP_FLAG = false;
	
	isSaveData = false;
	CMD_MEMORY_CLEAR = false;
	
	m_transfer_completed = false;
	m_transfer_completed2 = false;
	
	flashMap.flashPageChecker = 0;
	flashMap.pageNum = 0;
	flashMap.sectorNum = 0;
	flashMap.flashPageToRead = 0;
	
	sensorMode = 0;
	
	clockInit(); // 밴드의 시간정보를 초기화한다.	
	
	dataSendBuffer.sending = FALSE;
	SAVE_DATA = false;
	
//	writeCount = 0;
	readRun = false;	
	
	passHandler = true;

	// 플래시 초반데이터를 삭제한다. 그때 주소는 0x000000으로 하고 
	// 플래시를 지우고 나서는 메모리맵대로 설정하자.
	flash_addr = ACTIVITY_BASE;

	
	///////////////////////////////////////
	// 네이버 알고리즘
	///////////////////////////////////////
	memset(&stat, 0, sizeof(stat));
	retStepCount = 0;
	totalStepCount = 0;
	totalStepCountBackup = -1;
	
	
	
	
	//bsp_configuration();
	
//	LEDS_CONFIGURE(LEDS_MASK);
//    LEDS_OFF(LEDS_MASK);
    
    // Initialize.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);
    
	
#if( TWI0_ENABLED + TWI1_ENABLED ) > 0
	twi_master_init();
#endif	
	
	nrf_drv_spi_config_t const config =
    {
		/*
        #if (SPI0_ENABLED == 1) // not used
            .sck_pin  = SPIM0_SCK_PIN,
            .mosi_pin = SPIM0_MOSI_PIN,
            .miso_pin = SPIM0_MISO_PIN,
            .ss_pin   = SPIM0_SS_PIN,
        #elif (SPI1_ENABLED == 1) // flash
		*/
            .sck_pin  = SPIM1_SCK_PIN,
            .mosi_pin = SPIM1_MOSI_PIN,
            .miso_pin = SPIM1_MISO_PIN,
            .ss_pin   = SPIM1_SS_PIN,
		/*
        #elif (SPI2_ENABLED == 1) // oled
            .sck_pin  = SPIM2_SCK_PIN,
            .mosi_pin = SPIM2_MOSI_PIN,
            .miso_pin = SPIM2_MISO_PIN,
            .ss_pin   = SPIM2_SS_PIN,
        #endif
		*/
        .irq_priority = APP_IRQ_PRIORITY_LOW,
        .orc          = 0xCC,
        .frequency    = NRF_DRV_SPI_FREQ_1M,//NRF_DRV_SPI_FREQ_1M, // NRF_DRV_SPI_FREQ_500K, //NRF_DRV_SPI_FREQ_500K, //NRF_DRV_SPI_FREQ_1M, //NRF_DRV_SPI_FREQ_1M,
        .mode         = NRF_DRV_SPI_MODE_0,//NRF_DRV_SPI_MODE_0,
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST, //NRF_DRV_SPI_BIT_ORDER_LSB_FIRST,
				//.ss_pin       = NRF_DRV_SPI_PIN_NOT_USED,
    };		
	
    err_code = nrf_drv_spi_init(&m_spi_master, &config, spi_master_event_handler);			
			
	
	//printf("--- spi init : %u\n", err_code);
  //  APP_ERROR_CHECK(err_code);
	
	uart_init();
	
	
	
	//state_machine_state_change(APP_STATE_SINGLE_SHOT);	
	
		// CHIP ID READ
	//m_tx_data[0] = 0x9f;
	//spi_send_recv(m_tx_data, m_rx_data, 4 ); // TX_RX_BUF_LENGTH
		
	/*
	// Write Enable
	m_tx_data[0] = 0x06;
	spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
					
	// Sector Erase
	m_tx_data[0] = 0x20;
	m_tx_data[1] = flash_addr >> 16;
	m_tx_data[2] = flash_addr >> 8;
	m_tx_data[3] = flash_addr;		  
	spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_W ); // TX_RX_BUF_LENGTH	
	
	///*
    
	*/
	
	//nrf_gpio_cfg_input(BSP_BUTTON_0, NRF_GPIO_PIN_PULLUP);
	
	ble_opt_t static_pin_option;
	
	buttons_leds_init(&erase_bonds);
    ble_stack_init();
	
	///*
	static_pin_option.gap_opt.passkey.p_passkey = passkey;
    err_code =  sd_ble_opt_set(BLE_GAP_OPT_PASSKEY, &static_pin_option);
	
	if( err_code != NRF_SUCCESS ) {
		printf("sd_ble_opt_set, %d\n", err_code);
	}
	//*/
	
	
	
	device_manager_init(erase_bonds);
		
    gap_params_init();
    services_init();
    advertising_init();
    conn_params_init();
    
    //printf("%s",start_string);
	
	//nrf_gpio_cfg_output(24);
	//nrf_gpio_cfg_output(26);
	
	/*
	nrf_gpio_pin_clear(24);	
	nrf_delay_ms(DELAY_MS * 50);	
	nrf_gpio_pin_set(24);
	*/
	
	//nrf_gpio_pin_set(26);
	//nrf_gpio_pin_set(24);
		
	
	// 거리, 칼로리 테스트
	double test1, test2;
	//test1 = stepToMile(500);
	//test2 = stepToMile(1000);
	
	
	//test1 = stepToKm(100); // 0.06km
	//test1 = stepToKm(150); // 0.10km
	//test1 = stepToKm(300); // 0.20km	
	//test1 = stepToKm(500); // 0.3km
	//test2 = stepToKm(1000); // 0.6km
	//test2 = stepToKm(2000); // 1.3km
	//test2 = stepToKm(20000); // 13.8km
	//test2 = stepToKm(200000); // 138.3km
	
	printf("dis : %f, %f\n", test1, test2);
	
	test1 = stepToCalory(500);
	test2 = stepToCalory(1000);
	
	printf("cal : %d, %d\n", test1, test2);
	

    err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
	
	if( err_code != NRF_SUCCESS ) {
		printf("ble_advertising_start error %d\n", err_code);
	}
    //APP_ERROR_CHECK(err_code);
	

	printf("*********** new 2 ****\r\n");
	
	// Block Erase(64K)
	// 64M 전체 블럭을 삭제한다.
	///*
	for( iLoop = 0; iLoop < 2; iLoop++) {	 //
		m_tx_data[0] = 0x06;
		//m_transfer_completed = false;
		spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
		//while (m_transfer_completed == false);	
		nrf_delay_ms(DELAY_MS);
		
		m_tx_data[0] = 0xD8;		
		m_tx_data[1] = flash_addr >> 16;
		m_tx_data[2] = flash_addr >> 8;
		m_tx_data[3] = flash_addr;	
		
		printf("block add : %d, %x, %x, %x\n", iLoop, m_tx_data[1], m_tx_data[2], m_tx_data[3]);
		spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_W ); // TX_RX_BUF_LENGTH		
		
		flash_addr += 0x10000;
		nrf_delay_ms(DELAY_MS_100);
	}	
	
	notiFlashEraseComplete();
	//*/
	
	
	flash_addr = FLASH_TARGET_ADDR;
	flash_addr_r = FLASH_TARGET_ADDR;
	flash_addr_activity = ACTIVITY_BASE;
	flash_addr_activity_r = ACTIVITY_BASE;
	
	
	oledInit(SSD1306_CONFIG_DC_PIN,
             SSD1306_CONFIG_RST_PIN,
             SSD1306_CONFIG_CS_PIN,
             SSD1306_CONFIG_CLK_PIN,
             SSD1306_CONFIG_MOSI_PIN); // ccc
	
	// 64 by 32 영역을 확인하기 위해서
	// 128 by 32 영역을 모두 색칠해 놓는다. 
	ssd1306_clear_buffer_128(0xFF);
	ssd1306_refresh_gram_4_128_test( DIS_MULTI_BYTE );
	
	///*
	
	//ssd1306_display_string(10, 10, "Init...", 12, 1);
	//ssd1306_display_string(80, 15, "Init...", 16, 1);
	//ssd1306_display_string(28 - 4, 8, "00", 16, 1); // 16크기 두번째 자리수
	ssd1306_clear_screen(0x00);
	//ssd1306_display_string(5, 8, "Init..", 16, 1); // 16크기 세번째 자리수
	ssd1306_refresh_gram( DIS_MULTI_BYTE );		
	
	
	
	
	//app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, BSP_LED_0, BSP_LED_1);
	
	
	
	//ssd1306_display_string(10, 10, "xyz", 12, 1);
	//ssd1306_display_string(10, 8, "10:10", 16, 1);
	//ssd1306_display_string(80, 15, "10:10", 16, 1); // 좌측 상단
	//ssd1306_refresh_gram( DIS_MULTI_BYTE );	
	
	//*/
	
	//sensorModeChange(0);
	
	// 시계가 디폴트다.
    // 시간정보는 최초 아무런 입력이 없으면 00:00 부터 시작이다. 	
	updataTime(10);
	band_mode = BAND_MODE_CLOCK;
	bandModeLoop();
	SHOW_CURRENT_TIME = true;
	
	
	//ssd1306_refresh_gram( DIS_MULTI_BYTE );	
	
	///////////////////////////////////////
	// PWM 설정
	///////////////////////////////////////

		///*
	nrf_gpio_pin_dir_set(MOTOR_A_PWM_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
    nrf_gpio_pin_clear(MOTOR_A_PWM_PIN);
	app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(50, MOTOR_A_PWM_PIN);
	
	pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;	
	err_code = app_pwm_init(&PWM1, &pwm1_cfg,pwm_ready_callback);
	
	//printf("pwm error : %u\n", err_code);
    //app_pwm_enable(&PWM1);
	
	//app_pwm_disable(&PWM1);
	
	//pwm_stop_A();
	//*/
	
	
	sensorModeChange( 0 ); // 네이버 글자후 스텝카운트를 보여주기 위함
	
	/*
	ssd1306_clear_screen(0x00);
	ssd1306_display_string(10, 10, "Init...", 12, 1);
	ssd1306_refresh_gram( DIS_MULTI_BYTE );	
	*/
	//ssd1306_display_string(80, 15, "Init...", 16, 1);
	//ssd1306_display_string(28 - 4, 8, "00", 16, 1); // 16크기 두번째 자리수
	
	//*/
	
	//ssd1306_display_string(80, 15, "7", 16, 1);
	//ssd1306_display_char(80, 15, '7', 16, 1);
	//ssd1306_display_string(80, 15, "Init...", 16, 1);
	
	/*
	// 원하는 위치에 한 글자를 뿌리는 예제(앱에서 받은 데이터를 저장하는 버퍼 이용)
	ssd1306_display_string_app(80, 15, 0, 16, 1);
	ssd1306_refresh_gram( DIS_MULTI_BYTE );
	*/
	
	/*
	err_code = nrf_drv_timer_init(&TIMER_CLOCK, NULL, timer_clcok_event_handler);
    if( err_code != NRF_SUCCESS ) {
		printf("nrf_drv_timer_init err : %u\n", err_code);
	}
	
	time_ticks = nrf_drv_timer_ms_to_ticks(&TIMER_CLOCK, time_ms);
	
	nrf_drv_timer_extended_compare(
         &TIMER_CLOCK, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
    
    nrf_drv_timer_enable(&TIMER_CLOCK);
	*/
	
	/*
	// 3 표시 
	s_chDispalyBuffer[0][0] = 0x00;
	s_chDispalyBuffer[1][0] = 0x00;
	s_chDispalyBuffer[2][0] = 0x42;
	s_chDispalyBuffer[3][0] = 0x52;
	s_chDispalyBuffer[4][0] = 0x52;
	s_chDispalyBuffer[5][0] = 0x3C;
	s_chDispalyBuffer[6][0] = 0x00;
	s_chDispalyBuffer[7][0] = 0x00;
	*/

	//printf("--- s\n");
	//printf("--- e\n");
	//printf("dis string e\n");
	

	//savedDataClear();

	/*
	sendDataBuffer[0] = 0x64;
			sendDataBuffer[1] = 0x70;
			sendDataBuffer[2] = 3;
			sendDataBuffer[3] = 4;
			sendDataBuffer[4] = 5;
			sendDataBuffer[5] = 6;

            err_code = ble_nus_string_send(&m_nus, sendDataBuffer, 6);
			printf("send : %u\n", err_code);
	*/
	
	flash_addr = FLASH_TARGET_ADDR;
	
	/* aaa
	m_tx_data[0] = 0x9f;
	spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_R ); // TX_RX_BUF_LENGTH
	*/
	
	//printf("begin for loop \n");
	
// TIMER
	///*
	//APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create timers.
    err_code = app_timer_create(&main_timer_id,
                                APP_TIMER_MODE_REPEATED, //APP_TIMER_MODE_REPEATED,
                                main_timeout_handler);
								
    err_code = app_timer_create(&save_timer_id,
                                APP_TIMER_MODE_REPEATED, //APP_TIMER_MODE_SINGLE_SHOT,
                                save_timeout_handler);		

///*
	err_code = app_timer_create(&noti_timer_id,
                                APP_TIMER_MODE_REPEATED, //APP_TIMER_MODE_SINGLE_SHOT,
                                noti_timeout_handler);	
								//*/
																
	//printf("create : %d\n", err_code);
	
	//buttons_init();
	
	SHOW_OLED_STEP_FLAG = false;
										
	///*										
	err_code = app_timer_start(main_timer_id, MAIN_TIMER_INTERVAL, NULL);
	//printf("start : %d\n", err_code);
	
	err_code = app_timer_start(save_timer_id, MAIN_TIMER_INTERVAL, NULL);
	
	err_code = app_timer_start(noti_timer_id, TIMER_TICKS, NULL);
	//printf("start : %d\n", err_code);
	//*/
	//*/
	//*/
	
	//savedDataClear();
	
	uint32_t value;
	
	//pwm_start_A();
    
    // Enter main loop.
    for (;;)
    {
        //power_manage();		
		// ggg
		
		/*
		while(true)
		{
			//for (uint8_t i = 0; i < 40; ++i)
			//{
				//value = (i < 20) ? (i * 5) : (100 - (i - 20) * 5);
			//	value = (i < 20) ? 50 : 50;
			value = 5;
				
				ready_flag = false;
				// Set the duty cycle - keep trying until PWM is ready
				while (app_pwm_channel_duty_set(&PWM1, 0, value) == NRF_ERROR_BUSY);
				
				// ... or wait for callback
				//while(!ready_flag);
				//APP_ERROR_CHECK(app_pwm_channel_duty_set(&PWM1, 1, value));
				nrf_delay_ms(1);
			//}
		}
		*/
		if( band_mode == BAND_MODE_CLOCK ) {
		}
		else if( band_mode == BAND_MODE_WALK ) {
		}
		else if( band_mode == BAND_MODE_DISTANCE ) {
		}
		else if( band_mode == BAND_MODE_CAL ) {
		}
		else {
		}
		
		if( readRun == true ) {
			
			if( (flash_addr_r + 18) > FLASH_TARGET_SIZE ) {
				printf("+full, stop reading\n");
				
				// 메모리가 풀이면 더이상 기록하지 않는다.
				passHandler = true;  // 더 이상 모션센서를 기록하지 않는다.
				readRun = false;     // 메모리에서 값을 읽어서 보내지 않는다.
				
				notiFlashReadFull(); // 메모리를 끝까지 다 읽었음을 앱에 알린다.
			}
			else if( (flash_addr_r + 50) > flash_addr ) {			
				sensorModeChange( 0 );
				notiFlashReadLast();
			}
			else {
				//			readRun = false;
	//			printf("writeCount is 30 reached.\n");
				
				// Read
				m_tx_data[0] = 0x03;	
				m_tx_data[1] = flash_addr_r >> 16;
				m_tx_data[2] = flash_addr_r >> 8;
				m_tx_data[3] = flash_addr_r;		       
	//			m_tx_data[4] = 0;					
				
				printf("Raddr: %u, %u, %u \n", m_tx_data[1], m_tx_data[2], m_tx_data[3]);
				
				// 40바이트를 초기화한다. 
				memset(m_rx_data, 0, 40);
				memset(row_data, 0, 20);
				spi_send_recv(m_tx_data, m_rx_data, 4 + 18, RW_MODE_R);
		
				memcpy(&row_data[2], &m_rx_data[4], 18);
				flash_addr_r += 18;//SENSOR_BUF_SIZE;
				
				/*
				sendDataBuffer[0] = 1;
				sendDataBuffer[1] = 2;
				sendDataBuffer[2] = 3;
				sendDataBuffer[3] = 4;
				sendDataBuffer[4] = 5;
				sendDataBuffer[5] = 6;
				*/
				row_data[0] = 0xFF;
				row_data[1] = 0x11;
				
				///*
				printf("bt1 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", row_data[0], row_data[1], row_data[2], row_data[3], 
					row_data[4], row_data[5], row_data[6], row_data[7], row_data[8], row_data[9]);
				printf("bt2 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", row_data[10], row_data[11], row_data[12], row_data[13], 
					row_data[14], row_data[15], row_data[16], row_data[17], row_data[18], row_data[19]);	
					//*/

				//err_code = ble_nus_string_send(&m_nus, m_rx_data, 20);
				err_code = ble_nus_string_send(&m_nus, row_data, 20);
				printf("send: %u\n", err_code);
			}
		}
		
		if( SAVE_DATA == true ) {
			SAVE_DATA = false;
			//dataSendBuffer.sending = false;
			/*
			printf("*** save data *** %u \n", flashMap.flashPageToRead);			
			for( iLoop = 0; iLoop < 4; iLoop++ ) {
				printf("*** sBuffer : %c, %c, %c, %c, %c, %c, %c, %c, %c, %c\n", dataSendBuffer.sBufferTemp[iLoop * 10], dataSendBuffer.sBufferTemp[iLoop * 10 + 1] 
					, dataSendBuffer.sBufferTemp[iLoop * 10 + 2], dataSendBuffer.sBufferTemp[iLoop * 10 + 3], dataSendBuffer.sBufferTemp[iLoop * 10 + 4]
					, dataSendBuffer.sBufferTemp[iLoop * 10 + 5], dataSendBuffer.sBufferTemp[iLoop * 10 + 6], dataSendBuffer.sBufferTemp[iLoop * 10 + 7]
					, dataSendBuffer.sBufferTemp[iLoop * 10 + 8], dataSendBuffer.sBufferTemp[iLoop * 10 + 9]);         
			*/
			
			// Write Enable
			m_tx_data[0] = 0x06;   // write enable cmd
			spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
								
			///*
			dataSendBuffer.sBufferTemp[0] = 0x02;	 // page program cmd
			dataSendBuffer.sBufferTemp[1] = flash_addr >> 16;
			dataSendBuffer.sBufferTemp[2] = flash_addr >> 8;
			dataSendBuffer.sBufferTemp[3] = flash_addr;		
			//*/
			
			/*
			dataSendBuffer.sBufferForHeader[0] = 0x02;
			dataSendBuffer.sBufferForHeader[1] = flash_addr >> 16;
			dataSendBuffer.sBufferForHeader[2] = flash_addr >> 8;
			dataSendBuffer.sBufferForHeader[3] = flash_addr;
*/

			//printf("write size : %u\n", writeDataCount);
			printf("Waddr: %u, %u, %u \n", dataSendBuffer.sBufferTemp[1], dataSendBuffer.sBufferTemp[2], dataSendBuffer.sBufferTemp[3]);
			//printf("write addr is : %u, %u, %u \n", dataSendBuffer.sBufferForHeader[1], dataSendBuffer.sBufferForHeader[2], dataSendBuffer.sBufferForHeader[3]);
			
			if( flashMap.flashPageToRead == 4) {
//				printf("write 4 data : %x, %x, %x, %x \n", dataSendBuffer.sBufferTemp[4], dataSendBuffer.sBufferTemp[5], dataSendBuffer.sBufferTemp[6]
//							, dataSendBuffer.sBufferTemp[7]);
			}
			
			//spi_send_recv(dataSendBuffer.sBufferForHeader, m_rx_data, 4, RW_MODE_W);
			spi_send_recv(dataSendBuffer.sBufferTemp, m_rx_data, flashMap.flashPageToRead + 4, RW_MODE_W);
			
			/*
			printf("write1 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", dataSendBuffer.sBufferTemp[0], dataSendBuffer.sBufferTemp[1], 
											 dataSendBuffer.sBufferTemp[2], dataSendBuffer.sBufferTemp[3], dataSendBuffer.sBufferTemp[4], 
											 dataSendBuffer.sBufferTemp[5], dataSendBuffer.sBufferTemp[6], dataSendBuffer.sBufferTemp[7],
											 dataSendBuffer.sBufferTemp[8], dataSendBuffer.sBufferTemp[9]);
			*/
			
			/*printf("write2 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", readSensorBuffer2[10], readSensorBuffer2[11], 
											 readSensorBuffer2[12], readSensorBuffer2[13], readSensorBuffer2[14], 
											 readSensorBuffer2[15], readSensorBuffer2[16], readSensorBuffer2[17],
											 readSensorBuffer2[18], readSensorBuffer2[19]);
			printf("write3 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", readSensorBuffer2[20], readSensorBuffer2[21], 
											 readSensorBuffer2[22], readSensorBuffer2[23], readSensorBuffer2[24], 
											 readSensorBuffer2[25], readSensorBuffer2[26], readSensorBuffer2[27],
											 readSensorBuffer2[28], readSensorBuffer2[29]);											 
			printf("write4 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", readSensorBuffer2[30], readSensorBuffer2[31], 
											 readSensorBuffer2[32], readSensorBuffer2[33], readSensorBuffer2[34], 
											 readSensorBuffer2[35], readSensorBuffer2[36], readSensorBuffer2[37],
											 readSensorBuffer2[38], readSensorBuffer2[39]);	
											*/
			
			flash_addr += flashMap.flashPageToRead;
//			writeCount++;		
			
			dataSendBuffer.sBufferTemp[0] = 0x02;	
			dataSendBuffer.sBufferTemp[1] = flash_addr >> 16;
			dataSendBuffer.sBufferTemp[2] = flash_addr >> 8;
			dataSendBuffer.sBufferTemp[3] = flash_addr;		
			//printf("write addr ed : %u, %u, %u \n", dataSendBuffer.sBufferTemp[1], dataSendBuffer.sBufferTemp[2], dataSendBuffer.sBufferTemp[3]);
			
			printf("buffer 1 is write, %u \n", flashMap.flashPageToRead);
			
			isSaveData = false;
			
			/*
			if( writeCount >= 20) {
				passHandler = true;
			}
			*/
		}	
		
		if( CMD_MEMORY_CLEAR == true ) {
			CMD_MEMORY_CLEAR = false;
			
			readRun = false;     // 읽기 중단
			passHandler = true;  // 저장 중단  
			stepCheck = false;   // 스텝 체크 중단
			
			savedDataClear();
		}
		
		if( SHOW_OLED_STEP_FLAG == true ) {
			printf("##show step\n");
			SHOW_OLED_STEP_FLAG = false;
			
			if( IS_SHOW_CALL_NUM == false ) {				
				showOledCount();
				//ssd1306_display_string(28, 8, "010", 12, 1); // 16크기 1번째 자리수
				//ssd1306_refresh_gram( DIS_MULTI_BYTE );		
			}
		}
		
		// 이동거리 보여줌
		if( SHOW_DISTANCE == true ) {
			printf("##show dis\n");
			SHOW_DISTANCE = false;
			
			showOledDistance();
		}
		
		// 칼로리 보여줌
		if( SHOW_CALORY == true ) {
			printf("##show cal\n");
			SHOW_CALORY = false;
			
			showOledCalory();
		}
		
		if( SHOW_CALL_NUM == true ) {
			printf("##show call\n");
			SHOW_CALL_NUM = false;
	
			ssd1306_clear_buffer_128(0x00);
			ssd1306_clear_screen(0x00);					

			//ssd1306_display_string(5, 8, phone_num, 12, 1); // 16크기 1번째 자리수
			//ssd1306_display_string_h_scroll(5, 8, phone_num, 12, 1); // 16크기 1번째 자리수
			ssd1306_display_string_h_scroll(5, 18, phone_num, 12, 1); // 16크기 1번째 자리수
			//ssd1306_refresh_gram( DIS_MULTI_BYTE );		
			
			// OLED 상단에 전화마크(T)를 표기함. 이건 움직이면 안된다.
			ssd1306_draw_bitmap(2, 2, &IMG_inputvolt[0], 0x8, 0x6);	
			ssd1306_refresh_gram( DIS_MULTI_BYTE );
			
			ssd1306_refresh_gram_h_scroll_bottom( DIS_MULTI_BYTE );
			
			IS_CALL_NUM_SCROLLING = false;
		}
		
		if( SHOW_16_8 == true ) {
			printf("##show 16_8\n");
			SHOW_16_8 = false;
			
			ssd1306_clear_screen(0x00);
			//ssd1306_display_string_app(80, 15, 0, 16, 1);
			//ssd1306_display_string_app(0, 15, 0, 16, 1);
			//10, 8
			
			ssd1306_display_string_app(10, 8, 0, 16, 1);			
			ssd1306_refresh_gram( DIS_MULTI_BYTE );
		}
		
		if( IS_PWM_ENABLE ) {
			//printf("motor duty - first\n");
			IS_PWM_ENABLE = false;
			
			
			if( PWM_ON ) {
				ready_flag = false;
				
				//printf("motor duty - first 1 \n");
				
				/*
				// Set the duty cycle - keep trying until PWM is ready..
				while (app_pwm_channel_duty_set(&PWM1, 0, 15) == NRF_ERROR_BUSY);
				
				printf("motor duty - first 2 \n");
				
				// ... or wait for callback.
				while(!ready_flag);
				
				printf("motor duty - first 3 \n");
				
				app_pwm_channel_duty_set(&PWM1, 1, 15);
				
				*/
				
				pwm_start_A();
				
				//printf("motor duty 100\n");
			}
			else {
				/*
				printf("motor duty - first 4 \n");
				ready_flag = false;
				// Set the duty cycle - keep trying until PWM is ready..
				while (app_pwm_channel_duty_set(&PWM1, 0, 0) == NRF_ERROR_BUSY);
				
				printf("motor duty - first 5 \n");
				
				// ... or wait for callback.
				while(!ready_flag);
				
				printf("motor duty - first 6 \n");
				
				app_pwm_channel_duty_set(&PWM1, 1, 0);
				*/
				
				pwm_stop_A();
				
				printf("motor duty 0\n");
			}
		}
		
		if( SHOW_CURRENT_TIME == true) {
			SHOW_CURRENT_TIME = false;	
			printf("##show time\n");			
			
			printf("show current : %s\n", strCurruntTime);
			
			if( IS_CALL_NUM_SCROLLING == false ) {				
				//ssd1306_clear_screen(0x00);
				ssd1306_clear_buffer_128(0x00);
				ssd1306_display_string(10, 8, strCurruntTime, 16, 1);
				//ssd1306_display_string(80, 15, "11:34", 16, 1);
				ssd1306_refresh_gram( DIS_MULTI_BYTE );	
				
				//ssd1306_display_string(80, 15, "10:10", 16, 1);
				//ssd1306_refresh_gram( DIS_MULTI_BYTE );	
			}
			else {
				printf("show current time pass. num scrolling\n");
			}
		}
		
		if( SEND_BATTERY == true ) {
			SEND_BATTERY = false;
			sendBattery();
		}
		
		if( save_activity_count == true ) {
			save_activity_count = false;
			
			activity_count += 1;
			
			if( activity_count >= 65535 ) {
				activity_count = 1;
			}
			
			writeActivity( countRealActBackup );
			
			printf("save activity count\n");
		}
		
		
		if( SEND_ACT_DATA == true ) {
			readActivity();
		}
		
		if( CLEAR_ACT_DATA == true ) {
			CLEAR_ACT_DATA = false;
			printf("act erase, block 0 start\n");
			flash_addr_for_erase = ACTIVITY_BASE;
			
			// Block Erase(64K)
			// 0번 블럭을 삭제한다.
			m_tx_data[0] = 0x06; // write enable cmd
			spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
			nrf_delay_ms(DELAY_MS);
		
			m_tx_data[0] = 0xD8; // block erase cmd
			m_tx_data[1] = flash_addr_for_erase >> 16;
			m_tx_data[2] = flash_addr_for_erase >> 8;
			m_tx_data[3] = flash_addr_for_erase;	
			
			printf("block add : %x, %x, %x\n", m_tx_data[1], m_tx_data[2], m_tx_data[3]);
			spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_W ); // TX_RX_BUF_LENGTH		
			nrf_delay_ms(DELAY_MS_100);
			printf("act erase, block 0 end\n");
			
			// 각종 값에 대한 초기화를 진행하자.
			flash_addr_activity = ACTIVITY_BASE;
			flash_addr_activity_r = ACTIVITY_BASE;
			activity_count = 0;
			activity_count_readed = 0;
		}
		
		// 잘못된 커맨드가 app에서 오면, 잘못된 커맨드라는 상태를 앱으로 보내자.
		if( SEND_NOTI_TO_APP == true ) {
			SEND_NOTI_TO_APP = false;					
			
			notiToApp( notiStateCode );
		}
		
		// 펌웨어 버전가져오기 커맨드
		if( GET_FIRM_VER == true ) {
			GET_FIRM_VER = false;
			
			notiToAppFirmwareVersion();
		}
		
		// 실시간 걸음수다
		if( REAL_STEP == true ) {
			// 걸음수가 발생되었다면, 실시간 걸음수 반영을 위해서 발생된 값을 앱으로 보내자. 
			if( EVT_STEP == true ) {
				EVT_STEP = false;
				notiToAppRealtimeStep( retStepCount );
			}
		}
    } // vvv
}
///*	


/** 
 * @}
 */

static void enqueue(char* data, uint16 len) { 
    /*uint16 iLoop = 0;*/
    int remainBuffer = 0;
    int remainLen = 0;
    int remainIndex = 0;
        
	printf("enqueue\n");
    
    /*
    SHOW_MSG_STR("enqueue : ");
    SHOW_MSG_UINT8(len);
    SHOW_MSG_STR("\r\n");
    */
     
    if(is_full()) 
    { 
        printf("Queue full\r\n");
    } 

	// 새로 넣는 데이터가 버퍼를 초과할 경우
    if(dataSendBuffer.rear + len - 1 >= MAX_QUEUE_SIZE) {
        //printf("enqueue 11\r\n");
        
        
        /*
        SHOW_MSG_STR("enqueue 11, rear is : ");
        SHOW_MSG_UINT16(dataSendBuffer.rear);
        SHOW_MSG_STR("\r\n");
        */
        /*
        for( iLoop = 0; iLoop < len; iLoop++) {
            //enqueueEach(data[iLoop]); 
        }
        */
        /*
        remainBuffer = MAX_QUEUE_SIZE - dataSendBuffer.rear + 1;
        SHOW_MSG_STR("remainBuffer is : ");
        SHOW_MSG_UINT8(remainBuffer);
        SHOW_MSG_STR("\r\n");
        
        */
        remainIndex = 0;
        remainLen = len;

        do {
            dataSendBuffer.rear = (dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE;
            remainBuffer = MAX_QUEUE_SIZE - dataSendBuffer.rear + 1;
            
            /*
			printf("enqueue 11, remainBuffer is : %u\n", remainBuffer);
            */
            
            if( remainLen > remainBuffer ) {
                /*
                printf("do while if\n");				
                */
                memcpy(&dataSendBuffer.queue[dataSendBuffer.rear], &data[remainIndex], (char)remainBuffer);  
                dataSendBuffer.rear = dataSendBuffer.rear + remainBuffer - 1;
                
                /*
                printf("do while if rear is : %u\n", dataSendBuffer.rear);
                */
            
                remainIndex = remainIndex + remainBuffer;
                
                /*
                printf("do while if remainIndex is : %u\n", remainIndex);
                */
                
                remainLen = remainLen - remainBuffer;
                
                /*
                printf("do while if remainLen is : %u\n", remainLen);
                */
            }
            else { 
                /*
                printf("do while else....enqueue over *******\r\n");
                
                printf("do while else dataSendBuffer.rear 1 is : %u\n", dataSendBuffer.rear);
                */
                
                memcpy(&dataSendBuffer.queue[dataSendBuffer.rear], &data[remainIndex], (char)remainLen);  
                dataSendBuffer.rear = dataSendBuffer.rear + remainLen - 1;
                
                /*
                printf("do while else dataSendBuffer.rear 2 is : %u\n", dataSendBuffer.rear);                
                printf("do while else remainIndex is : %u\n", remainIndex);
                
                printf("do while else remainLen is : %u\n", remainLen);
                */
				
                remainLen = remainLen - remainLen;
                //dataSendBuffer.rear = dataSendBuffer.rear + len - 1;
            }
        } while( remainLen > 0 );
    }
	// 새로 넣는 데이터가 버퍼를 초과하지 않을 경우
    else {
//        printf("enqueue 22\r\n"); 
        dataSendBuffer.rear = (dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE;
        
        ///*
//        printf("enqueue 22, rear index : %u\n", dataSendBuffer.rear);        
        //*/
            
        memcpy(&dataSendBuffer.queue[dataSendBuffer.rear], &data[0], (char)len);  
        dataSendBuffer.rear = dataSendBuffer.rear + len - 1;
    }
    
    ///*
//    printf("enque final rear : %u\n", dataSendBuffer.rear);                
//    printf("enque final front : %u\n", dataSendBuffer.front);
    //*/
                
    //dataSendTimerStart();
} 

static int is_full() { 
    uint16 val = 0;
    
    /*
    SHOW_MSG_STR("is_full check\r\n");
    */
    
    val = (dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE;
    ///*
    printf("fullcheck,rear,front: %u, %u\n", val, dataSendBuffer.front);    	
    //*/
    
    return ((dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE == dataSendBuffer.front);  
} 

static int is_empty() {
    /*I
    SHOW_MSG_STR("is_empty front : ");
    SHOW_MSG_UINT16(dataSendBuffer.front);
    SHOW_MSG_STR("\r\n");
    
    SHOW_MSG_STR("is_empty rare : ");
    SHOW_MSG_UINT16(dataSendBuffer.rear);
    SHOW_MSG_STR("\r\n");
    */
    return (dataSendBuffer.front == dataSendBuffer.rear);  
} 

static void dequeue() {  
    int count = 0;
    int remainLen = 0;
    int remainIndex = 0;
    int remainToReadCount = 0;
//	int iLoop = 0;
    
    if(is_empty()) 
    { 
        printf("Queue empty\n");
    } 	
    
	// 읽어올 수 있는 데이터 수를 알아낸다.
	// rear가 큰 경우는 rear-front가 읽어갈 수 있는 데이터의 수이다. 
    if(dataSendBuffer.rear >= dataSendBuffer.front) {
        count = dataSendBuffer.rear - dataSendBuffer.front;   
        /*
        //printf("dequeue 11 read count : %u\n", count);
        */
    }
	// front가 큰 경우는 rear데이터가 버퍼의 끝을 넘어서 다시 처음으로 간 경우다. 
    else {
        count = MAX_QUEUE_SIZE - dataSendBuffer.front + dataSendBuffer.rear;
        /*
        //printf("dequeue 22 read count : %u\n", count);
        */
    }
	
	printf("Run dequeue \n");
	
	if( count >= 36 ) {		        
		//printf("Run dequeue, over 36 \n");
		/*
		buffer 잔량 체크를 위한 로그
		if(( count > 900 ) && ( count < 1100)) {
			SHOW_MSG_STR("enque buffer == 900 ~ 1000\r\n");    
		}
		else if( ( count > 1900 ) && ( count < 2100 ) ) {
			SHOW_MSG_STR("enque buffer == 1900 ~ 2100 \r\n");    
		}
		else if( (count > 2900 ) && (count < 3100 )) {
			SHOW_MSG_STR("enque buffer == 2900 ~ 3100\r\n");    
		}
		else if( count > 3800 ) {
			SHOW_MSG_STR("enque buffer >  3800\r\n");    
		}
		*/
		
		// 센서 데이터 x, y, z가 각각 6바이트다. 
		// 이걸 한번에 6묶음을 쓰려고 한다. 그래서 36바이트다. 
		/*
		if( count > 20 ) {
			count = 20;
		}
		*/
		
		// flash page에 저장하기 위한 데이터수를 결정하는 부분
		// 기본적으로는 36바이트다. 센서 데이터(x, y, z가 각각 2바이트) 1회 분량이
		// 6바이트이고, 6묶음해서 총 36바이트		
		if( (flashMap.flashPageChecker + 36) > 256 ) {
			flashMap.flashPageToRead = 256 - flashMap.flashPageChecker;			
			//printf("### 00 flashPageToRead : %u\n", flashMap.flashPageToRead);
		}
		// pageChecker가 255라는건 page의 단위가 바뀐다는 말이다. 따라서 36을 다 읽어와야한다. 
		else if( flashMap.flashPageChecker == 256) {
			flashMap.flashPageChecker = 0;
			flashMap.flashPageToRead = 36;
			//printf("### 01 flashPageToRead : %u\n", flashMap.flashPageToRead);
		}
		else {
			flashMap.flashPageToRead = 36;
			//printf("### 02 flashPageToRead : %u\n", flashMap.flashPageToRead);
		}
		
		//printf("flashPageToRead, checker : %u, %u\n", flashMap.flashPageToRead, flashMap.flashPageChecker);
		
		/*
		flashMap.flashPageToRead = FLASH_PAGE_MAX - flashMap.flashPageChecker;
		if( flashMap.flashPageToRead >= 36 ) {
			flashMap.flashPageToRead = 36;
		}
		*/
		
		// 이하 코드가 count로부터 읽어들이게 되어있어서 값을 넣어준다. 
		// refactoring 필요
		count = flashMap.flashPageToRead;
		
		///*
		//printf("dequeue front : %u\n", dataSendBuffer.front);
		//*/
		
		// TODO 리팩토링
		/*
		for( iLoop = 0; iLoop < 20; iLoop++) {
			dataSendBuffer.sBufferTemp[iLoop] = 0;
		}
		*/
		
		// 플래시에 쓰려는 데이터는 36바이트이지만, 앞에 커맨드와 addr가 붙기때문에
		// 40바이트를 초기화한다. 
		memset(dataSendBuffer.sBufferTemp, 0, 40);
		
		// 읽으려는 데이터가 버퍼를 초과한다. 
		if(dataSendBuffer.front + count - 1 >= MAX_QUEUE_SIZE) {
			remainIndex = 4;
			remainLen = count;  
			
			///*
			//printf("dequeue 777 remainLen : %u\n", remainLen);
			//*/
		
			do {
				// 읽으려는 수가 맥스버퍼를 초과하기때문에 맥스버퍼에서 읽어올 수 있는 수를 계산한다.
				// remainToReadCount는 현재 버퍼에서 내가 읽일 수 있는 만큼의 수를 가진다.
				remainToReadCount = MAX_QUEUE_SIZE - dataSendBuffer.front;
				dataSendBuffer.front = (dataSendBuffer.front + 1) % MAX_QUEUE_SIZE;
				
			   // aaac = aaac + 1;
				
				/*            
				SHOW_MSG_STR("dequeue 777-8 aaac : ");
				SHOW_MSG_UINT16(aaac);
				SHOW_MSG_STR("\r\n");
				*/
				
				///*
				//printf("dequeue 777-1 remainToReadCount : %u\n", remainToReadCount);            
				//printf("dequeue 777-2 front : %u\n", dataSendBuffer.front);
				//*/
				
				// refactoring
				// 이 부분은 필요없어 보인다. 이미 위에서 읽어올 수보다 남은 수가 적다는게
				// 판단이 되어있기때문이다. 
				if( remainToReadCount >= remainLen) {
					///*
					//printf("dequeue 777-3 remainLen : %u\n", remainLen);
					//*/
					remainToReadCount = remainLen;
				}
				
				// 읽어온 카운트가 이미 맥스버퍼를 가리키고 있으면서 읽어올 데이터는 아직 남아있다. 
				if( (remainToReadCount == 0) && (remainLen > 0) ) {
					remainToReadCount = remainLen;
					
					///*
					//printf("dequeue 777-3-1 remainLen : %u\n", remainLen);
					//*/
									
					if( remainToReadCount >= 36 ) {
						remainToReadCount = 36;
						///*
						//printf("dequeue 777-3-2 remainToReadCount : %u\n", remainToReadCount);
						//*/
					}
				}
				
				/*
				SHOW_MSG_STR("*** sBuffer : ");
				for( i = 0; i < remainToReadCount; i++ ) {
					SHOW_MSG_CHAR( dataSendBuffer.queue[i] );         
				}
				SHOW_MSG_STR("..\r\n");
				*/
				
				///*
				//printf("dequeue 777-4-1 remainIndex : %u\n", remainIndex);            
				//printf("dequeue 777-4 remainToReadCount : %u\n", remainToReadCount);            
				//printf("dequeue 777-4 front : %u\n", dataSendBuffer.front);
				//*/
							
				// flash에 기록할 데이터를 뽑아온다.
				memcpy(&dataSendBuffer.sBufferTemp[remainIndex], &dataSendBuffer.queue[dataSendBuffer.front], remainToReadCount);
				remainIndex = remainIndex + remainToReadCount;
				flashMap.flashPageChecker += remainToReadCount;
				printf("checkcheck 00 : %u\n", flashMap.flashPageChecker);
				
		
				/*
				SHOW_MSG_STR("dequeue 777-4-2 remainLen : ");
				SHOW_MSG_UINT16(remainLen);
				SHOW_MSG_STR("\r\n");
				
				SHOW_MSG_STR("dequeue 777-4-3 remainToReadCount : ");
				SHOW_MSG_UINT16(remainToReadCount);
				SHOW_MSG_STR("\r\n");
				*/
				
				remainLen = remainLen - remainToReadCount;
				
				/*
				SHOW_MSG_STR("dequeue 777-5 remainIndex : ");
				SHOW_MSG_UINT16(remainIndex);
				SHOW_MSG_STR("\r\n");
				
				SHOW_MSG_STR("dequeue 777-6 remainLen : ");
				SHOW_MSG_UINT16(remainLen);
				SHOW_MSG_STR("\r\n");
				*/
				
				dataSendBuffer.front = dataSendBuffer.front + remainToReadCount - 1;
				
				/*
				SHOW_MSG_STR("dequeue 777-7 front : ");
				SHOW_MSG_UINT16(dataSendBuffer.front);
				SHOW_MSG_STR("\r\n");
				*/
				
				/*
						if( aaac == 2) {
					SHOW_MSG_STR("xxxxxxx aaac is 2\r\n ");
					break;          
				}
				*/
				

				
			} while( remainLen > 0 );
			
			/*
			for(iLoop = 0; iLoop < count; iLoop++) {
				maxValue = dataSendBuffer.front + 1;
				
				if( maxValue == 40 ) {
					dataSendBuffer.front = 40; //(dataSendBuffer.front + 1 + 1) % MAX_QUEUE_SIZE;
				}
				else {
					dataSendBuffer.front = (dataSendBuffer.front + 1) % MAX_QUEUE_SIZE;
				}
				
				dataSendBuffer.sBufferTemp[iLoop] = dequeueEach();
				
			}        
			*/
		}
		// 읽으려는 데이터가 max버퍼를 넘지 않는다. 즉, 한번에 읽을 수 있다.
		else {
			dataSendBuffer.front = (dataSendBuffer.front + 1) % MAX_QUEUE_SIZE;
			
			// 남은 잔량이 내가 읽으려는 수보다 크다. 즉, 한번에 읽을 수 있다.          
			//printf("dequeue read at once, %u\n", flashMap.flashPageToRead);
			memcpy(&dataSendBuffer.sBufferTemp[4], &dataSendBuffer.queue[dataSendBuffer.front], flashMap.flashPageToRead);
			count = flashMap.flashPageToRead;
			
			flashMap.flashPageChecker += count;
			//printf("checkcheck 11 : %u\n", flashMap.flashPageChecker);

			dataSendBuffer.front = dataSendBuffer.front + count - 1;
		}
		
		///*    
	//	printf("in dequeue 10 \n");
		
		/*
		for( iLoop = 0; iLoop < 4; iLoop++ ) {
			printf("*** sBuffer : %c, %c, %c, %c, %c, %c, %c, %c, %c, %c\n", dataSendBuffer.sBufferTemp[iLoop * 10], dataSendBuffer.sBufferTemp[iLoop * 10 + 1] 
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 2], dataSendBuffer.sBufferTemp[iLoop * 10 + 3], dataSendBuffer.sBufferTemp[iLoop * 10 + 4]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 5], dataSendBuffer.sBufferTemp[iLoop * 10 + 6], dataSendBuffer.sBufferTemp[iLoop * 10 + 7]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 8], dataSendBuffer.sBufferTemp[iLoop * 10 + 9]);         
		}
		*/
		
		printf("in dequeue \n");
		/*
		for( iLoop = 0; iLoop < 4; iLoop++ ) {
			printf("*** sBuffer : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", dataSendBuffer.sBufferTemp[iLoop * 10], dataSendBuffer.sBufferTemp[iLoop * 10 + 1] 
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 2], dataSendBuffer.sBufferTemp[iLoop * 10 + 3], dataSendBuffer.sBufferTemp[iLoop * 10 + 4]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 5], dataSendBuffer.sBufferTemp[iLoop * 10 + 6], dataSendBuffer.sBufferTemp[iLoop * 10 + 7]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 8], dataSendBuffer.sBufferTemp[iLoop * 10 + 9]);         
		}
		*/
		
		//printf("in dequeue 11 %c\n", dataSendBuffer.sBufferTemp[4]);
		//printf("in dequeue 12 %u\n", dataSendBuffer.sBufferTemp[4]);
		
		/*
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		
		printf("in dequeue  22 \n");
		*/
		
		/*
		SHOW_MSG_STR("dequeue read count : ");
		SHOW_MSG_UINT16(count);
		SHOW_MSG_STR("\r\n");
		*/
		
		/*
		// 데이터를 사용하면 된다.
		GattCharValueNotification(appData.connectedUcid,
											   HANDLE_UART_WRITE_VALUE,
											   count,
											   (uint8*)dataSendBuffer.sBufferTemp); 
		*/
		
		if( (flash_addr + flashMap.flashPageToRead) > FLASH_TARGET_SIZE ) {
			printf("+full, stop recording\n");
			
			// 메모리가 풀이면 더이상 기록하지 않는다.
			passHandler = true;  // 더 이상 모션센서를 기록하지 않는다.
			readRun = false;     // 메모리에서 값을 읽어서 보내지 않는다.
			
			notiFlashWriteFull(); // 메모리가 꽉 찼음을 앱에 알린다.
		}
		else {
			SAVE_DATA = true;
			isSaveData = true;
		}
		
		
		
		if(is_empty()) 
		{ 
			dataSendBuffer.sending = FALSE;
		} 
	}
	else {
		dataSendBuffer.sending = FALSE;
		printf("Rundequeue, less than 36\n");
	}		
} 

static void spi_send_recv(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode)
{
    // Initalize buffers.
   // init_buffers(p_tx_data, p_rx_data, 8);

    // Start transfer.
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master, p_tx_data, len, p_rx_data, len);
	
	if( err_code != 0 ) {
		printf("send_recv result : %u\n", err_code);
	}
	//printf("send_recv result : %u\n", err_code);
	//printf("spi rx : %x, %x, %x\n", p_rx_data[1], p_rx_data[2], p_rx_data[3]);
	
	if( rw_mode == RW_MODE_R ) {
		showReadData(len);
		//nrf_delay_ms(100);
	}
	
//	

 //   APP_ERROR_CHECK(err_code);
    nrf_delay_ms(DELAY_MS_100);
}

static void spi_send_recv2(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode)
{
    // Initalize buffers.
   // init_buffers(p_tx_data, p_rx_data, 8);

    // Start transfer.
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master2, p_tx_data, len, p_rx_data, len);
	
	nrf_delay_ms(2);
//	printf("s\n");
	if( err_code != 0 ) {
		printf("2 result: %u\n", err_code);
	}
	//printf("spi rx : %x, %x, %x\n", p_rx_data[1], p_rx_data[2], p_rx_data[3]);
	
	nrf_gpio_pin_set(26);
	
	if( rw_mode == RW_MODE_R ) {
//		showReadData(len);
		//nrf_delay_ms(100);
	}
	
//	

 //   APP_ERROR_CHECK(err_code);
    //nrf_delay_ms(1000);
}

static void spi_send_recv3(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode)
{
    // Start transfer.	
	m_transfer_completed2 = false;	
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master2, p_tx_data, len, p_rx_data, len);	
	while (m_transfer_completed2 == false);
	
	if( err_code != 0 ) {
		printf("send 3 result: %u\n", err_code);
	}
}

static void showReadData(uint16_t len) {
	/*
	uint16_t iLoop;
	
	for( iLoop = 0; iLoop < 10; iLoop++) {
		printf("Read 1: %x, %x, %x, %x \n", m_rx_data[iLoop * 4], m_rx_data[iLoop * 4 + 1], 
											 m_rx_data[iLoop * 4 + 2], m_rx_data[iLoop * 4 + 3] );
	}		
	
	printf("end of showReadData \n");
	*/
}

static void spi_master_event_handler(nrf_drv_spi_event_t event)
{
    uint32_t err_code = NRF_SUCCESS;
    bool result = false;
	
//	printf("event handler \n");

    switch (event)
    {
        case NRF_DRV_SPI_EVENT_DONE:
		//	printf("e1 handler, done \n");
            // Check if data are valid.

            result = buf_check(m_rx_data, TX_RX_BUF_LENGTH);
            APP_ERROR_CHECK_BOOL(result);

            err_code = bsp_indication_set(BSP_INDICATE_RCV_OK);
            APP_ERROR_CHECK(err_code);

            // Inform application that transfer is completed.
            m_transfer_completed = true;
            break;

        default:
			//		printf("event handler, default \n");
            // No implementation needed.
            break;
    }
}

static void spi_master_event_handler2(nrf_drv_spi_event_t event)
{
    uint32_t err_code = NRF_SUCCESS;
    bool result = false;
	
//	printf("event handler 2222222222222 \n");

    switch (event)
    {
        case NRF_DRV_SPI_EVENT_DONE:
			//printf("e2 handler, done \n");
            // Check if data are valid.
		/* oledmodi
            result = buf_check(m_rx_data, TX_RX_BUF_LENGTH);
            APP_ERROR_CHECK_BOOL(result);

            err_code = bsp_indication_set(BSP_INDICATE_RCV_OK);
            APP_ERROR_CHECK(err_code);
		*/

            // Inform application that transfer is completed.
            m_transfer_completed2 = true;
            break;

        default:
			//		printf("event handler, default \n");
            // No implementation needed.
            break;
    }
}

static __INLINE bool buf_check(uint8_t * p_buf, uint16_t len)
{
    uint16_t i;

    for (i = 0; i < len; i++)
    {
        if (p_buf[i] != (uint8_t)('a' + i))
        {
            return false;
        }
    }

    return true;
}

void bsp_configuration()
{
    uint32_t err_code = NRF_SUCCESS;

    NRF_CLOCK->LFCLKSRC            = (CLOCK_LFCLKSRC_SRC_Xtal << CLOCK_LFCLKSRC_SRC_Pos);
    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_LFCLKSTART    = 1;

    while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0)
    {
        // Do nothing.
    }

    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, NULL);
        
    err_code = bsp_init(BSP_INIT_LED, APP_TIMER_TICKS(100, APP_TIMER_PRESCALER), NULL);
    APP_ERROR_CHECK(err_code);
}

//////////////////////////
// 네이버 새로운 알고리즘
// 2015.12.14
//////////////////////////

int footprint(struct acc *stat, long timestamp){
    int i, count=0;

    stat->last_peak_time = timestamp;
    stat->step_window[stat->step_index] = timestamp;
    stat->step_index = (stat->step_index + 1)%SIZE;

    for(i=0; i<SIZE; i++){
        int ii = (stat->step_index - 1 - i + SIZE) % SIZE;
        if(timestamp - stat->step_window[ii] > STEP_WINDOW_DURATION) break;
        count++;
    }

    if(count > STEP_WINDOW_THRES) return 1;
    return 0;
}



int update(struct acc *stat, int _ax, int _ay, int _az){
    double ax = _ax / AS * GRAVITY;
    double ay = _ay / AS * GRAVITY;
    double az = _az / AS * GRAVITY;

    double scalar = pow(ax*ax + ay*ay + az*az, 0.5);
    double avg, stdev;

    int temp_state = -3;
    int i, ib;
    int si, sib, sibb;

//    struct timeval te;
    long timestamp;

    int ret = 0;

	/*
    gettimeofday(&te, NULL);
    timestamp = te.tv_sec*1000 + te.tv_usec/1000;
	*/
	
	timestamp = dataTimeStamp.data_count;

    /* TODO: Hz가 짧은 시간안에 지속적으로 바뀌는 경우가 아니면,
     * Hz 변경되는 경우에만 val_sum, sq_sum을 다시 계산하는 기능이 추가되어야함 */
    stat->val_sum += scalar - stat->window[stat->index];
    stat->sq_sum += scalar*scalar - stat->window[stat->index]*stat->window[stat->index];
    avg = stat->val_sum / SIZE;
    stdev = pow(stat->sq_sum/SIZE - avg*avg, 0.5);

    /* update window */
    stat->window[stat->index] = scalar;
    stat->index = get_index(stat->index, 1, SIZE);

    i = get_index(stat->index, -1, SIZE);
    ib = get_index(stat->index, -2, SIZE);

    /* state transition */
    if(stdev < STDEV_THRES){
        temp_state = 0;
    }else{
        if(stat->window[i] >= avg + stdev) temp_state = 2;
        else if(stat->window[i] >= avg + stdev/2) temp_state = 1;
        else if(stat->window[i] >= avg) temp_state = 0;
        else if(stat->window[i] >= avg - stdev/2) temp_state = -1;
        else temp_state = -2;
    }
	
	/*
	printf("stat->index: %d, size: %d, max_size: %d, %d %d\n", stat->index, SIZE, MAX_SIZE, i, ib);
    printf("scalar: %lf, val_sum: %lf, sq_sum: %lf\n", scalar, stat->val_sum, stat->sq_sum);
    printf("timestamp: %ld, avg: %lf, stdev: %lf, state: %d\n", timestamp, avg, stdev, temp_state);
    printf("timestamp - stat->last_peak_time=%ld, %ld\n", (timestamp - stat->last_peak_time), STEP_MIN_INTERVAL);
    printf("--\n");
	*/

    /* determin the current state */
    if(temp_state != stat->state_window[(stat->state_index + SIZE - 1)%SIZE]){
        stat->state_window[stat->state_index] = temp_state;
        stat->state_timestamp[stat->state_index] = timestamp;
        stat->state_index = get_index(stat->state_index, 1, SIZE);
    }else{
        goto OUT;
    }	

	/*
#ifdef __DEBUG__
    printf("stat->index: %d, size: %d, max_size: %d, %d %d\n", stat->index, SIZE, MAX_SIZE, i, ib);
    printf("scalar: %lf, val_sum: %lf, sq_sum: %lf\n", scalar, stat->val_sum, stat->sq_sum);
    printf("timestamp: %ld, avg: %lf, stdev: %lf, state: %d\n", timestamp, avg, stdev, temp_state);
    printf("timestamp - stat->last_peak_time=%ld, %ld\n", (timestamp - stat->last_peak_time), STEP_MIN_INTERVAL);
    printf("--\n");
#endif
	*/

    /* determin whether the current state is peak */
    si = get_index(stat->state_index, -1, SIZE);
    sib = get_index(stat->state_index, -2, SIZE);
    sibb = get_index(stat->state_index, -3, SIZE);

    if(timestamp - stat->last_peak_time >= STEP_MIN_INTERVAL
        && stdev > STDEV_THRES 
        && ((stat->state_window[si] == 2
            && stat->state_timestamp[si] - stat->state_timestamp[sib] < STATE_KEEP_DURATION)
            || (stat->state_window[sib] == 1 
            && stat->state_window[sibb] < stat->state_window[sib]
            && stat->state_timestamp[sib] - stat->state_timestamp[sibb] < STATE_KEEP_DURATION)
        )){
        ret = footprint(stat, timestamp);
    }
OUT:
    return ret;
}

static void getChecksum(char* value, uint8 size, uint16 *total) {
 //   int checksum = 0;
//    return checksum;
//    /*
    uint8 bb = size;
    uint8 i = 0;
    uint16 sum = 0;
//    uint8 checksum = 0;
//    uint8 cs = 0;
    
    //printf("check loop size : %d\n", size);   
 
	for (i = 0; i < bb; i++) {
		sum += value[i];
        
        //printf("check sum : %d\n", sum);
	}
    
    //printf("tatal check sum : %d\n", sum);
    
    *total = sum;
}

static void notiFlashEraseComplete() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x5;  // flash full erase complete

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("EraseComplete: %u\n", err_code);	
}

static void notiToApp(uint8 code) {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = code;

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("EraseComplete: %u\n", err_code);	
}

static void notiToAppRealtimeStep(uint8 count) {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	printf("send realtime step start %d\n", count);
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xDA;
	row_data[2] = 0x1;
	row_data[3] = count;

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("EraseComplete: %u\n", err_code);	
	
	printf("send realtime step end\n");
}

static void notiToAppFirmwareVersion() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	printf("notiToAppFirmwareVersion start\n");
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xDA;
	row_data[2] = 0x2;
	row_data[3] = FIRM_REV_MAJOR;
	row_data[4] = FIRM_REV_MINOR;

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[5] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("EraseComplete: %u\n", err_code);	
	
	printf("notiToAppFirmwareVersion end\n");
}

static void notiFlashWriteFull() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x6;  // flash write full

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("hWriteFull: %u\n", err_code);	
}

static void notiFlashReadFull() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x7;  // flash read full

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("ReadFull: %u\n", err_code);	
}

static void notiFlashReadLast() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x8;  // flash read last

	getChecksum(row_data, row_data[2] + 3, &acca );
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("send, notiFlashReadLast : %u\n", err_code);	
}

static void ssd1306_write_byte(unsigned char chData, unsigned char chCmd) 
{
	_LO_CS();

	if(chCmd) 
		_HI_DC();
	else 
		_LO_DC();
	
	//printf("w b \n");

	m_transfer_completed2 = false;
	spi_send_recv2(&chData, m_rx_data, 1, RW_MODE_W);
	while (m_transfer_completed2 == false);

	// ** μo·¹AI CE¿a **
	//nrf_delay_ms(2);

	_HI_DC();
	_HI_CS();
}  

// 전화번호를 우->좌로 스크롤함. 64 by 32 전체 스크롤
void ssd1306_refresh_gram_h_scroll(int mode) {
	uint16 i = 0, j = 0, iLoop = 0;
	uint8 pos = 0;
	
	nrf_gpio_pin_set(26);
	
	printf("R1 1\n");
	
	for( iLoop = 0; iLoop < 100; iLoop++) {
		for( i = 0; i < 4; i++)  {
			m_transfer_completed2 = false;
			ssd1306_write_byte(0xB0 + i, SSD1306_CMD);   
			while (m_transfer_completed2 == false);		
			
			m_transfer_completed2 = false;
			//ssd1306_write_byte(0x02, SSD1306_CMD); 
			ssd1306_write_byte(0x10, SSD1306_CMD); 
			while (m_transfer_completed2 == false);		
			
			m_transfer_completed2 = false;
			//ssd1306_write_byte(0x10, SSD1306_CMD); 
			ssd1306_write_byte(0x02, SSD1306_CMD); 
			while (m_transfer_completed2 == false);		
					
			// 64면 전화번호를 한줄에 나열할 수가 없다. 따라서 버퍼는 그 이상이며(90)
			// 이걸 64화면에 보이도록 우->좌로 스크롤해준다.
			// 128
			for( j = 0; j < 64; j++ ) {
				pos = j + iLoop;
				
				if( (pos >= 90) && (pos < 179) ) {
					pos = pos - 90;
				}
				else if( pos >= 179 ) {
					pos = pos - 179;
				}
				
				oled_data[j] = s_chDispalyBuffer[pos][i];
				
				//printf("R1, %d, %d\n", pos, i);
			}		
			
			m_transfer_completed2 = false;
			spi_send_recv2(oled_data, m_rx_data, 128, RW_MODE_W);
			while (m_transfer_completed2 == false);
			
			//nrf_delay_ms(1000);
		}	
		
		printf("R2 1\n");
		
		// ** μo·¹AI CE¿a **
		//nrf_delay_ms(DELAY_MS + 5);
		nrf_delay_ms(2);
		nrf_gpio_pin_set(26);
	}	
}

// 전화번호를 우->좌로 스크롤함. 64 by 16 하단 스크롤
void ssd1306_refresh_gram_h_scroll_bottom(int mode) {
	uint16 i = 0, j = 0, iLoop = 0;
	uint8 pos = 0;
	
	nrf_gpio_pin_set(26);
	
	printf("R1 2 \n");
	
	for( iLoop = 0; iLoop < 100; iLoop++) {
		for( i = 0; i < 2; i++)  {
			m_transfer_completed2 = false;
			ssd1306_write_byte(0xB0 + i, SSD1306_CMD);   
			while (m_transfer_completed2 == false);		
			
			m_transfer_completed2 = false;
			//ssd1306_write_byte(0x02, SSD1306_CMD); 
			ssd1306_write_byte(0x10, SSD1306_CMD); 
			while (m_transfer_completed2 == false);		
			
			m_transfer_completed2 = false;
			//ssd1306_write_byte(0x10, SSD1306_CMD); 
			ssd1306_write_byte(0x02, SSD1306_CMD); 
			while (m_transfer_completed2 == false);		
					
			// 64면 전화번호를 한줄에 나열할 수가 없다. 따라서 버퍼는 그 이상이며(90)
			// 이걸 64화면에 보이도록 우->좌로 스크롤해준다.
			// 128
			for( j = 0; j < 64; j++ ) {
				pos = j + iLoop;
				
				if( (pos >= 90) && (pos < 179) ) {
					pos = pos - 90;
				}
				else if( pos >= 179 ) {
					pos = pos - 179;
				}
				
				oled_data[j] = s_chDispalyBuffer[pos][i];
				
				//printf("R1, %d, %d\n", pos, i);
			}		
			
			m_transfer_completed2 = false;
			spi_send_recv2(oled_data, m_rx_data, 128, RW_MODE_W);
			while (m_transfer_completed2 == false);
			
			//nrf_delay_ms(1000);
		}	
		
		printf("R2 2\n");
		
		// ** μo·¹AI CE¿a **
		//nrf_delay_ms(DELAY_MS + 5);
		nrf_delay_ms(2);
		nrf_gpio_pin_set(26);
	}	
}

void ssd1306_refresh_gram_4_128_test(int mode) {
	uint16 i = 0, j = 0;
	
	nrf_gpio_pin_set(26);
	
	printf("R1 3\n");
	
	for( i = 0; i < 4; i++)  {
		m_transfer_completed2 = false;
		ssd1306_write_byte(0xB0 + i, SSD1306_CMD);   
		while (m_transfer_completed2 == false);		
		
		m_transfer_completed2 = false;
		//ssd1306_write_byte(0x02, SSD1306_CMD); 
		ssd1306_write_byte(0x10, SSD1306_CMD); 
		while (m_transfer_completed2 == false);		
		
		m_transfer_completed2 = false;
		//ssd1306_write_byte(0x10, SSD1306_CMD); 
		ssd1306_write_byte(0x02, SSD1306_CMD); 
		while (m_transfer_completed2 == false);		
		
		for( j = 0; j < 128; j++ ) {
			oled_data[j] = s_chDispalyBuffer[j][i];
		}		
		
		m_transfer_completed2 = false;
		spi_send_recv2(oled_data, m_rx_data, 128, RW_MODE_W);
		while (m_transfer_completed2 == false);
		
		//nrf_delay_ms(1000);
	}	
	
	printf("R2 3\n");
	
	// ** μo·¹AI CE¿a **
	//nrf_delay_ms(DELAY_MS + 5);
	nrf_delay_ms(2);
	nrf_gpio_pin_set(26);	
}

void spi_send_recv_new(uint8_t * const p_tx_data,
                   uint8_t * const p_rx_data,
                   const uint16_t len)
{
	//printf("new\n");
    m_transfer_completed2 = false;
    // Start transfer.
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master,
                         p_tx_data, len, p_rx_data, len);
	
	if( err_code != 0 ) {
		printf("^^^^^^^ send new : %u\n", err_code);
	}
	
    while (!m_transfer_completed2);
}


uint8_t* spi_transfer(uint8_t * message, const uint16_t len)
{
    memcpy((void*)m_tx_data, (void*)message, len);
    spi_send_recv_new(m_tx_data, m_rx_data, len);
    return m_rx_data;
}

void ssd1306_command(uint8_t c)
{
	_LO_CS();
	_LO_DC();
	//_LO_CS();
	UNUSED_VARIABLE(spi_transfer(&c, 1));
	//spi_transfer(&c, 1);
	_HI_CS();
}

/*
void ssd1306_refresh_gram(int mode) {
	uint16 i = 0, j = 0;
	
	//nrf_gpio_pin_set(26);
	
	printf("R1 4\n");
	
	for( i = 0; i < 4; i++)  {
		//m_transfer_completed2 = false;
		ssd1306_write_byte(0xB0 + i, SSD1306_CMD);   
		printf("11\n");
		//while (m_transfer_completed2 == false);		
		
		nrf_delay_ms(1);
		
		//m_transfer_completed2 = false;
		ssd1306_write_byte(0x02, SSD1306_CMD); 
		//ssd1306_write_byte(0x10, SSD1306_CMD); 
		printf("22\n");
		//while (m_transfer_completed2 == false);		
		
		nrf_delay_ms(1);
		
		//m_transfer_completed2 = false;
		ssd1306_write_byte(0x10, SSD1306_CMD); 
		//ssd1306_write_byte(0x02, SSD1306_CMD); 
		printf("33\n");
		//while (m_transfer_completed2 == false);		
		
		for( j = 0; j < 64; j++ ) {
			oled_data[j] = s_chDispalyBuffer[j][i];
			
			//m_transfer_completed2 = false;
			ssd1306_write_byte(oled_data[j], SSD1306_DAT);   
			//printf("11\n");
			//while (m_transfer_completed2 == false);		
		}		
		
		//nrf_delay_ms(2);
		
		//m_transfer_completed2 = false;
		//spi_send_recv2(oled_data, m_rx_data, 64, RW_MODE_W);
		//printf("44\n");
		//while (m_transfer_completed2 == false);
		
		//nrf_delay_ms(2);
		//nrf_delay_ms(1000);
	}	
	
	printf("R2 4\n");
	
	// ** μo·¹AI CE¿a **
	//nrf_delay_ms(DELAY_MS + 5);
	//nrf_delay_ms(2);
	//nrf_gpio_pin_set(26);	
}
*/

///* ori
void ssd1306_refresh_gram(int mode) {
	uint16 i = 0, j = 0;
	
	nrf_gpio_pin_set(26);
	
	printf("R1 4\n");
	
	for( i = 0; i < 4; i++)  {
		m_transfer_completed2 = false;
		ssd1306_write_byte(0xB0 + i, SSD1306_CMD);   
		printf("11\n");
		while (m_transfer_completed2 == false);		
		
		nrf_delay_ms(1);
		
		m_transfer_completed2 = false;
		ssd1306_write_byte(0x02, SSD1306_CMD); 
		//ssd1306_write_byte(0x10, SSD1306_CMD); 
		printf("22\n");
		while (m_transfer_completed2 == false);		
		
		nrf_delay_ms(1);
		
		m_transfer_completed2 = false;
		ssd1306_write_byte(0x10, SSD1306_CMD); 
		//ssd1306_write_byte(0x02, SSD1306_CMD); 
		printf("33\n");
		while (m_transfer_completed2 == false);		
		
		for( j = 0; j < 64; j++ ) {
			oled_data[j] = s_chDispalyBuffer[j][i];
		}		
		
		nrf_delay_ms(2);
		
		m_transfer_completed2 = false;
		spi_send_recv2(oled_data, m_rx_data, 64, RW_MODE_W);
		printf("44\n");
		while (m_transfer_completed2 == false);
		
		nrf_delay_ms(2);
		//nrf_delay_ms(1000);
	}	
	
	printf("R2 4\n");
	
	// ** μo·¹AI CE¿a **
	//nrf_delay_ms(DELAY_MS + 5);
	nrf_delay_ms(2);
	nrf_gpio_pin_set(26);	
}
//*/

void ssd1306_display(void)
{
    ssd1306_command(0x21);
    ssd1306_command(0);   // Column start address (0 = reset)
    ssd1306_command(128 - 1); // Column end address (127 = reset)

    ssd1306_command(0x22);
    ssd1306_command(0); // Page start address (0 = reset)

    ssd1306_command(7); // Page end address


        _HI_CS();
        _HI_DC();
        _LO_CS();
        for (uint16_t i = 0; i < (128 * 64 / 8); i++) {
            UNUSED_VARIABLE(spi_transfer(&buffer[i], 1));
        }
        _HI_CS();

}
/*
void ssd1306_display(void) {
	uint16 i = 0, j = 0;
	
	//nrf_gpio_pin_set(26);
	
	printf("R1 4, newnew\n");
	
	for( i = 0; i < 4; i++)  {
	
		m_transfer_completed2 = false;
		ssd1306_write_byte(0xB0 + i, SSD1306_CMD);   
		printf("11\n");
		while (m_transfer_completed2 == false);		
	
		
		ssd1306_command(0xB0 + i);
		
		nrf_delay_ms(1);
		
		m_transfer_completed2 = false;
		ssd1306_write_byte(0x02, SSD1306_CMD); 
		//ssd1306_write_byte(0x10, SSD1306_CMD); 
		printf("22\n");
		while (m_transfer_completed2 == false);		
		
		nrf_delay_ms(1);
		
		m_transfer_completed2 = false;
		ssd1306_write_byte(0x10, SSD1306_CMD); 
		//ssd1306_write_byte(0x02, SSD1306_CMD); 
		printf("33\n");
		while (m_transfer_completed2 == false);		
		
		for( j = 0; j < 64; j++ ) {
			oled_data[j] = s_chDispalyBuffer[j][i];
		}		
		
		nrf_delay_ms(2);
		
		m_transfer_completed2 = false;
		spi_send_recv2(oled_data, m_rx_data, 64, RW_MODE_W);
		printf("44\n");
		while (m_transfer_completed2 == false);
		
		nrf_delay_ms(2);
		//nrf_delay_ms(1000);
	}	
	
	printf("R2 4, newnew\n");
	
	// ** μo·¹AI CE¿a **
	//nrf_delay_ms(DELAY_MS + 5);
	nrf_delay_ms(2);
	//nrf_gpio_pin_set(26);		
}
*/

void ssd1306_clear_buffer_128(unsigned char chFill)  
{ 
	unsigned char i, j;

	for (i = 0; i < 4; i ++) 
	{
		for (j = 0; j < 128; j ++) 
		{
			s_chDispalyBuffer[j][i] = chFill;
		}
	}
}

void ssd1306_clear_screen(unsigned char chFill)  
{ 
	unsigned char i, j;

	for (i = 0; i < 4; i ++) 
	{
		m_transfer_completed2 = false;
		ssd1306_write_byte(0xB0 + i, SSD1306_CMD);
		while (m_transfer_completed2 == false);	
		
		m_transfer_completed2 = false;
		//ssd1306_write_byte(0x02, SSD1306_CMD); 
		ssd1306_write_byte(0x10, SSD1306_CMD); 
		while (m_transfer_completed2 == false);	
		
		m_transfer_completed2 = false;
		//ssd1306_write_byte(0x10, SSD1306_CMD); 
		ssd1306_write_byte(0x02, SSD1306_CMD); 
		while (m_transfer_completed2 == false);	
		
		for (j = 0; j < 64; j ++) 
		{
			s_chDispalyBuffer[j][i] = chFill;
		}
	}
	
	//ssd1306_draw_0608char(0,54,'1');

	//ssd1306_refresh_gram( DIS_ONE_BYTE ); //( DIS_MULTI_BYTE );
	m_transfer_completed2 = false;
	ssd1306_refresh_gram( DIS_MULTI_BYTE ); //( DIS_MULTI_BYTE );
	while (m_transfer_completed2 == false);	
}

void ssd1306_power_on(void)
{
    nrf_gpio_pin_set(SSD1306_CONFIG_VDD_PIN); // vdd
    nrf_gpio_cfg(
        SSD1306_CONFIG_VDD_PIN,
        NRF_GPIO_PIN_DIR_OUTPUT,
        NRF_GPIO_PIN_INPUT_DISCONNECT,
        NRF_GPIO_PIN_NOPULL,
        NRF_GPIO_PIN_H0H1, // NRF_GPIO_PIN_S0S1,
        NRF_GPIO_PIN_NOSENSE);
}

static void oledInit(uint32_t dc, uint32_t rs, uint32_t cs, uint32_t clk, uint32_t mosi) {	
	/*
	uint8 oledBuffer[24] = {0xAE, 0xD5, 0x80, 0xA8, 0x3F, 0xD3, 0x00, 0x40, 0xAD, 0x8B, 0x32, 0xA1, 0xC8, 0xDA, 0x12, 
			0x81, 0xBB, 0xD9, 0x22, 0xDB, 0x40, 0xA4, 0xA6, 0xAF};
		*/	
	_dc = dc;
	_rs = rs;
	_cs = cs;
	
	nrf_gpio_cfg_output(dc);
	nrf_gpio_cfg_output(rs);
	_HI_CS();
    nrf_gpio_cfg_output(rs);
	
	// OLED
	nrf_drv_spi_config_t const config2 =
    {
        #if (SPI2_ENABLED == 1)
            .sck_pin  = clk,
            .mosi_pin = mosi,
            .miso_pin = NRF_DRV_SPI_PIN_NOT_USED,//SPIM2_MISO_PIN,
            .ss_pin   = NRF_DRV_SPI_PIN_NOT_USED,//SPIM2_SS_PIN,
        #endif
        .irq_priority = APP_IRQ_PRIORITY_LOW,
        .orc          = 0xCC,
        .frequency    = NRF_DRV_SPI_FREQ_8M, //NRF_DRV_SPI_FREQ_500K, //NRF_DRV_SPI_FREQ_1M, //NRF_DRV_SPI_FREQ_1M,
        .mode         = NRF_DRV_SPI_MODE_3,//NRF_DRV_SPI_MODE_0,
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST, //NRF_DRV_SPI_BIT_ORDER_LSB_FIRST,
				//.ss_pin       = NRF_DRV_SPI_PIN_NOT_USED,
    };
	ret_code_t err_code = nrf_drv_spi_init(&m_spi_master2, &config2, spi_master_event_handler2);	
	
	//if ( 1 ) { // reset인가?
		// Setup reset pin direction (used by both SPI and I2C)
		_HI_RS();
		// VDD (3.3V) goes high at start, lets just chill for a ms
		nrf_delay_ms(1);
		// bring reset low
		_LO_RS();
		// wait 10ms
		nrf_delay_ms(10);
		// bring out of reset
		_HI_RS();
		// turn on VCC (9V?)
	//}
	
	
	//nrf_gpio_pin_clear(26);  // dc is clear
	//nrf_gpio_pin_set(24);    // reset is set			
			
	m_transfer_completed2 = false;	
	ssd1306_write_byte(0xAE, SSD1306_CMD);//--turn off oled panel
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x00, SSD1306_CMD);//---set low column address
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x10, SSD1306_CMD);//---set high column address
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x40, SSD1306_CMD);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x81, SSD1306_CMD);//--set contrast control register
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xCF, SSD1306_CMD);// Set SEG Output Current Brightness
	while (m_transfer_completed2 == false);
		
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xA1, SSD1306_CMD);//--Set SEG/Column Mapping     
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xC0, SSD1306_CMD);//Set COM/Row Scan Direction   
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xA6, SSD1306_CMD);//--set normal display
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xA8, SSD1306_CMD);//--set multiplex ratio(1 to 64)
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x3f, SSD1306_CMD);//--1/64 duty
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xD3, SSD1306_CMD);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x00, SSD1306_CMD);//-not offset
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xd5, SSD1306_CMD);//--set display clock divide ratio/oscillator frequency
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x80, SSD1306_CMD);//--set divide ratio, Set Clock as 100 Frames/Sec
	while (m_transfer_completed2 == false);
		
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xD9, SSD1306_CMD);//--set pre-charge period
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xF1, SSD1306_CMD);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xDA, SSD1306_CMD);//--set com pins hardware configuration
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x12, SSD1306_CMD);
	while (m_transfer_completed2 == false);
		
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xDB, SSD1306_CMD);//--set vcomh
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x40, SSD1306_CMD);//Set VCOM Deselect Level
	while (m_transfer_completed2 == false);	
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x20, SSD1306_CMD);//-Set Page Addressing Mode (0x00/0x01/0x02)
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x02, SSD1306_CMD);//
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x8D, SSD1306_CMD);//--set Charge Pump enable/disable
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0x14, SSD1306_CMD);//--set(0x10) disable
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xA4, SSD1306_CMD);// Disable Entire Display On (0xa4/0xa5)
	while (m_transfer_completed2 == false);
		
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xA6, SSD1306_CMD);// Disable Inverse Display On (0xa6/a7) 
	while (m_transfer_completed2 == false);
	
	m_transfer_completed2 = false;
	ssd1306_write_byte(0xAF, SSD1306_CMD);//--turn on oled panel			
	while (m_transfer_completed2 == false);
}

void ssd1306_draw_point(unsigned char chXpos, unsigned char chYpos, unsigned char chPoint)
{
	unsigned char chPos, chBx, chTemp = 0;

	if (chXpos > 127 || chYpos > 63) 
		return;

	chPos = 7 - chYpos / 8;
	chBx = chYpos % 8;
	chTemp = 1 << (7 - chBx);

	if (chPoint) {
		//printf("aaa %d, %d\n", chXpos, chPos);
		s_chDispalyBuffer[chXpos][chPos] |= chTemp;
	}
	else {
		//printf("bbb %d, %d\n", chXpos, chPos);
		s_chDispalyBuffer[chXpos][chPos] &= ~chTemp;
	}
	
//	printf("draw point : %d, %d, %x\n", chXpos, chYpos, s_chDispalyBuffer[chXpos][chPos] );
}

void ssd1306_draw_bitmap(unsigned char chXpos, unsigned char chYpos, const unsigned char *pchBmp, unsigned char chWidth, unsigned char chHeight)
{
	unsigned int i, j, byteWidth = (chWidth + 7) / 8;

	for(j = 0; j < chHeight; j ++)
	{
		for(i = 0; i < chWidth; i ++ ) 
		{
			if(*(pchBmp + j * byteWidth + i / 8) & (128 >> (i & 7)))
				ssd1306_draw_point(chXpos + i, chYpos + j, 1);
		}
	}
}

void ssd1306_display_char_app(unsigned char chXpos, unsigned char chYpos, uint8 index, unsigned char chSize, unsigned char chMode)
{      	
	unsigned char i, j;
	unsigned char chTemp, chYpos0 = chYpos;			   
	
	//printf("@@ char : %d\n", chChr);
	for (i = 0; i < chSize; i ++)
	{   
		if (chSize == 12) 
		{
			if (chMode) {
				chTemp = c_chFont1608_app[index][i];
				//printf("111 \n");
			}
			else { 
				//printf("111 - 1 \n");
				chTemp = ~c_chFont1608_app[index][i];
			}
		} 
		else
		{
			if (chMode) {
				//printf("222 \n");
				chTemp = c_chFont1608_app[index][i];
			}
			else { 
				//printf("222 - 1 \n");
				chTemp = ~c_chFont1608_app[index][i];
			}
		}

		for (j = 0; j < 8; j ++) 
		{
			if (chTemp & 0x80) {
				//printf("333, %d, %d \n", chXpos, chYpos);
				ssd1306_draw_point(chXpos, chYpos, 1);
			}
			else {
				//printf("333 -1 \n");
				//printf("333, %d, %d \n", chXpos, chYpos);
				ssd1306_draw_point(chXpos, chYpos, 0);
			}
			
			chTemp <<= 1;
			chYpos ++;
			//chYpos ++;

			if ((chYpos - chYpos0) == chSize) 
			{
				chYpos = chYpos0;
				chXpos ++;
				break;
			}
		}  	 
	} 
}

void ssd1306_display_char(unsigned char chXpos, unsigned char chYpos, unsigned char chChr, unsigned char chSize, unsigned char chMode)
{      	
	unsigned char i, j;
	unsigned char chTemp, chYpos0 = chYpos;

	chChr = chChr - ' ';				   
	
	//printf("@@ char : %d\n", chChr);
	for (i = 0; i < chSize; i ++)
	{   
		if (chSize == 12) 
		{
			if (chMode) {
				chTemp = c_chFont1206[chChr][i];
				//printf("111 \n");
			}
			else { 
				//printf("111 - 1 \n");
				chTemp = ~c_chFont1206[chChr][i];
			}
		} 
		else
		{
			if (chMode) {
				//printf("222 \n");
				chTemp = c_chFont1608[chChr][i];
			}
			else { 
				//printf("222 - 1 \n");
				chTemp = ~c_chFont1608[chChr][i];
			}
		}

		for (j = 0; j < 8; j ++) 
		{
			if (chTemp & 0x80) {
				//printf("333, %d, %d \n", chXpos, chYpos);
				ssd1306_draw_point(chXpos, chYpos, 1);
			}
			else {
				//printf("333 -1 \n");
				//printf("333, %d, %d \n", chXpos, chYpos);
				ssd1306_draw_point(chXpos, chYpos, 0);
			}
			
			chTemp <<= 1;
			chYpos ++;
			//chYpos ++;

			if ((chYpos - chYpos0) == chSize) 
			{
				chYpos = chYpos0;
				chXpos ++;
				break;
			}
		}  	 
	} 
}

void ssd1306_display_string_app(unsigned char chXpos, unsigned char  chYpos, uint8 index, unsigned char  chSize, unsigned char  chMode)
{
	uint8 iLoop = 0;
	
	//while (*pchString != '\0')
	//{       
	for( iLoop = 0; iLoop < appFontData; iLoop++) {
		if (chXpos > (SSD1306_WIDTH - chSize / 2)) 
		{
			chXpos = 0;
			chYpos += chSize;
			if (chYpos > (SSD1306_HEIGHT - chSize))
			{
				chYpos = chXpos = 0;
				ssd1306_clear_screen(0x00);
			}
		}

		//ssd1306_display_char(chXpos, chYpos, *pchString, chSize, chMode);
		//ssd1306_display_char_app(chXpos, chYpos, index, chSize, chMode);
		
		printf("str, x, y, index :%d %d %d\n", chXpos, chYpos, iLoop);
		ssd1306_display_char_app(chXpos, chYpos, iLoop, chSize, chMode);
		
		chXpos += chSize / 2;
		//pchString ++;
	}
	//}
}

void ssd1306_display_string_h_scroll(unsigned char chXpos, unsigned char  chYpos, const unsigned char  *pchString, unsigned char  chSize, unsigned char  chMode)
{
	while (*pchString != '\0')
	{       
		if (chXpos > (128 - chSize / 2)) 
		{
			chXpos = 0;
			chYpos += chSize;
			if (chYpos > (64 - chSize))
			{
				chYpos = chXpos = 0;
				ssd1306_clear_screen(0x00);
			}
		}
		
		printf("A, %d, %d\n", chXpos, chYpos);

		ssd1306_display_char(chXpos, chYpos, *pchString, chSize, chMode);
		
		chXpos += chSize / 2;
		pchString ++;
	}
}

void ssd1306_display_string(unsigned char chXpos, unsigned char  chYpos, const unsigned char  *pchString, unsigned char  chSize, unsigned char  chMode)
{
	while (*pchString != '\0')
	{       
		if (chXpos > (SSD1306_WIDTH - chSize / 2)) 
		{
			chXpos = 0;
			chYpos += chSize;
			if (chYpos > (SSD1306_HEIGHT - chSize))
			{
				chYpos = chXpos = 0;
				ssd1306_clear_screen(0x00);
			}
		}
		
		printf("A1, %d, %d\n", chXpos, chYpos);

		ssd1306_display_char(chXpos, chYpos, *pchString, chSize, chMode);
		
		chXpos += chSize / 2;
		pchString ++;
	}
}

void ssd1306_draw_0608char(unsigned char chXpos, unsigned char chYpos, unsigned char chChar)
{
	unsigned char i, j;
	unsigned char chTemp = 0, chXpos0 = chXpos, chMode = 0;

	for (i = 0; i < 8; i ++)
	{
		chTemp = c_chFont0608[chChar - 0x30][i];
		
		for (j = 0; j < 8; j ++) 
		{
			chMode = chTemp & 0x80? 1 : 0; 
			ssd1306_draw_point(chXpos, chYpos, chMode);
			chTemp <<= 1;
			chXpos ++;
			
			
			if ((chXpos - chXpos0) == 6) 
			{
				chXpos = chXpos0;
				chYpos ++;
				break;
			}
		}
	}
}

void showOledCount() {
	///*
	ssd1306_clear_screen(0x00);
	//ssd1306_refresh_gram( DIS_MULTI_BYTE );
	
	sprintf(strCountHex, "%d", totalStepCount);
	
	if( ( totalStepCount >= 0) && ( totalStepCount <= 9) ) {
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(28, 8, strCountHex, 16, 1); // 16크기 1번째 자리수
	}
	else if( ( totalStepCount >= 10) && ( totalStepCount <= 99) ) {
		//ssd1306_display_string(5 + 14, 10, "2", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(28 - 4, 8, strCountHex, 16, 1); // 16크기 2번째 자리수
	}
	else if( ( totalStepCount >= 100) && ( totalStepCount <= 999) ) {
		//ssd1306_display_string(5 + (14 * 2), 10, "3", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(28 - 8, 8, strCountHex, 16, 1); // 16크기 3번째 자리수
	}
	else if( ( totalStepCount >= 1000) && ( totalStepCount <= 9999) ) {
		//ssd1306_display_string(5 + (14 * 3), 10, "4", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(28 - 12, 8, strCountHex, 16, 1); // 16크기 4번째 자리수
	}
	else if( ( totalStepCount >= 10000) && ( totalStepCount <= 99999) ) {
		//ssd1306_display_string(5 + (14 * 4), 10, "5", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(28 - 16, 8, strCountHex, 16, 1); // 16크기 5번째 자리수
	}
	
	ssd1306_refresh_gram( DIS_MULTI_BYTE );
	//*/
}

// 요청스펙
// 이동거리는 999.9km
// 소수점 1자리
// km는 작은 크기
void showOledDistance() {
	///*
	ssd1306_clear_screen(0x00);
	//ssd1306_refresh_gram( DIS_MULTI_BYTE );
	
	stepToKm( totalStepCount );
	
	sprintf(strDistanceHex, "%d.%d", dataDis.disFirst, dataDis.disThird);
	
	// 10, 8
	
	if( ( dataDis.disFirst >= 0) && ( dataDis.disFirst <= 9) ) {
		printf("-- 3\n");
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(24, 8, strDistanceHex, 16, 1); // 16크기 3번째 자리수
	}
	else if( ( dataDis.disFirst >= 10) && ( dataDis.disFirst <= 99) ) {
		printf("-- 4\n");
		//ssd1306_display_string(5 + 14, 10, "2", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(24 - 8, 8, strDistanceHex, 16, 1); // 16크기 4번째 자리수
	}
	else if( ( dataDis.disFirst >= 100) && ( dataDis.disFirst <= 999) ) {
		printf("-- 5\n");
		//ssd1306_display_string(5 + (14 * 2), 10, "3", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(24 - 16, 8, strDistanceHex, 16, 1); // 16크기 5번째 자리수
	}
	/*
	else if( ( dataDis.disFirst >= 1000) && ( dataDis.disFirst <= 9999) ) {
		printf("-- 6\n");
		//ssd1306_display_string(5 + (14 * 3), 10, "4", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(10 - 12, 8, strDistanceHex, 16, 1); // 16크기 6번째 자리수
	}
	
	else if( ( dataDis.disFirst >= 10000) && ( dataDis.disFirst <= 99999) ) {
		printf("-- 7\n");
		//ssd1306_display_string(5 + (14 * 4), 10, "5", 12, 1);
		//ssd1306_display_string(5, 10, strCountHex, 16, 1);
		ssd1306_display_string(10 - 16, 8, strDistanceHex, 16, 1); // 16크기 7번째 자리수
	}
	*/
	
	ssd1306_display_string(50, 12, "km", 12, 1);
	
	ssd1306_refresh_gram( DIS_MULTI_BYTE );
	//*/
}

// 99,999kcal 로 표기
// kcal은 작은 크기
void showOledCalory() {
	///*
	stepToCalory( totalStepCount );
	
	ssd1306_clear_screen(0x00);
	//ssd1306_refresh_gram( DIS_MULTI_BYTE );

	if( ( dataCal.calData >= 0) && ( dataCal.calData <= 9) ) {
		printf("-- 1\n");
		sprintf(strCaloryHex, "%d", dataCal.calData);
		// 아래와 같이 y축 8이면 센터다. 
		//ssd1306_display_string(24, 8, strCaloryHex, 16, 1); // 16크기 1번째 자리수  (0 kcal)
		ssd1306_display_string(28, 6, strCaloryHex, 16, 1); // 16크기 1번째 자리수  (0 kcal)
	}
	else if( ( dataCal.calData >= 10) && ( dataCal.calData <= 99) ) {
		printf("-- 2\n");
		sprintf(strCaloryHex, "%d", dataCal.calData);
		ssd1306_display_string(28 - 4, 6, strCaloryHex, 16, 1); // 16크기 2번째 자리수 (10 kcal)
	}
	else if( ( dataCal.calData >= 100) && ( dataCal.calData <= 999) ) {
		printf("-- 3\n");
		sprintf(strCaloryHex, "%d", dataCal.calData);
		ssd1306_display_string(28 - 8, 6, strCaloryHex, 16, 1); // 16크기 3번째 자리수 (100 kcal)
	}
	else if( ( dataCal.calData >= 1000) && ( dataCal.calData <= 9999) ) {
		printf("-- 5\n");
		sprintf(strCaloryHex, "%d,%d", dataCal.calFirst, dataCal.calSecond);
		ssd1306_display_string(28 - 16, 6, strCaloryHex, 16, 1); // 16크기 5번째 자리수 (1,000 kcal)
	}
	
	else if( ( dataCal.calData >= 10000) && ( dataCal.calData <= 99999) ) {
		printf("-- 6\n");
		sprintf(strCaloryHex, "%d,%d", dataCal.calFirst, dataCal.calSecond);
		ssd1306_display_string(28 - 20, 6, strCaloryHex, 16, 1); // 16크기 6번째 자리수 (10,000 kcal)
	}
	
	ssd1306_display_string(38, 20, "kcal", 12, 1);	
	ssd1306_refresh_gram( DIS_MULTI_BYTE );
	//*/
}


void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
	//printf("pwm_ready_callback \n");
    ready_flag = true;
}

void pwm_stop_A(void) {
	printf("pwm_stop_A\n");
	/*
   nrf_gpio_pin_clear(MOTOR_A_EN_PIN);
   app_pwm_disable(&PWM1);
   nrf_drv_gpiote_out_task_disable(MOTOR_A_PWM_PIN);
   nrf_gpio_cfg_output(MOTOR_A_PWM_PIN);
   nrf_gpio_pin_clear(MOTOR_A_PWM_PIN);
   //pwmAEnabled=false;	
	*/
	
	nrf_gpio_pin_clear(MOTOR_A_EN_PIN);
}

void pwm_start_A(void) {
	printf("pwm_start_A\n");
	nrf_gpio_pin_set(MOTOR_A_EN_PIN);
    /*
	nrf_drv_gpiote_out_task_enable(MOTOR_A_PWM_PIN); 
    app_pwm_enable(&PWM1);
    //pwmAEnabled=true;	
	
	//while (app_pwm_channel_duty_set(&PWM1, 1, 5) == NRF_ERROR_BUSY);
	//app_pwm_channel_duty_set(&PWM1, 1, 5);
	while (app_pwm_channel_duty_set(&PWM1, 1, 5) == NRF_ERROR_BUSY);
	*/
	STOP_VIBE = true;	
}

static double stepToMile(double step) {
	return step / stepPerMile(step);
}

static double stepPerMile(double step) {
	if( 1 ) { // male
		return MILE_BY_INCHE / ( 0.415 * getInfoHeight() );
	}
	else {    // female
		return MILE_BY_INCHE / ( 0.413 * getInfoHeight() );
	}
}

static double stepToKm(double step) {
	///*
	double kmData;
	kmData = step / stepPerKm(step);
	kmData = kmData * 1.6; // mile을 km로 변환
	//return kmData * 1.6; 
			
	printf("km : %f\n", kmData);
	
	dataDis.disFirst = kmData / 1;
	dataDis.disSecond = kmData - (int)kmData;	
	
	printf("km sep : %d, %f\n", dataDis.disFirst, dataDis.disSecond);
	
	dataDis.disSecond = dataDis.disSecond * 10;
	dataDis.disThird = dataDis.disSecond / 1;
	
	printf("km sep : %d.%d\n", dataDis.disFirst, dataDis.disThird);
	//*/
	//return step / stepPerKm(step);
}

static double stepPerKm(double step) {
	if( 1 ) { // male
		return MILE_BY_INCHE / ( 0.415 * getInfoHeight() );
	}
	else {    // female
		return MILE_BY_INCHE / ( 0.413 * getInfoHeight() );
	}
}

static void setInfoWeight(double w, int mode) {
	int wPound = 0;
	
	printf("setInfoWeight 1 : %d\n", w);
	
	if( mode == CONV_POUND ) {            // pound
		dataManInfo.weight = w;
	}
	else {                       // kg
		wPound = w * 2.2;
		dataManInfo.weight = wPound;
	}
	
	printf("setInfoWeight 2 : %f\n", dataManInfo.weight);
}

static void manInfoInit() {
	dataManInfo.heightInInches = 0;
	dataManInfo.sex = 1;
	dataManInfo.weight = 0;
}

static void setInfoHeight(double h, int mode) {
	int hInch = 0;
	
	printf("setInfoHeight 1 : %d\n", h);
	
	if( mode == CONV_INCH ) {                   // inch
		dataManInfo.heightInInches = h;
	}
	else {                              // cm
		hInch = h / 2.54;
		dataManInfo.heightInInches = hInch;
	}
	
	printf("setInfoHeight 2 : %f\n", dataManInfo.heightInInches);
}

static void setInfoSex(uint8 s) {
	dataManInfo.sex = s;
}

static double getInfoHeight() {
	return dataManInfo.heightInInches;
}

static double getInfoWeight() {
	return dataManInfo.weight;
}

static void showManInfo() {
	printf("man info : %u, %f, %f\n", dataManInfo.sex, dataManInfo.heightInInches, dataManInfo.weight);
}

static uint32 stepToCalory(uint32 step) {
	//return stepToMile(step) * (getInfoWeight() * 0.57);
	dataCal.calData = stepToMile(step) * (getInfoWeight() * 0.57);
	
	dataCal.calFirst = dataCal.calData / 1000;
	dataCal.calSecond = dataCal.calData % 1000;
	
	printf("show cal : %d, %d\n", dataCal.calFirst, dataCal.calSecond);
	
	return 99999;
}

static void sendBattery() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	int data = 0;
	
	//srand(time(NULL));
	data = rand() % 100;
	
	printf("sendBattery %d\n", data);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xD4;
	row_data[2] = 0x1;
	
	row_data[3] = data;
	
	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;

	//err_code = ble_nus_string_send(&m_nus, m_rx_data, 20);
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("sendBattery end\n");
	
	//int i = 0;
	//for( i = 0; i< 300; i++) {
		//err_code = ble_nus_string_send(&m_nus, row_data, 20);
		if( err_code != NRF_SUCCESS ) {
			printf("sendBattery: %u\n", err_code);
		}
		//
	//}		
}

static void goalInit() {
	goalInfo.goal_step = 0;      // 목표 걸음수
	goalInfo.goal_calory = 0;    // 목표 칼로리
	goalInfo.goal_distance = 0;  // 목표 거리
	goalInfo.goal_default = 0;   // 기본목표 
}

static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result)
{
    uint32_t err_code;
    //APP_ERROR_CHECK(event_result);
	
	printf("device_manager_evt_handler\n");
	
	if( event_result != NRF_SUCCESS ) {
		printf("device_manager_evt_handler, %d\n", event_result);
	}
	
    //ble_ancs_c_on_device_manager_evt(&m_ancs_c, p_handle, p_evt);
/*
	if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
*/	
    switch (p_event->event_id)
    {
        case DM_EVT_CONNECTION:
            //m_peer_handle = (*p_handle);
            //err_code      = app_timer_start(m_sec_req_timer_id, SECURITY_REQUEST_DELAY, NULL);
            //APP_ERROR_CHECK(err_code);
            break;

        case DM_EVT_LINK_SECURED:
            //err_code = ble_db_discovery_start(&m_ble_db_discovery,
            //                                  p_evt->event_param.p_gap_param->conn_handle);
            //APP_ERROR_CHECK(err_code);
            break; 

        default:
            break;

    }
    return NRF_SUCCESS;
}

/*
주의 
이 작업을 위해서는 device_manager.h를 추가해야하며, 해당 프로젝트의 config폴더에
다른 프로젝트에서 device_manager_cnfg.h를 복사해서 넣어야한다.
*/
static void device_manager_init(bool erase_bonds) 
{
   uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    memcpy(&m_sec_param, &register_param.sec_param, sizeof(ble_gap_sec_params_t));

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}

static void reset_prepare(void)
{
    uint32_t err_code;
    if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Disconnect from peer.
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
        err_code = bsp_indication_set(BSP_INDICATE_IDLE);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        // If not connected, the device will be advertising. Hence stop the advertising.
        advertising_stop();
    }
    err_code = ble_conn_params_stop();
    APP_ERROR_CHECK(err_code);
    nrf_delay_ms(500);
}

static void advertising_stop(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);
}

/*
static void app_context_load(dm_handle_t const * p_handle)
{
    uint32_t                 err_code;
    static uint32_t          context_data;
    dm_application_context_t context;

    context.len    = sizeof(context_data);
    context.p_data = (uint8_t *)&context_data;

    err_code = dm_application_context_get(p_handle, &context);
    if (err_code == NRF_SUCCESS)
    {
        // Send Service Changed Indication if ATT table has changed.
        if ((context_data & (DFU_APP_ATT_TABLE_CHANGED << DFU_APP_ATT_TABLE_POS)) != 0)
        {
            err_code = sd_ble_gatts_service_changed(m_conn_handle, APP_SERVICE_HANDLE_START, BLE_HANDLE_MAX);
            if ((err_code != NRF_SUCCESS) &&
                (err_code != BLE_ERROR_INVALID_CONN_HANDLE) &&
                (err_code != NRF_ERROR_INVALID_STATE) &&
                (err_code != BLE_ERROR_NO_TX_BUFFERS) &&
                (err_code != NRF_ERROR_BUSY) &&
                (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
            {
                APP_ERROR_HANDLER(err_code);
            }
        }

        err_code = dm_application_context_delete(p_handle);
        APP_ERROR_CHECK(err_code);
    }
    else if (err_code == DM_NO_APP_CONTEXT)
    {
        // No context available. Ignore.
    }
    else
    {
        APP_ERROR_HANDLER(err_code);
    }
}
*/

static void writeActivity(int count) {
	uint8 count_mode = 0;
	
	// Write Enable
	m_tx_data[0] = 0x06;   // write enable cmd
	spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
	
	//currentActivity = 0x40; // 수면일때
	currentActivity = 0x00; // 걷기일때
	
	if( count > 63 ) {
		// count값은 6비트이므로 63보다는 크면 안된다.
		printf("+++++++ count > 63 +++++\n");
	}
	
	count_mode = count | currentActivity;
	
	printf("test %x, %d\n", count_mode, count_mode);
						
	activityTemp[0] = 0x02;	 // page program cmd
	activityTemp[1] = flash_addr_activity >> 16;
	activityTemp[2] = flash_addr_activity >> 8;
	activityTemp[3] = flash_addr_activity;	
	activityTemp[4] = count;		

	printf("ac addr: %u, %u, %u, %d \n", activityTemp[1], activityTemp[2], activityTemp[3], count);
	
	spi_send_recv(activityTemp, m_rx_data, 5, RW_MODE_W);	
	
	flash_addr_activity += 1;			
}

static void readActivity() {
	uint32_t err_code;
	uint8 read_count = 0;
	
	printf("readActivity, %d, %d\n", activity_count, activity_count_readed);
	
	// 읽을 데이터가 있어야 한다.
	if( activity_count > activity_count_readed ) {
		if( ( activity_count - activity_count_readed ) >= 12 ) {
			read_count = 12;
		}
		else {
			read_count = activity_count - activity_count_readed;
		}
		
		activity_count_readed += read_count;
		
		printf("readActivity, r_count %d\n", read_count);
		
		// Read
		m_tx_data[0] = 0x03;	
		m_tx_data[1] = flash_addr_activity_r >> 16;
		m_tx_data[2] = flash_addr_activity_r >> 8;
		m_tx_data[3] = flash_addr_activity_r;		       			
		
		printf("Raddr: %u, %u, %u \n", m_tx_data[1], m_tx_data[2], m_tx_data[3]);
		
		// 40바이트를 초기화한다. 
		memset(m_rx_data, 0, 40);
		memset(row_data, 0, 20);
		spi_send_recv(m_tx_data, m_rx_data, 4 + read_count, RW_MODE_R); // 12, 패킷 한번에 12개의 데이터(3분)를 보내기 때문에 12개를 읽어오자.

		memcpy(&row_data[5], &m_rx_data[4], 12);
		flash_addr_activity_r += 12;//SENSOR_BUF_SIZE;
		
		row_data[0] = 0xFF;
		row_data[1] = 0xD7;  // 활동 데이터 가져오기
		row_data[2] = 0xE;   // data size
		row_data[3] = 0x00;  // index msb
		row_data[4] = 0x00;  // index lsb

		row_data[read_count + 5] = 0xff; // CRC
		
		///*
		printf("bt1 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", row_data[0], row_data[1], row_data[2], row_data[3], 
			row_data[4], row_data[5], row_data[6], row_data[7], row_data[8], row_data[9]);
		printf("bt2 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", row_data[10], row_data[11], row_data[12], row_data[13],
			row_data[14], row_data[15], row_data[16], row_data[17], row_data[18], row_data[19]);
		
		err_code = ble_nus_string_send(&m_nus, row_data, 20);
		printf("send: %u\n", err_code);
	}	
	else { // 더 이상 읽을 데이터가 없다. 전송완료다. 
		// 전송완료에 대한 초기화가 필요하다.
		SEND_ACT_DATA = false;				
	}
}