#include <stdio.h>
#include <stdbool.h>
#include "MX25_CMD.h"
#include "app_util_platform.h"
#include "nrf_drv_spi.h"
#include "app_uart.h"

#if (SPI0_ENABLED == 1)
    static const nrf_drv_spi_t m_spi_master = NRF_DRV_SPI_INSTANCE(0);
#elif (SPI1_ENABLED == 1)
    static const nrf_drv_spi_t m_spi_master = NRF_DRV_SPI_INSTANCE(1);
#elif (SPI2_ENABLED == 1)
    static const nrf_drv_spi_t m_spi_master = NRF_DRV_SPI_INSTANCE(2);
#else
    #error "No SPI enabled."
#endif

#define TX_RX_BUF_LENGTH         16                 /**< SPI transaction buffer length. */

static uint8_t m_tx_data[TX_RX_BUF_LENGTH] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer with data to transfer. */
static uint8_t m_rx_data[TX_RX_BUF_LENGTH] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer for incoming data. */

static void showReadData(uint16_t len) {
	uint16_t iLoop;
	
	printf("show len : %d\n", len);
	for( iLoop = 0; iLoop < len; iLoop++) {
		printf("Read 1 : %x      \n", m_rx_data[iLoop]);
	}		
}

ReturnMsg CMD_RDID(uint8 * const p_tx_data, uint8 * const p_rx_data)
{
    //uint32 temp;
    //uint8  gDataBuffer[3];

    // Chip select go low to start a flash command
    //CS_Low();
	
	m_tx_data[0] = 0x9F;

	printf("cmd_rdid : errorcode 11 \n");
    // Send command
    //SendByte( FLASH_CMD_RDID, SIO );
	 uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master, p_tx_data, 1, p_rx_data, 4);
	
	printf("cmd_rdid : errorcode : %u\n", err_code);
	
	showReadData(4);

    // Get manufacturer identification, device identification
	/*
    gDataBuffer[0] = GetByte( SIO );
    gDataBuffer[1] = GetByte( SIO );
    gDataBuffer[2] = GetByte( SIO );
	*/

    // Chip select go high to end a command
    //CS_High();

    // Store identification
    /*
		temp =  gDataBuffer[0];
    temp =  (temp << 8) | gDataBuffer[1];
    *Identification =  (temp << 8) | gDataBuffer[2];
		*/

    return FlashOperationSuccess;
}